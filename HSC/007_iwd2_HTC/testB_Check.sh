#!/bin/bash


###
# Bash script for ImpedanceWake2D, to check the HTCondor run produced results
###

###
# For more infos on AFS phaseout, see G.Iadarola presentation: https://indico.cern.ch/event/812709/
# and script example: https://github.com/giadarol/afs_phaseout_ecloud_test/blob/master/test_pyecloud.sh
###

###
# 2019-05-06: script created by N. Mounet (nicolas.mounet@cern.ch)
###

# The script will stop on the first error 
set -e


##############################
# Path definition
##############################

# Folder in which the tests will be performed 
PATHTEST=/afs/cern.ch/work/g/giadarol/afs_phaseout_temptests_iw2d_htc


######################
# Activate miniconda #
######################

printf "\n\n**********************\nActivating miniconda environment\n"

source $PATHTEST/miniconda2/bin/activate
PYTHONEXE=`which python`
echo "Python executable: " $PYTHONEXE

# Set matplotlib backend to Agg 
# (to avoid errors if display not avaialable)
export MPLBACKEND=Agg


#####################################
# Check what was launched on HTCondor
#####################################

printf "\n\n**********************\nChecking calculations launched on HTCondor\n"

cat > $PATHTEST/check_HTCondor.py << EOF
#!$PYTHONEXE

from Impedance import imp_model_from_IW2D,impedance_wake_input,layer,Z_layer,\
            vacuum_layer,freq_param,identify_component,test_impedance_wake_comp
from string_lib import float_to_str
import numpy as np

gamma = 2.5
vac_lay = vacuum_layer(thickness=np.inf)
fpar = freq_param(fmin=1.e3,fmax=1.e8,ftypescan=0,nflog=5,fadded=[])
dire = 'test/'

def check_imp_model(imp_mod,flat=False,message=''):
    components = ['Zlong','Zxdip','Zydip','Zxquad','Zyquad']
    #if flat:
    #    components += ['Zycst']
    
    for comp in components:
        l = [len(iw.func)==26 for iw in imp_mod if test_impedance_wake_comp(iw,*identify_component(comp)[:-1])]
        assert len(l)==1 and all(l), "{}: pb with component {}".format(message,comp)


for b in np.arange(1e-3,11e-3,1e-3):

    print "    b={} mm ...".format(b*1e3)
    for rho in [1e-8, 1e-7, 1e-6, 1e-5, 1e-4]:

        lay1 = Z_layer(rhoDC=rho,thickness=0.05)
        layers = [lay1,vac_lay]
        
        # round
        comment = '_round_b{}mm_rho{}muOhmm'.format(float_to_str(b*1e3),float_to_str(rho*1e6))
        iw_input = impedance_wake_input(machine='',b=[b],length=1.,layers=layers,geometry='round',
                                        comment=comment, gamma=gamma, fpar=fpar, Yokoya=[1,np.pi**2/24,np.pi**12/12,0,0])
        imp_mod,[] = imp_model_from_IW2D(iw_input,wake_calc=False,path='$PATHTEST/IW2D/ImpedanceWake2D',flagrm=True,
                                         lxplusbatch='retrieve',dire=dire)
        check_imp_model(imp_mod,flat=False,message="b={}mm, rho={} muOhm.m, round".format(b*1e3,rho*1e6))
        
        
        # flat
        comment = '_flat_b{}mm_rho{}muOhmm'.format(float_to_str(b*1e3),float_to_str(rho*1e6))

        iw_input = impedance_wake_input(machine='',b=[b],length=1.,layers=layers,geometry='flat',
                                        comment=comment, gamma=gamma, fpar=fpar)
        imp_mod,[] = imp_model_from_IW2D(iw_input,wake_calc=False,path='$PATHTEST/IW2D/ImpedanceWake2D',flagrm=True,
                                         lxplusbatch='retrieve',dire=dire)
        check_imp_model(imp_mod,flat=True,message="b={}mm, rho={} muOhm.m, flat".format(b*1e3,rho*1e6))        

    for epsb in [1.2, 1.5, 2., 3., 5.]:

        lay1 = layer(rhoDC=4e12,tau=0,epsb=epsb,mur=1,fmu=np.inf,thickness=0.05)
        layers = [lay1,vac_lay]

        # round
        comment = '_round_b{}mm_epsb{}'.format(float_to_str(b*1e3),float_to_str(epsb))
        iw_input = impedance_wake_input(machine='',b=[b],length=1.,layers=layers,geometry='round',
                                        comment=comment, gamma=gamma, fpar=fpar, Yokoya=[1,np.pi**2/24,np.pi**12/12,0,0])
        imp_mod,[] = imp_model_from_IW2D(iw_input,wake_calc=False,path='$PATHTEST/IW2D/ImpedanceWake2D',flagrm=True,
                                         lxplusbatch='retrieve',dire=dire)
        check_imp_model(imp_mod,flat=False,message="b={}mm, epsb={}, round".format(b*1e3,epsb))        

        # flat
        comment = '_flat_b{}mm_epsb{}'.format(float_to_str(b*1e3),float_to_str(epsb))
        iw_input = impedance_wake_input(machine='',b=[b],length=1.,layers=layers,geometry='flat',
                                        comment=comment, gamma=gamma, fpar=fpar)
        imp_mod,[] = imp_model_from_IW2D(iw_input,wake_calc=False,path='$PATHTEST/IW2D/ImpedanceWake2D',flagrm=True,
                                         lxplusbatch='retrieve',dire=dire)
        check_imp_model(imp_mod,flat=True,message="b={}mm, epsb={}, flat".format(b*1e3,epsb))        

EOF

chmod +x $PATHTEST/check_HTCondor.py

$PATHTEST/check_HTCondor.py

printf "Done\n**********************\n"
