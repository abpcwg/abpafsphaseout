#!/bin/bash


###
# Bash script for ImpedanceWake2D installation, and run on HTCondor
# The repository is cloned from GitLab
# Then install GMP, MPFR and ALGLIB and compile the code
# Then we launch the code on HTContor.
###

###
# For more infos on AFS phaseout, see G.Iadarola presentation: https://indico.cern.ch/event/812709/
# and script example: https://github.com/giadarol/afs_phaseout_ecloud_test/blob/master/test_pyecloud.sh
###

###
# 2019-05-06: script created by N. Mounet (nicolas.mounet@cern.ch)
###

# The script will stop on the first error 
set -e


##############################
# Path definition
##############################

# Folder in which the tests will be performed 
PATHTEST=/afs/cern.ch/work/g/giadarol/afs_phaseout_temptests_iw2d_htc


COMPILATION_OUTPUT="compilation.out"
COMPILATION_ERRORS="compilation.err"
CONDA_OUTPUT="conda.out"
CONDA_ERRORS="conda.err"

# Remove any pre-existing folder
if [ -d "$PATHTEST" ] || [ -f "$PATHTEST" ]; then
    echo "Removing $PATHTEST..."
    rm -rf $PATHTEST
fi

# Create the folder
mkdir $PATHTEST
cd $PATHTEST

#####################################################
# Clone the repository using kerberos authentication
# WARNING: the repository should be made public for the purpose of the test
#####################################################
git clone https://:@gitlab.cern.ch:8443/IRIS/IW2D.git


##############################
# Installation and compilation
# of the C++ code
##############################

printf "\n\n**********************\nInstalling IW2D C++ code\n"

$PATHTEST/IW2D/Install_scripts/install_IW2D_lxplus_CERN_CC7.sh > $PATHTEST/$COMPILATION_OUTPUT 2> $PATHTEST/$COMPILATION_ERRORS

printf "Done\n**********************\n"


#########################################
# Download and install miniconda (python)
#########################################

printf "\n\n**********************\nInstalling Miniconda\n"

cd $PATHTEST
mkdir downloads
cd downloads

wget https://repo.anaconda.com/miniconda/Miniconda2-latest-Linux-x86_64.sh
bash Miniconda2-latest-Linux-x86_64.sh -b -p $PATHTEST/miniconda2 > $PATHTEST/$CONDA_OUTPUT 2> $PATHTEST/$CONDA_ERRORS

printf "Done\n**********************\n"


######################
# Activate miniconda #
######################

printf "\n\n**********************\nActivating and installing dependencies\n"

source $PATHTEST/miniconda2/bin/activate
PYTHONEXE=`which python`
echo "Python executable: " $PYTHONEXE

# Set matplotlib backend to Agg 
# (to avoid errors if display not avaialable)
export MPLBACKEND=Agg

##############################
# Install required libraries #
##############################

pip install numpy > $PATHTEST/$CONDA_OUTPUT 2> $PATHTEST/$CONDA_ERRORS
pip install scipy > $PATHTEST/$CONDA_OUTPUT 2> $PATHTEST/$CONDA_ERRORS
pip install matplotlib > $PATHTEST/$CONDA_OUTPUT 2> $PATHTEST/$CONDA_ERRORS

printf "Done\n**********************\n"

##################################
# Installation of IW2D python code
##################################

printf "\n\n**********************\nAdding the IW2D paths in conda activate script\n"

# paths for IW2D
cd $PATHTEST/IW2D/ImpedanceWake2D
echo $'\n# paths for ImpedanceWake2D' >> $PATHTEST/miniconda2/bin/activate
echo "LD_LIBRARY_PATH="`pwd`":\$LD_LIBRARY_PATH" >> $PATHTEST/miniconda2/bin/activate
echo "export LD_LIBRARY_PATH" >> $PATHTEST/miniconda2/bin/activate
echo "PATH="`pwd`":\$PATH" >> $PATHTEST/miniconda2/bin/activate
echo "export PATH" >> $PATHTEST/miniconda2/bin/activate
echo "IW2D_PATH="`pwd` >> $PATHTEST/miniconda2/bin/activate
echo "export IW2D_PATH" >> $PATHTEST/miniconda2/bin/activate

# pythonpath for general python tools
cd $PATHTEST/IW2D/PYTHON_codes_and_scripts/General_Python_tools
echo $'\n# Python path for General_Python_tools' >> $PATHTEST/miniconda2/bin/activate
echo "PYTHONPATH="`pwd`":\$PYTHONPATH" >> $PATHTEST/miniconda2/bin/activate
echo "export PYTHONPATH" >> $PATHTEST/miniconda2/bin/activate

# pythonpath for impedance library, and path for Yokoya factor file
cd $PATHTEST/IW2D/PYTHON_codes_and_scripts/Impedance_lib_Python
echo $'\n# paths for Impedance_lib_Python' >> $PATHTEST/miniconda2/bin/activate
echo "PYTHONPATH="`pwd`":\$PYTHONPATH" >> $PATHTEST/miniconda2/bin/activate
echo "export PYTHONPATH" >> $PATHTEST/miniconda2/bin/activate
echo "YOK_PATH="`pwd` >> $PATHTEST/miniconda2/bin/activate
echo "export YOK_PATH" >> $PATHTEST/miniconda2/bin/activate

source $PATHTEST/miniconda2/bin/activate

printf "Done\n**********************\n"

##############################
# Run on HTCondor
##############################

printf "\n\n**********************\nLaunching calculations on HTCondor\n"

cat > $PATHTEST/run_HTCondor.py << EOF
#!$PYTHONEXE

from Impedance import imp_model_from_IW2D,impedance_wake_input,layer,Z_layer,vacuum_layer,freq_param
from string_lib import float_to_str
import numpy as np

gamma = 2.5
vac_lay = vacuum_layer(thickness=np.inf)
fpar = freq_param(fmin=1.e3,fmax=1.e8,ftypescan=0,nflog=5,fadded=[])
dire = 'test/'

for b in np.arange(1e-3,11e-3,1e-3):

    print "    b={} mm ...".format(b*1e3)
    for rho in [1e-8, 1e-7, 1e-6, 1e-5, 1e-4]:

        lay1 = Z_layer(rhoDC=rho,thickness=0.05)
        layers = [lay1,vac_lay]
        
        # round
        comment = '_round_b{}mm_rho{}muOhmm'.format(float_to_str(b*1e3),float_to_str(rho*1e6))
        iw_input = impedance_wake_input(machine='',b=[b],length=1.,layers=layers,geometry='round',
                                        comment=comment, gamma=gamma, fpar=fpar, Yokoya=[1,np.pi**2/24,np.pi**12/12,0,0])
        imp_mod,[] = imp_model_from_IW2D(iw_input,wake_calc=False,path='$IW2D_PATH',flagrm=True,
                                         lxplusbatch='launch',dire=dire)
        
        # flat
        comment = '_flat_b{}mm_rho{}muOhmm'.format(float_to_str(b*1e3),float_to_str(rho*1e6))

        iw_input = impedance_wake_input(machine='',b=[b],length=1.,layers=layers,geometry='flat',
                                        comment=comment, gamma=gamma, fpar=fpar)
        imp_mod,[] = imp_model_from_IW2D(iw_input,wake_calc=False,path='$IW2D_PATH',flagrm=True,
                                         lxplusbatch='launch',dire=dire)

    for epsb in [1.2, 1.5, 2., 3., 5.]:

        lay1 = layer(rhoDC=4e12,tau=0,epsb=epsb,mur=1,fmu=np.inf,thickness=0.05)
        layers = [lay1,vac_lay]

        # round
        comment = '_round_b{}mm_epsb{}'.format(float_to_str(b*1e3),float_to_str(epsb))
        iw_input = impedance_wake_input(machine='',b=[b],length=1.,layers=layers,geometry='round',
                                        comment=comment, gamma=gamma, fpar=fpar, Yokoya=[1,np.pi**2/24,np.pi**12/12,0,0])
        imp_mod,[] = imp_model_from_IW2D(iw_input,wake_calc=False,path='$IW2D_PATH',flagrm=True,
                                         lxplusbatch='launch',dire=dire)
        # flat
        comment = '_flat_b{}mm_epsb{}'.format(float_to_str(b*1e3),float_to_str(epsb))
        iw_input = impedance_wake_input(machine='',b=[b],length=1.,layers=layers,geometry='flat',
                                        comment=comment, gamma=gamma, fpar=fpar)
        imp_mod,[] = imp_model_from_IW2D(iw_input,wake_calc=False,path='$IW2D_PATH',flagrm=True,
                                         lxplusbatch='launch',dire=dire)

EOF

chmod +x $PATHTEST/run_HTCondor.py

$PATHTEST/run_HTCondor.py

printf "Done\n**********************\n"
