#!/bin/bash

# Clean environement
# [ -z "$CLEANED" ] && exec /bin/env -i CLEANED=1 /bin/bash "$0" "$@"


###
# Bash script for ImpedanceWake2D installation
# The repository is cloned from GitLab
# Then install GMP, MPFR and ALGLIB and compile the code
# Then we run the code on four examples and check the results
###

###
# For more infos on AFS phaseout, see G.Iadarola presentation: https://indico.cern.ch/event/812709/
# and script example: https://github.com/giadarol/afs_phaseout_ecloud_test/blob/master/test_pyecloud.sh
###

###
# 2019-05-02: script created by D.Amorim (damorim@cern.ch) and N. Mounet (nicolas.mounet@cern.ch)
# 2019-05-03: v2 version: use the install & test scripts inside the git repository
###

# The script will stop on the first error 
set -e


##############################
# Path definition
##############################

# Folder in which the tests will be performed 
PATHTEST=/afs/cern.ch/work/g/giadarol/afs_phaseout_temptests_iw2d

COMPILATION_OUTPUT="compilation.out"
COMPILATION_ERRORS="compilation.err"

# Remove any pre-existing folder
if [ -d "$PATHTEST" ] || [ -f "$PATHTEST" ]; then
    echo "Removing $PATHTEST..."
    rm -rf $PATHTEST
fi

# Create the folder
mkdir $PATHTEST
cd $PATHTEST

#####################################################
# Clone the repository using kerberos authentication
# WARNING: the repository should be made public for the purpose of the test
#####################################################
git clone https://:@gitlab.cern.ch:8443/IRIS/IW2D.git


##############################
# Installation and compilation
##############################

printf "\n\n**********************\nInstalling the code\n"

$PATHTEST/IW2D/Install_scripts/install_IW2D_lxplus_CERN_CC7.sh > $PATHTEST/$COMPILATION_OUTPUT 2> $PATHTEST/$COMPILATION_ERRORS

printf "Done\n**********************\n"

##############################
# Run and check
##############################

$PATHTEST/IW2D/Tests/run_and_check_IW2D.sh
