#!/usr/bin/bash

PATHTEST=/afs/cern.ch/work/g/giadarol/afs_phaseout_temptests_bimbim
MYPATH=$(pwd)
set -e
mkdir $PATHTEST
cp -r ./* $PATHTEST/
cd $PATHTEST
source /cvmfs/sft.cern.ch/lcg/views/LCG_93python3/x86_64-centos7-gcc7-opt/setup.sh
export PYTHONPATH=$PYTHONPATH:$PATHTEST/BimBim
python3 ./BimBimTest.py $PATHTEST BimBimTest 11 4 10.0 0.01 15.0
cd $MYPATH
