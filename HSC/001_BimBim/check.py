#!/usr/bin/env python3

import pickle, os, sys
import numpy as np

pathTest = sys.argv[1] 

myFile = open(os.path.join(pathTest,'BimBimTest.pkl'),'rb')
ev = pickle.load(myFile)
myFile.close()

if len(ev) != 352 or np.any(np.isnan(ev)) or np.any(np.isinf(ev)):
    print('failed')
else:
    print('ok')
