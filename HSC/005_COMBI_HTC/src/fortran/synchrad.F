#include <const.h>

      subroutine synchrad(b1,Np,energy,m,
     &   rho,circ,alfmom,av_betx,av_bety,betax,betay,
     &   frac,table,nt)
      implicit none
      integer Np
      double precision b1(NCOORD,Np)
      double precision :: m,rho,circ,alfmom
      double precision :: av_betx,av_bety,betax,betay
      double precision :: energy,eta
      integer :: n,k 
      double precision :: frac
      double precision :: cg,dEturn,phis,cq
      double precision :: dx,dy
      double precision :: energyp,gammap,rhop
      double precision :: ngp,ecp
      integer :: ranp,nt
      double precision, dimension(nt,2) :: table
      double precision :: de, totde


      COMMON/COMBI_CST/rp,ga,c,e,eps0,hbar, pi,a0
      double precision rp, ga, c,e,eps0,hbar,pi,a0

!    energy   Design energy
!    m        Particle mass
!    rho      Bending radius
!    circ     Full machine circumference
!    alfmom   Momentum compaction factor
!    av_betx  average beta in the arc
!    av_bety  average beta in the arc
!    frac     Arc length (fraction of 2piR)
!    table    Table with the photon energy distribution
!    nt       size of the table
    
      eta = alfmom-1/ga**2

      dx = alfmom*circ/(2*pi)
      dy = sqrt(13.0/55.0*av_betx*av_bety)/ga

      cq = 55/(32*sqrt(3.0))*hbar*c/m ! quantum constant

      cg = e*1E-9/(3*eps0*m**4)
      dEturn = cg*energy**4*frac/rho ! Average energy loss

      !open (unit=14,file="totde.dat",access="APPEND")
!$OMP PARALLEL DEFAULT(FIRSTPRIVATE) SHARED(b1,table)
!$OMP& PRIVATE(n,k)
!$OMP DO SCHEDULE(GUIDED,1000)
      do n=1,Np
        !energyp = b1(6,n)*energy+energy ! particle energy
        !gammap = energyp/m !particle gamma
        energyp = energy
        gammap = ga
        rhop = rho*energyp/energy ! bending radius
        ngp = 5.0d0*dsqrt(3.0d0)*a0*gammap/(6.0d0*rhop) ! Avg photon emitted per unit length
        ecp = 3.0d0*gammap**3.0d0*hbar*c/(2.0d0*rhop) ! photon critical energy

        totde = 0.0  

        call poissonRandomNumber(ngp*(2*pi*rhop*frac),ranp) ! get number of photon emitted (over the arc length)

        !write(*,*) 'nb photon',ngp*(2*pi*rhop*frac),ranp
        !write(*,*) 'critical energy',ecp

        do k = 1, ranp
          call getEnergy(ecp,table,nt,de)
          totde = totde+de                  ! compute total energy loss
        enddo
        !write(14,*) totde
        !write(*,*) 'Energy loss',dEturn,totde
!        write(*,*) totde/energy,(dx*(dEturn-totde)/energy)
!     &              *sqrt(betax/av_betx)/b1(1,n)
        
        ! damping partitions are jx=1,jy=1,jz=2
        b1(1,n) = b1(1,n)+(dx*(dEturn-totde)/energy)*sqrt(betax/av_betx)
        b1(2,n) = b1(2,n)*(1.0-totde/energy)
        b1(3,n) = b1(3,n)+(dy*(dEturn-totde)/energy)*sqrt(betay/av_bety)
        b1(4,n) = b1(4,n)*(1.0-totde/energy)

        b1(5,n) = b1(5,n)-circ*eta*totde*1E9/(c*energy)
        b1(6,n) = b1(6,n)*(1.0-2.0*totde/energy)
      enddo
!$OMP END DO
!$OMP END PARALLEL

      !close(14)

      end subroutine synchrad

      subroutine getEnergy(ec,t,nt,e)
      implicit none

      double precision :: ub,lb
      double precision :: cst0
      double precision :: ran
      double precision :: ec
      integer :: ip,nt
      double precision, dimension(nt,2) :: t
      double precision :: e
      double precision :: re
      double precision :: mini,maxi,eps

      ub = 0.999d0
      lb = 0.21
      cst0=1.23159     
      call random_number(ran)
      ran = ran*(1.0-1.0e-15)
      mini = 0.0
      maxi = 100000.0
      eps = 1.0e-15

      if(ran.le.lb) then
        e=(ran/cst0)**3*ec
      else if(ran.gt.ub) then
        call bs_invfunc(ran,mini,maxi, eps,re)
        e = re*ec
      else
        call bs_table(t,ran,nt,ip)
        call interpolate(ip,ran,t,nt,re)
        e = re*ec
      endif

      end subroutine getEnergy    
 
   
      double precision function upbranch(x)
      implicit none

      double precision :: cstInf,x

      cstInf=0.239365d0
      upbranch =  1.0d0-cstInf*dexp(-x)/dsqrt(x)

      end function upbranch

  
      subroutine bs_invfunc(y,lo,hi,delta,dp)
      implicit none
      double precision,external :: upbranch
      double precision :: y,lo,hi,delta,dp
      double precision :: fx,x,flo,fhi

      fx = 0.0
      do while(lo.lt.hi)               
        x = (lo + hi)/2.0
        fx = upbranch(x)
        if(fx.lt.y) then
          lo = x + delta                 
        else if(fx.gt.y) then 
          hi = x - delta
        else
          dp = x
          return
        endif
      enddo

      flo = upbranch(lo)
      fhi = upbranch(hi)

      if((fhi-y) .lt. (y - flo)) then
        dp = hi 
        return
      else 
        dp = lo
        return
      endif

      end subroutine bs_invfunc


      subroutine bs_table(t,y,nt,ip)
      implicit none

      integer :: hi,lo,nt,x,ip
      double precision :: y
      double precision, dimension(nt,2) :: t

      lo = 1
      hi = nt

      do while(lo .lt. hi)
        x = (lo + hi) / 2
      if(t(x,2) .lt. y) then
        lo = x + 1
      else if(t(x,2) .gt. y) then  
        hi = x
      else
        ip = x
        return
      endif
      enddo

      if((t(hi,2)-y) .lt. (y - t(lo,2))) then
        ip = hi
        return 
      else 
        ip = lo
        return
      endif

      end subroutine bs_table

    
      subroutine interpolate(ip,p,t,nt,intp)
      implicit none

      integer :: ip,nt
      double precision, dimension(nt,2) :: t
      double precision :: p,intp

      intp = t(ip-1,1) + (t(ip,1)-t(ip-1,1))
     &       *(abs(p)-t(ip-1,2))/(t(ip,2)-t(ip-1,2))

      end subroutine interpolate


!      integer function nelement_sr(filename)
!      implicit none
!      include 'mpif.h'
!      character*7 :: filename
!      double precision,dimension(2) :: dummy

!      open(unit=13,file=filename,status='old')

!      nelement_sr = 0

!      do
!        read(13,*,end=111)dummy(1:2)
!        nelement_sr = nelement_sr+1
!      enddo

!111   continue
!      close(13)
!      end function nelement_sr
    
!      subroutine readTable(nt,t)
!      implicit none
!
!      integer :: i,nt
!      double precision :: re,pe
!      double precision, dimension(nt,2) :: t
!          
!      open(1,file="pofe.in",status="old")
!      do i = 1, nt
!        read(1,*)re,pe
!        t(i,1) = re
!        t(i,2) = pe
!      enddo
!
!      end subroutine readTable
  
      subroutine poissonRandomNumber(lamb,pk)
      implicit none
      include "mpif.h"
      integer :: pk,k
      double precision :: l,p,lamb,u,u1,u2,twopi
         
      l = -lamb
      k = 0
      p = 0.0d0
      twopi = 4.0d0*dasin(1.0d0)

      if(lamb.lt.11) then
        do while(p.gt.l)
          k = k + 1
          call random_number(u)
          p = p + log(u)
        enddo
        pk = k - 1
      else
        call random_number(u1)
        call random_number(u2)
        if(u1.eq.0.0d0) u1 = 1e-18
        pk = int((sqrt(-2.0*log(u1))*dcos(twopi*u2))
     &       *sqrt(dble(lamb))+lamb)
      endif

      end subroutine poissonRandomNumber
