#include <const.h>

      SUBROUTINE INCKICKLR(b1,Np,intensity,plan,sep,rms)

C Long range incoherent kick
C gaussian approx.
C horizontal plan = 1, vertical plan = 2
C
C      use omp_lib
      IMPLICIT NONE
      INTEGER Np,plan
      double precision b1(NCOORD,Np)
      double precision rms(NCOORD)
      double precision sep,intensity

      COMMON/COMBI_BEAM/IBEAM,IBUNCH,rp,ga
      INTEGER IBEAM,IBUNCH
      double precision rp, ga

      COMMON/COMBI_DEBUG/debug
      INTEGER debug

      double precision Lx1,Lx2,Ly1,Ly2,ax1,ay1,ax2,ay2
      double precision hor1,horb2,scaling,tmpexp
      double precision for1,forx1,fory1,kick
 
      INTEGER jj
      horb2 = rms(1)**2+rms(3)**2
      scaling = ((2*intensity*rp) / ga)

      if(plan==1)then
         IF(IBEAM.eq.1) THEN
            Lx1 = sep*rms(1)
            Lx2 = -sep*rms(1)
         ELSE
            Lx1 = -sep*rms(1)
            Lx2 = sep*rms (1)
         ENDIF
         IF(Lx1==0) then
            kick = 0
         ELSE
            for1 = Lx1**2
            forx1 = -(1.0 - exp(-1.0*for1/horb2))/Lx1
            kick = -scaling*forx1
         endif

!$OMP PARALLEL DEFAULT(FIRSTPRIVATE) SHARED(b1)
!$OMP DO SCHEDULE(GUIDED,1000)
         do jj=1,Np
                ax1 = (b1(1,jj) - Lx2 )
                ay1 = b1(3,jj)
                hor1 = ax1**2 + ay1**2
                tmpexp = scaling * 
     &               (1.0 - exp(-1.0*(hor1/horb2)))/hor1

                b1(2,jj) = b1(2,jj) + tmpexp*ax1 - kick
            b1(4,jj) = b1(4,jj) + tmpexp*ay1
         enddo
!$OMP END DO
!$OMP END PARALLEL
         
      else if (plan==2) then
         IF(IBEAM.eq.1) THEN
            Ly1 = sep*rms(3)
            Ly2 = -sep*rms(3)
         ELSE
            Ly1 = -sep*rms(3)
            Ly2 = sep*rms(3)
         ENDIF
        if(Ly1==0) then
            kick = 0
        else
            for1 = Ly1**2
            fory1 = -(1.0 - exp(-1.0*for1/horb2))/ly1
            kick = -scaling*fory1
        endif
         
!$OMP PARALLEL DEFAULT(FIRSTPRIVATE) SHARED(b1)
!$OMP DO SCHEDULE(GUIDED,1000)
         do jj=1,Np
            ax1 = b1(1,jj)
            ay1 = b1(3,jj) - Ly2
            hor1 = ax1**2 + ay1**2
            tmpexp = scaling * 
     &               (1.0 - exp(-1.0*((hor1)/horb2)))/hor1

            b1(2,jj) = b1(2,jj) + tmpexp*ax1
            b1(4,jj) = b1(4,jj) + tmpexp*ay1 - kick
         enddo
!$OMP END DO
!$OMP END PARALLEL
      endif

      RETURN
      END
