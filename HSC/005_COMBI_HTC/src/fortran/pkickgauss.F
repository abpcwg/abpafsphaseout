#include "const.h"      
      SUBROUTINE PKICKGAUSS(b1,Np,b2param)
C Head-on collision (act code 2)

      IMPLICIT NONE
      INTEGER Np

      double precision b1(NCOORD,Np)
      double precision b2param(NPARAM)

      COMMON/COMBI_BEAM/IBEAM,IBUNCH,rp,ga
      INTEGER IBEAM,IBUNCH
      double precision rp, ga

      COMMON/COMBI_DEBUG/debug
      INTEGER debug

      REAL*8 sep,Lx1,Lx2,ax1,ay1,ax2,ay2,hor1,hor2,horb1,horb2
      REAL*8 for1,for2,horx1,horx2,hory1,hory2
      REAL*8 lrx1,lrx2,lry1,lry2,frx1,frx2

C For now:
      REAL Ntot,ik1,ik2
      INTEGER jj

C      write(10,*)'PKICK: ',Np,b2param

      do 370 jj=1,Np 

         ax1 = (b1(1,jj) - b2param(1))
         ay1 = (b1(3,jj) - b2param(3))
         hor1 = sqrt(ax1**2 + ay1**2)
         horb2 = (b2param(2)**2 + b2param(4)**2)
         horx1 = (1.0 - exp(-1.0*((hor1**2)/horb2)))*(ax1
     &        /(hor1**2))
         hory1 = (1.0 - exp(-1.0*((hor1**2)/horb2)))*(ay1
     &        /(hor1**2))
         lrx1 = (((2*b2param(11)*rp) / ga) * horx1)
         lry1 = (((2*b2param(11)*rp) / ga) * hory1)
         b1(2,jj) = b1(2,jj) + lrx1
         b1(4,jj) = b1(4,jj) + lry1


c         if(IBEAM.eq.1)then
C           write(15,*)b1(1,jj),b1(3,jj),lrx1,lry1
C           flush(15)
C         endif

         if(debug.eq.1) then
            write(15,*) 
     &           'after kick HO',lrx1,lry1
     &           ,b1(1,jj),b1(2,jj),b1(3,jj),b1(4,jj)
         endif 
  370 continue

      RETURN
      END
