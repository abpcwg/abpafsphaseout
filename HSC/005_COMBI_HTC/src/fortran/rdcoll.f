      SUBROUTINE RDCOLL(colfil,colfilsize,coly,colx,delqx1,delqx2,delqy1
     &,delqy2,npos1,npos2)
      
      IMPLICIT NONE

      CHARACTER*1600 INLINE
      INTEGER        ipcnt
      INTEGER        NTRX, NTRY
      real*8         xleft1, xleft2, xright1, xright2
      Double precision    DQXT1, DQYT1
      Double precision    DQXT2, DQYT2
      INTEGER colfilsize
      character(len=colfilsize) colfil
      character*16  IPC

C-------from tra1.incl-------------
      INTEGER TRAIN1(3564)
      INTEGER TRAIN2(3564)
      INTEGER npos1, npos2
      INTEGER IP(8)
      INTEGER colx(7128)
      INTEGER coly(7128)
      real*8  delqx1(7128), delqy1(7128)
      real*8  delqx2(7128), delqy2(7128)

C----------------------------------------------------

      INTEGER i,II,inp,i3101,inip,modeip,nip
      integer mleft1, mleft2, mright1, mright2,nbl,nbr
c      integer  npos1,npos2


      OPEN(51,ERR=9999,FILE=colfil,FORM='FORMATTED'                     
     &,STATUS='OLD',ACTION='READ')

C 
C ---  READ IN THE COLLISION FILE
C
      ipcnt = 0
      i3101 = 1
      dqxt1 = 0.0
      dqyt1 = 0.0
      dqxt2 = 0.0
      dqyt2 = 0.0
      ntrx = 0
      ntry = 0

 3100 read(51,*,end=3101) inip, modeip, xleft1, xright1, xleft2, xright2
      write(*,*)'coll',  inip, modeip, xleft1, xright1, xleft2, xright2
    
      mleft1 = xleft1
      mright1 = xright1
      mleft2 = xleft2
      mright2 = xright2

      coly(inip) = abs(modeip)
      write(*,*)'pos,modeip', inip, modeip

C
C HP collisions action code 2
C      

      if(abs(modeip).eq.2) then
             ipcnt = ipcnt + 1
             if(modeip.lt.0) ipc(i3101:i3101) = 'V'
             if(modeip.gt.0) ipc(i3101:i3101) = 'H'
             if(nbl.eq.0) then
                 nbl = mleft1
                 nbr = mright1
             endif
C
C Linear trasfer, rotation in phase space action code 3
C      

      elseif(abs(modeip).eq.3) then
             if(modeip.eq.3) ipc(i3101:i3101) = '.'

C
C Synchrotron motion, rotation in long phase space action code 1
C      

      elseif(abs(modeip).eq.1) then
             if(modeip.eq.1) ipc(i3101:i3101) = '..'

C
C Measurements divice, BPM, action code 8
C      

      elseif(abs(modeip).eq.8) then
C             if(modeip.eq.8) ipc(i3101:i3101) = '..'
             coly(inip) =  8

C
C Ac kicker, action code 9
C

      elseif(abs(modeip).eq.9) then
C             if(modeip.eq.8) ipc(i3101:i3101) = '..'
             coly(inip) =  9

C
C collimator, action code 10
C

      elseif(abs(modeip).eq.10) then
            coly(inip) = 10

C
C Long range collisions without HO, action code 4
C

      elseif(abs(modeip).eq.4) then
             coly(inip) = 4
             ipcnt = ipcnt + 1
             if(modeip.lt.0) ipc(i3101:i3101) = 'v'
             if(modeip.gt.0) ipc(i3101:i3101) = 'h'
             if(nbl.eq.0) then
                 nbl = mleft1
                 nbr = mright1
             endif

C
C Long range collisions without HO, action code 5
C

      elseif(abs(modeip).eq.5) then
             if(modeip.eq.5)  then
                  ipc(i3101:i3101) = 'x'
                  coly(inip) =  5
             endif
             if(modeip.eq.-5) then
                  ipc(i3101:i3101) = 'y'
                  coly(inip) = -5
             endif

      endif

      if((modeip.eq.2).or.(modeip.eq.-2)) then
        do  3001 II = mleft1,mright1
          if(II.ne.0) then
            if(((inip+II).gt.0).and.(modeip.eq.2))  
     &      coly(inip+II)=10*(II/ABS(II))
            if(((inip+II).le.0).and.(modeip.eq.2))  
     &      coly(inip+II+npos1)=10*(II/ABS(II))
            if(((inip+II).gt.0).and.(modeip.eq.-2)) 
     &      coly(inip+II)=20*(II/ABS(II))
            if(((inip+II).le.0).and.(modeip.eq.-2)) 
     &      coly(inip+II+npos1)=20*(II/ABS(II))
          endif
 3001   continue
      endif

      if((modeip.eq.4).or.(modeip.eq.-4)) then
        do  3002 II = mleft1,mright1
          if(II.ne.0) then
            if(((inip+II).gt.0).and.(modeip.eq.4))  
     &      coly(inip+II)=10*(II/ABS(II))
            if(((inip+II).le.0).and.(modeip.eq.4))  
     &      coly(inip+II+npos1)=10*(II/ABS(II))
            if(((inip+II).gt.0).and.(modeip.eq.-4)) 
     &      coly(inip+II)=20*(II/ABS(II))
            if(((inip+II).le.0).and.(modeip.eq.-4)) 
     &      coly(inip+II+npos1)=20*(II/ABS(II))
          endif
 3002   continue
      endif



C Single LR

      if((modeip.eq.5).or.(modeip.eq.-5)) then
       
            if(modeip.eq.5)  
     &      coly(inip)=5                      
           
            if(modeip.eq.-5) 
     &      coly(inip)=-5
           
      endif



      if(modeip.eq.3) then
         delqx1(inip) = xleft1
         delqy1(inip) = xright1
         delqx2(inip) = xleft2
         delqy2(inip) = xright2
         dqxt1 = dqxt1 + delqx1(inip)
         dqyt1 = dqyt1 + delqy1(inip)
         dqxt2 = dqxt2 + delqx2(inip)
         dqyt2 = dqyt2 + delqy2(inip)
         ntrx = ntrx + 1
         ntry = ntry + 1
c         write(*,*)'dqxt1',dqxt1
      endif


      do  3111  inp = 1,8
          if(abs(ip(inp)-inip).lt.20) then
             if(ip(inp).eq.inip) then
                  write(*,*) 'IP ',inp,' confirmed at: ',inip
             else
                  ip(inp) = inip
                  write(*,*) 'IP ',inp,' reset to: ',inip
             endif
          endif
 3111 continue    

      write(*,*) 'i3101', i3101
      i3101 = i3101 + 1
      write(*,*) 'i3101', i3101

      go to  3100

 3101 continue

      write(*,*) 'i3101', i3101
      i3101 = i3101 - 1
      write(*,*) 'i3101', i3101


      do  4444 I = 1,npos1 
         write(*,*) 'inside', i,npos1,colx(i),coly(i)
         COLX(I) = COLY(I)
         write(*,*) 'inside2', i,npos1,colx(i),coly(i)
 4444 continue


 1991 CONTINUE

      go to   777

 9999 write(*,*) 'error opening file collision file'
      stop 9999

 777  continue
      nip = ipcnt
      if((abs(dqxt1-1.0).gt.1.0E-7).or.(abs(dqyt1-1.0).gt.1.0E-7).or.
     & (abs(dqxt2-1.0).gt.1.0E-7).or.(abs(dqyt2-1.0).gt.1.0E-7)) then
           write(*,*) 'WARNING - WARNING - WARNING- WARNING'
           write(*,*) 'Tunes not complete over one turn ...'
           write(*,*) 'Full tunes: ',dqxt1,dqyt1,dqxt2,dqyt2
           write(*,*) 'WARNING - WARNING - WARNING- WARNING'
      endif
      write(*,*) 'Full tunes: ',dqxt1,dqyt1,dqxt2,dqyt2
      RETURN
      END
