      SUBROUTINE READIN(nbx,nbxx,mm,nipx,nipy,
     &  sigi,xilin,nuse,nbux,xi,nkick,
     &  colx,coly,
     &  delqx1,delqx2,delqy1,delqy2,
     &  npos1,npos2,nbch1,nbch2,
     &  xxx1,xxx2,posb1,posb2,
     &  NPART, NUMT)

      IMPLICIT NONE

C     character*(*) filein
C     character*60  fileout
      character*24        colfil, filfila, filfilb
      double precision    qx0, qy0
      double precision    XILIN,XI     
      double precision    SEPHOR, SEPVER
      real*8              sigi
      integer             nbx, nbxx,nuse,nbux    
      integer             nipx, nipy,nkick
      integer             mm
      integer             bunkck
      INTEGER NPART,NUMT

      integer             debug, ppl
      integer             flgsgi
      character*80  INBUFF  

C------variables to be defined for first data set-----

C      integer   takeout,nshow,Np,updk
C      integer nbu,mturn,mturn5,mt,mr5,mt2
C      real   Ntot
C      double precision   rp, ga,PI      
C      double precision  RMS_X, RMS_Y
C      double precision  RMS_XP, RMS_YP
C      double precision  qx,bxa,bxb,qy,bya,byb

C----------------------------------------------------

C----------variable to be defined for rdfill---------

      INTEGER NBCH1,NBCH2
      integer npos1, npos2
      integer posb1(NPOS1), posb2(NPOS1)
      integer xxx1(NPOS1)
      integer xxx2(NPOS1)

C-----------------------------------------------------

C----------variable to be defined for rdcoll---------

      INTEGER COLX(NPOS1)
      integer coly(NPOS1)
      real*8    delqx1(NPOS1), delqy1(NPOS1)
      real*8    delqx2(NPOS1), delqy2(NPOS1)

C----------------------------------------------------


C     fileout = 'out'//filein

C     OPEN(12,ERR=8888,FILE=filein,FORM='FORMATTED'
C    &,STATUS='OLD')
C     OPEN(22,ERR=8889,FILE=fileout,FORM='FORMATTED'
C    &,STATUS='REPLACE')

    1 continue
      read(5,5001,END=99,ERR=999) INBUFF
 5001 format(A80)
      write(6,*) INBUFF

      if(INBUFF(1:10).eq.'collision:') then
         read(INBUFF(11:80),*) colfil 
      elseif(INBUFF(1:8).eq.'filling:') then
         read(INBUFF(9:80),*) filfila, filfilb
      elseif(INBUFF(1:10).eq.'use bunch:') then
         read(INBUFF(11:80),*) nbx, nbxx    
      elseif(INBUFF(1:16).eq.'number of turns:') then
         read(INBUFF(17:80),*) mm     
      elseif(INBUFF(1:11).eq.'separation:') then
         read(INBUFF(12:80),*) sephor,sepver
      elseif(INBUFF(1:14).eq.'number of ips:') then
         read(INBUFF(15:80),*) nipx, nipy
      elseif(INBUFF(1:4).eq.'QX0:') then
         read(INBUFF(5:80),*) qx0          
      elseif(INBUFF(1:4).eq.'QY0:') then
         read(INBUFF(5:80),*) qy0          
      elseif(INBUFF(1:16).eq.'Sigma Intensity:') then
         read(INBUFF(17:80),*) sigi         
         flgsgi=1
      elseif(INBUFF(1:20).eq.'beam-beam parameter:') then
         read(INBUFF(21:80),*) xilin        
      elseif(INBUFF(1:16).eq.'bunches to kick:') then
         read(INBUFF(17:80),*) bunkck       
      elseif(INBUFF(1:21).eq.'nb of macro particle:') then
         read(INBUFF(22:80),*) NPART   
      endif

      go to  1

   99 continue

      nuse = nbx
      nbux = nbxx
      xi = xilin
      nkick = bunkck

      NUMT = 2**mm

      write(6,6001) colfil,filfila,filfilb 
      write(6,*) 'use bunch number: ',nbx, nbxx
      write(6,*) 'number of turns (2**):', mm
      write(6,*) 'number of IPs: ',nipx, nipy
      write(6,*) 'Qx: ',qx0
      write(6,*) 'Qy: ',qy0
      write(6,*) 'Sigma Intensity: ',sigi
      write(6,*) 'beam-beam parameter: ',xilin
      write(6,*) 'Nb of macro particle : ',NPART

 6001 format(1X,'files: ',A20,3X,A20)

C     close(22)

C------variables defined in the readin routine -----

C      takeout = 0
      debug = 0
C      nshow = 1000
      
C Np Number of macro particles in a bunch
C Ntot total number of charged particles

C  RHIC parameters

C      Np = 10000
C      updk = 1
C      Ntot = 1.40E+11
C      ppl = 0    
C      rp = 1.551E-18
C      ga = (2.7E+04)/938
C      nbu = NBX
C      mturn = 2**mm
C      mturn5 = mturn/2
C      mt  = mturn
C      mt2 = mm
C      mr5 = mturn5
C      pi = 4.0*DATAN(1.0D0)
C      RMS_X = 3.0E-03
C      RMS_Y = 3.0E-03
C      RMS_XP = 3.0E-03/1.0
C      RMS_YP = 3.0E-03/1.0 

C LHC parameters
c      Np = 10000
c      updk = 0
c      Ntot = 1.05E+11
c      ppl = 0    
c      rp = 1.551E-18
c      ga = (7E+06)/938
c      nbu = NBX
c      mturn = 2**mm
c      mturn5 = mturn/2
c      mt  = mturn
c      mt2 = mm
c      mr5 = mturn5
c      pi = 4.0*DATAN(1.0D0)
c      RMS_X = 16E-06
c      RMS_Y = 16E-06
c      RMS_XP = 16E-06/0.5
c      RMS_YP = 16E-06/0.5 

C      bxa = 0.5
C      bxb = 0.5
C      bya = 0.5
C      byb = 0.5

C-----------------------------------------------------------
    
C     OPEN(11,FILE='coll.out')       
C     if(ppl.eq.1) then
C        OPEN(21,FILE='lhctt.out')
C        OPEN(14,FILE='lhctt.col')
C     endif

      if(debug.eq.1) then
         OPEN(15,FILE='lhctt.deb')
      endif

      write(*,*) "calling rdfill"
      CALL RDFILL(NBX,NBCH1,NBCH2,filfila,filfilb,
     &            xxx1,xxx2,npos1,npos2,posb1,posb2)
      write(6,*)'nbch1,nbch2',nbch1,nbch2,npos1,npos2
      write(*,*) "calling rdcoll"
      CALL RDCOLL(colfil,coly,colx,delqx1,delqx2,delqy1,
     &            delqy2,npos1,npos2)

      RETURN

  999 STOP 'READIN: ERROR READING INPUT FILE'
 9999 STOP 'READIN: ERROR OPENING OUTPUT FILE'

      end
