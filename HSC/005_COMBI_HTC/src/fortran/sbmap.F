#include <const.h>

      SUBROUTINE sbmap(part,Np,b2param,cphi,cphi2,nsli,f,jsli,b1,         &
     &Moments1,Moments2,star)
      
      IMPLICIT NONE
      
      integer Np,nsli,jsli,part,plan
      double precision b1(NCOORD,Np)
      double precision bbfx,bbfy,bbgx,bbgy,bcu,cphi,                    &
     &dum,f,s,sepx,sepx0,sepy,sepy0,sfac,sinth,sinthp,sp,star,sx,       &
     &sy,cphi2,half,four,two
      double precision b2param(NPARAM)
      double precision Moments1(5*nsli),Moments2(11*nsli)
      dimension dum(13)
      dimension star(3,nsli)
      parameter(half = 0.5d0, two = 2.0d0, four=4.0d0)
      
      COMMON/COMBI_BEAM/IBEAM,IBUNCH,rp,ga
      INTEGER IBEAM,IBUNCH
      double precision rp, ga
      
      s=(b1(5,part)*0.3-star(3,jsli))*half
!      if(part.eq.1.and.ibeam.eq.1)then
!      write(*,*)'sloc',part,s,b1(5,part)*0.3,star(3,jsli)
!      endif
      sp=s/cphi

	dum(1)=(b2param(2)**2)+(two*0)*sp+(b2param(6)**2)*sp**2
      dum(2)=(b2param(4)**2)+(two*0)*sp+(b2param(8)**2)*sp**2
      !if(part.eq.1.and.ibeam.eq.1)then
      !write(*,*)'s',jsli,sp,dum(1)
      !endif
      !dum(1)=b2param(2)**2
      !dum(2)=b2param(4)**2
      sx=dum(1)!b2param(2)**2
      sy=dum(2)!b2param(4)**2
           
      sepx=(b1(1,part)+b1(2,part)*s)-star(1,jsli)-b2param(1)                   
      sepy=(b1(3,part)+b1(4,part)*s)-star(2,jsli)-b2param(3)
      
      plan = 1
         
      call pkick6D(b1,Np,b2param,plan,nsli,jsli,dum,star,part,cphi)

      !call lrdpkickjav(b2param,star,nsli,jsli,part)   
         
         
!           if(sx.gt.sy) then
!              call bbf(sepx,sepy,sx,sy,bbfx,bbfy,bbgx,bbgy)
!           else
!              call bbf(sepy,sepx,sy,sx,bbfy,bbfx,bbgy,bbgx)
!           endif
          

!            bbfx=f*bbfx
!            bbfy=f*bbfy
!            bbgx=f*bbgx
!            bbgy=f*bbgy

           

           
!          if(IBEAM.eq.1)then
!           b1(1,part)=b1(1,part)!+s*bbfx
!           b1(2,part)=b1(2,part)+bbfx
!           b1(3,part)=b1(3,part)!+s*bbfy
!           b1(4,part)=b1(4,part)+bbfy
!          endif
!          if(IBEAM.eq.2)then
!          b1(1,part)=b1(1,part)!+s*bbfx
!          b1(2,part)=b1(2,part)-bbfx
!          b1(3,part)=b1(3,part)!+s*bbfy
!          b1(4,part)=b1(4,part)+bbfy
!          endif

         
       return
      end
      
      
