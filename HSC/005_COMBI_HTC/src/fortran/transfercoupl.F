#include <const.h>

      SUBROUTINE TRANSFERCOUPL(b1,Np,qxx,qyy,bx,by,phi)

C Matrix transfer  (act code 3)
C bx,by : array of dim 2, {beta before,beta after}

      IMPLICIT NONE
      INTEGER Np
      double precision qxx, qyy

      double precision  b1(NCOORD,Np)

      COMMON/COMBI_BEAM/IBEAM,IBUNCH
      INTEGER IBEAM,IBUNCH

      COMMON/COMBI_DEBUG/debug
      INTEGER debug

      INTEGER jj

      double precision bx(2),by(2)
      double precision a(6,6),b(6,6)
      double precision x(6),y(6)
      
      INTEGER i,j
      double precision pi,r,c,phi
      double precision chrx,chry,qs
      pi = 4.0*DATAN(1.0D0)
      
      r = cos(phi)
      c = sin(phi)
      
C     chroma      
      chrx=0.0
      chry=0.0

      DO J=1,6
         DO I=1,6
            A(I,J)=0.
            B(I,J)=0.
         ENDDO
      ENDDO


      do 390 jj=1,Np

         call btoxpar(b1,Np,x,jj)

C         if(jj.eq.1)then
C            IF(IBEAM.EQ.1)WRITE(21,*)b1(1,jj),b1(2,jj),b1(3,jj),b1(4,jj)
C            IF(IBEAM.EQ.2)WRITE(22,*)b1(1,jj),b1(2,jj),b1(3,jj),b1(4,jj)
C         endif

c     qxx = (qx0 - (nip*0.5))*delqx(kk)
c     qyy = (qy0 - (nip*0.5))*delqy(kk)


C       TODO improve speed by doing the mult by hand

         a(1,1) =  cos(2*pi*(qxx+b1(6,jj)*chrx))*(sqrt(bx(2)/bx(1)))
         a(1,2) =  sin(2*pi*(qxx+b1(6,jj)*chrx))*(sqrt(bx(2)*bx(1)))
         a(2,1) = -sin(2*pi*(qxx+b1(6,jj)*chrx))/(sqrt(bx(2)*bx(1)))
         a(2,2) =  cos(2*pi*(qxx+b1(6,jj)*chrx))*(sqrt(bx(1)/bx(2)))
         a(3,3) =  cos(2*pi*(qyy+b1(6,jj)*chry))*(sqrt(by(2)/by(1)))
         a(3,4) =  sin(2*pi*(qyy+b1(6,jj)*chry))*(sqrt(by(2)*by(1)))
         a(4,3) = -sin(2*pi*(qyy+b1(6,jj)*chry))/(sqrt(by(2)*by(1)))
         a(4,4) =  cos(2*pi*(qyy+b1(6,jj)*chry))*(sqrt(by(1)/by(2)))
         a(5,5) = 1
         a(6,6) = 1 
         
         b(1,1) = r
         b(1,3) = c 
         b(2,2) = r
         b(2,4) = c
         b(3,1) = -c
         b(3,3) = r
         b(4,2) = -c
         b(4,4) = r
         b(5,5) = 1
         b(6,6) = 1

         call dmat(x,b,y,6)
         call dmat(y,a,x,6)
         
         b(1,3) = -c 
         b(2,4) = -c
         b(3,1) = c
         b(4,2) = c
         b(5,5) = 1
         b(6,6) = 1

         call dmat(x,b,y,6)

         call ytobpar(b1,Np,y,jj)

 390  continue

C       WRITE(*,*)'Transfer in after',b1(1,1) 

C      IF(IBEAM.EQ.1)WRITE(91,*)b1(1,1),b1(2,1),b1(3,1),b1(4,1)
C      IF(IBEAM.EQ.2)WRITE(92,*)b1(1,1),b1(2,1),b1(3,1),b1(4,1)

      RETURN
      END
