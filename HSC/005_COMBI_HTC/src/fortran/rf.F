#include <const.h>

      subroutine rf(b1,Np,qss,ratio) 

C Synchrotron oscillations (act code 1)

      IMPLICIT NONE

      INTEGER Np,Nturn,JJ
      double precision qss,ratio
      double precision  b1(NPARAM,Np)
      double precision pi
      pi = 4.0*DATAN(1.0D0)
 
      do 391 jj=1,Np
         b1(5,jj) = cos(2*pi*qss)*b1(5,jj) + 
     &       sin(2*pi*qss)*ratio*b1(6,jj)
         b1(6,jj) = cos(2*pi*qss)*b1(6,jj) -
     &       sin(2*pi*qss)/ratio*b1(5,jj)

 391  continue


      return
      end
