      subroutine initconst(IIBEAM,IIBUNCH,energy,mass,charge)
      
      IMPLICIT NONE
      integer IIBEAM,IIBUNCH
      double precision energy,mass,charge
      
      COMMON/COMBI_BEAM/IBEAM,IBUNCH,rp,ga
      INTEGER IBEAM,IBUNCH
      double precision rp, ga, c
      COMMON/COMBI_NOISE/ phase1,phase2
      double precision phase1,phase2
      
      IBEAM=IIBEAM
      IBUNCH=IIBUNCH
C this is not a mistake. The charge is not squared
C (in case charge!=0 rp is no longer the classical radius)
      rp = 1.534695381E-18*charge*0.938272/mass
      ga = energy/mass
      c = 299792458.0
      
      phase1 = 0.0
      phase2 = 0.0
      return
      end
