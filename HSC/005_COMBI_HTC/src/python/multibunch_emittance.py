import numpy as np
import matplotlib.pyplot as plt
from parseOutput import *
import os

def checkBeamParameters(inputdir,lim=0.1,makePlots = False):
    maxTurn = -1;
    for beam in [1,2]:
        bunch = 1;
        while True:
            fileName = inputdir+"B"+str(beam)+"b"+str(bunch)+".bparam";
            if not os.path.exists(fileName):
                break;
            #print 'Beam ',beam,', bunch ',bunch;
            bpData = parseBeamParameter(fileName);
            eLim = 1.0+lim;
            intLim = (1.0-lim)*bpData[10][0];
            for i in range(len(bpData[8])):
                if bpData[8][i] > eLim or bpData[9][i] > eLim or bpData[15][i] > eLim  or bpData[10][i] < intLim:
                    if maxTurn == -1 or i < maxTurn:
                        maxTurn = i;
                        break;
            if maxTurn > len(bpData[8]) or maxTurn == -1:
                maxTurn = len(bpData[8]);
            if makePlots:
                plt.figure(0);
                plt.plot(np.arange(len(bpData[8])),bpData[8]);
                plt.xlabel('Turn');plt.ylabel('X emit');
                plt.figure(1);
                plt.plot(np.arange(len(bpData[9])),bpData[9]);
                plt.xlabel('Turn');plt.ylabel('Y emit');
                plt.figure(2);
                plt.plot(np.arange(len(bpData[15])),bpData[15]);
                plt.xlabel('Turn');plt.ylabel('Z emit');
                plt.figure(3);
                plt.plot(np.arange(len(bpData[10])),bpData[10]);
                plt.xlabel('Turn');plt.ylabel('Intensity');
            bunch = bunch + 1;
    if makePlots:
        for figure in [0,1,2,3]:
            plt.figure(figure);
            plt.plot([maxTurn,maxTurn],[0,10],'-k');
    return maxTurn;

if __name__ == '__main__':
    inputdir = '../../output/';
    maxTurn = checkBeamParameters(inputdir, makePlots = True);
    print maxTurn;
    plt.show();
