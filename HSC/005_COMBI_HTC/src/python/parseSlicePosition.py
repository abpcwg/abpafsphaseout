
def parse(fileName,redFact=1):
    myFile = open(fileName,'r');
    data = [];
    size = -1;
    lineCount = 0;
    for line in myFile.readlines():
        if lineCount%redFact == 0:
            tokens = line.rstrip().split(' ');
            if size == -1:
                size = len(tokens);
                #print size,'slices'
            if len(tokens) == size:
                tmpData = [];
                for i in range(size):
                    tmpData.append(float(tokens[i]));
                data.append(tmpData);
        lineCount = lineCount + 1;
    myFile.close();
    return data;
     
if __name__ == '__main__':
    import numpy as np
    import matplotlib.pyplot as plt
    #inputdir = '../../output/test/';
    inputdir = '/media/Elements/CERN/COMBIOutput/BBZ/LRZD/test_lowNPART/IP1-noise/colored/10.0_4E-2_7_2E-4_0.311_1000/';
    dataH = parse(inputdir+'B1b1_H.spos');
    dataV = parse(inputdir+'B1b1_V.spos');
    dataS = parse(inputdir+'B1b1_S.spos');
    avgPosH = [];
    for i in range(len(dataH)):
    #for i in range(50000,51000,100):
        plt.figure(0);
        plt.plot(dataS[i],dataH[i]);
        plt.xlabel('s');plt.ylabel('x');
        plt.figure(1);
        plt.plot(dataS[i],dataV[i]);
        plt.xlabel('s');plt.ylabel('y');
        plt.figure(2);
        plt.plot(dataH[i],dataV[i]);
        plt.xlabel('x');plt.ylabel('y');
        
        avgPosH.append(np.average(dataH[i]));
    plt.figure(3);
    plt.plot(np.arange(len(avgPosH)),avgPosH);
    plt.show();
