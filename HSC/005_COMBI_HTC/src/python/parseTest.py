import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import csv,math
from pylab import rfft,polyfit,polyval
from parseOutput import *

def parse(fileName,nColumn=2):
    data = [];
    for i in np.arange(nColumn):
        data.append([]);
    reader = csv.reader(open(fileName, 'rb'), delimiter=',');
    for row in reader:
        if len(row)==nColumn:
            for i in np.arange(nColumn):
                try:
                    value = float(row[i])
                    if math.isnan(value):
                        print row[0],row[1];
                    data[i].append(value);
                except ValueError:
                    print row[0],row[1]
    return data;

def testLR():
    data = parse("../../testLR_H.csv");
    plt.plot(data[0],data[1],label='H');
    data = parse("../../testLR_V.csv");
    plt.plot(data[0],data[1],label='V');
    plt.legend();

def testHO():
    data = parse("../../testHO_H.csv");
    plt.plot(data[0],data[1]);
    #data = parse("../../testHO_V.csv");
    #plt.plot(data[0],data[1]);
    
def testTransfer():
    data = parse("../../testTransfer.csv",6);
    plt.figure(10);
    plt.plot(data[0],data[1],'x',label = 'H');
    plt.plot(data[2],data[3],'x',label = 'V');
    plt.legend(loc=0);
    plt.figure(11);
    plt.plot(data[4],data[5],'x',label = 'L');
    plt.legend(loc=0);
    data = parseBeamParameter("../../testTransfer.bparam");
    makeLongitudinalParameterPlots(data,startFFT=0);
    #makeStandardPlots(data,fitPlot=False,sussix=False,startFFT=0000,spectrogram=False);

if __name__ == "__main__":
    testLR();
    plt.grid();
    plt.show();
