import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft

#copied from HARPY
def jacobsen(samples):
    dft_values = fft(samples)
    k = np.argmax(np.abs(dft_values))
    n = len(samples)
    r = dft_values
    delta = np.tan(np.pi / n) / (np.pi / n)
    kp = (k + 1) % n
    km = (k - 1) % n
    delta = delta * np.real((r[km] - r[kp]) / (2 * r[k] - r[km] - r[kp]))
    return (k + delta) / n

if __name__ == "__main__":
    npart = 42

    emit = 2.5E-6
    beta = 0.15
    gamma = 7000/0.938
    sig = np.sqrt(beta*emit/gamma)
    sigp = np.sqrt(emit/(gamma*beta))

    jacTunex = np.zeros(npart,dtype=float)
    jacTuney = np.zeros_like(jacTunex)
    fftTunex = np.zeros_like(jacTunex)
    fftTuney = np.zeros_like(jacTunex)
    jx = np.zeros_like(jacTunex)
    jy = np.zeros_like(jacTunex)
    for part in range(npart):
        data = np.loadtxt('../../output/B1b1_p'+str(part)+'.testpart',delimiter=' ',usecols=range(4))
        #plt.plot(data[:,2],data[:,3],'x')
        jxs = (data[:,0]/sig)**2+(data[:,1]/sigp)**2
        plt.plot(np.arange(len(jxs)),jxs)

        jacTunex[part] = jacobsen(data[:,0])
        jacTuney[part] = jacobsen(data[:,2])
        jx[part] = np.sqrt((data[0,0]/sig)**2+(data[0,1]/sigp)**2)
        jy[part] = np.sqrt((data[0,2]/sig)**2+(data[0,3]/sigp)**2)

    plt.figure()
    plt.plot(jacTunex,jacTuney,'x')

    plt.figure()
    plt.plot(jx,jy,'x')

    plt.show();
