import numpy as np
import matplotlib.pyplot as plt
import csv
import plotRoutines as pltRoutines
import fit as fit

#file column layout
# X F1(X) F2(X)

def parse(fileName):
    data = [];
    for i in np.arange(3):
        data.append([]);
    reader = csv.reader(open(fileName, 'rb'), delimiter=' ');
    for row in reader:
        count = 0;
        for i in np.arange(len(row)):
            if count<3:
                if len(row[i])!=0:
                    data[count].append(float(row[i]));
                    count=count+1;
    return data;

def makeHistogramPlot(data,weights=None,bins=100,color='r',label='raw',fitPlot=True):
    events,edges = np.histogram(data,weights=weights,bins=bins);
    bin = np.arange(len(events),dtype=float);
    for i in np.arange(len(events)):
        bin[i] = (edges[i]+edges[i+1])/2.0;
    [x,fx],mu,sigma,myMax = fit.gaussianFit(events,bin);
    plt.plot(bin,events,label=label,color=color);
    if fitPlot:
        plt.plot(x,fx,label='fit',color='b');
    plt.ylabel('count');
    plt.legend();
    return mu,sigma,myMax,edges

if __name__=="__main__":
    data = parse("../../fort.17");
    fig = plt.figure(0);
    fig.add_subplot(211);
    nBin = 1000;
    makeHistogramPlot(data[0],data[2],bins=nBin);
    fig.add_subplot(212);
    makeHistogramPlot(data[1],data[2],bins=nBin);
    plt.show();

