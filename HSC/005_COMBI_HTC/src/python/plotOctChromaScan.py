import pickle
import numpy as np
import matplotlib.pyplot as plt

if __name__=="__main__":
    myFile = open('octChromaScan_noBB_dGain0.02.pkl','rb')
    [IOcts,chromas,maxXs,maxEmitX,maxYs,maxEmitY] = pickle.load(myFile,encoding='latin-1')
    myFile.close()

    thres = np.zeros_like(chromas)
    thresLowerErr = np.zeros_like(chromas)
    thresUpperErr = np.zeros_like(chromas)
    for i in range(len(thres)):
        plt.figure(0)
        plt.plot(IOcts,maxXs[:,i],'-x',label=str(chromas[i]))
        plt.figure(2)
        plt.plot(IOcts,maxEmitX[:,i],'-x',label=str(chromas[i]))

        stable = np.argwhere(np.logical_and(maxXs[:,i]<1E-7,maxEmitX[:,i]<1.2))
        if len(stable) == 0:
            thres[i] = IOcts[-1]
            thresLowerErr[i] = 0
            thresUpperErr[i] = 1E3
        else:
            iStable = stable[0,0]
            if iStable == 0 :
                thres[i] = IOcts[0]
                thresLowerErr[i] = 0.0
                thresUpperErr[i] = IOcts[0]
            else:
                thres[i] = (IOcts[iStable-1]+IOcts[iStable])/2
                thresLowerErr[i] = IOcts[iStable-1]-thres[i]
                thresUpperErr[i] = thres[i]-IOcts[iStable]
                print(chromas[i],iStable,thres[i],thresLowerErr[i],thresUpperErr[i])

    plt.figure(1)
    plt.errorbar(chromas,thres*1.0/1.2,yerr=[thresLowerErr*1.0/1.2,thresUpperErr*1.0/1.2],label='Dip+Quad')

    myFile = open('octChromaScan_noBB_noQuad_dGain0.02.pkl','rb')
    [IOcts,chromas,maxXs,maxEmitX,maxYs,maxEmitY] = pickle.load(myFile,encoding='latin-1')
    myFile.close()


    thres = np.zeros_like(chromas)
    thresLowerErr = np.zeros_like(chromas)
    thresUpperErr = np.zeros_like(chromas)
    for i in range(len(thres)):
        plt.figure(0)
        plt.plot(IOcts,maxXs[:,i],'-x',label=str(chromas[i]))
        plt.figure(2)
        plt.plot(IOcts,maxEmitX[:,i],'-x',label=str(chromas[i]))

        stable = np.argwhere(np.logical_and(maxXs[:,i]<1E-7,maxEmitX[:,i]<1.2))
        print(stable)
        if len(stable) == 0:
            thres[i] = IOcts[-1]
            thresLowerErr[i] = 0
            thresUpperErr[i] = 1E3
        else:
            iStable = stable[0,0]
            if iStable == 0 :
                thres[i] = IOcts[0]
                thresLowerErr[i] = 0.0
                thresUpperErr[i] = IOcts[0]
            else:
                thres[i] = (IOcts[iStable-1]+IOcts[iStable])/2
                thresLowerErr[i] = IOcts[iStable-1]-thres[i]
                thresUpperErr[i] = thres[i]-IOcts[iStable]
                print(chromas[i],iStable,thres[i],thresLowerErr[i],thresUpperErr[i])

    plt.figure(1)
    plt.errorbar(chromas,thres*1.0/1.2,yerr=[thresLowerErr*1.0/1.2,thresUpperErr*1.0/1.2],label='Dip')

    plt.xlabel('Chromaticity')
    plt.ylabel('Octupole threshold [A]')
    plt.grid()
    plt.legend(loc=0)
    plt.xlim([-1,18])
    plt.ylim([0.0,300.0])

    plt.figure(0)
    plt.grid()
    plt.legend(loc=0)

    plt.figure(2)
    plt.grid()
    plt.legend(loc=0)

    plt.show()
