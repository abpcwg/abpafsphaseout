import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from Sussix import sussix_inp,sussixBS
from parseOutput import parseBeamParameter,t2f,c2db,sort
from pylab import rfft

def getSussixSpectra(x,px,y,py):
    sussix_inp(ir=0,turns=len(x),tunex=0.695,tuney=0.685,istun=1,narm=1000);
    out = sussixBS(x,y,px,py);
    sort(out.oy,out.ay);
    out.oy = 1.0-out.oy;
    retVal = [];
    for i in np.arange(0.668,0.6725,0.000001):
        retVal.append(np.interp(i,out.oy,out.ay));
    return retVal;

def getSpectra(tbtData):
    return c2db(rfft(tbtData));
                

if __name__ == '__main__':
    bpData = parseBeamParameter('/home/xbuffat/COMBI results/hfmm/RHIC/sin/6703/1E-3/B1b1.bparam',nparam=11);
#    bpData = parseBeamParameter('/home/xbuffat/COMBI results/hfmm/RHIC/white/shit/25E-4Noise/25/B1b1.bparam');
    span = 5000;
    step = 100;
    print len(bpData[2]),float(len(bpData[0]))/step;
    SSXspectras = [];
    FFTspectras = [];
    for i in np.arange(0,len(bpData[0])-span,step):
        #spectra = getSussixSpectra(bpData[0][i:i+span],bpData[4][i:i+span],bpData[2][i:i+span],bpData[6][i:i+span]);
        #SSXspectras.append(spectra);
        spectra = getSpectra(bpData[2][i:i+span]);
        FFTspectras.append(spectra);
    #plt.figure(0);
    #plt.imshow(np.flipud(SSXspectras), aspect='auto', extent=[0.668,0.6725,span/2,len(bpData[0])-span/2]);
    #plt.xlabel('Tune spectrum');plt.ylabel('Time [turn]');
    plt.figure(1);
    plt.imshow(np.fliplr(np.flipud(FFTspectras)), aspect='auto', extent=[0.5,1.0,0.0,len(bpData[0])]);
    plt.xlabel('Tune spectrum');plt.ylabel('Time [turn]');
    plt.show();
    
