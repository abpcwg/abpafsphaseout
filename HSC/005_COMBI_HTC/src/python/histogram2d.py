import numpy as np
import matplotlib.pyplot as plt
import csv
import plotRoutines as plotRoutines
from mpl_toolkits.mplot3d import Axes3D

#file column layout
# X Y F(X,Y)

def parse(fileName,size=3):
    data = [];
    for i in np.arange(size):
        data.append([]);
    reader = csv.reader(open(fileName, 'rb'), delimiter=' ');
    for row in reader:
        count = 0;
        for i in np.arange(len(row)):
            if count<size:
                if len(row[i])!=0:
                    data[count].append(float(row[i]));
                    count=count+1;
    return data;

def binnedAverage2d(x,y,z,nbin=100):
    xmin = min(x);
    xmax = max(x);
    xstep = (xmax-xmin)/nbin;
    ymin = min(y);
    ymax = max(y);
    ystep = (ymax-ymin)/nbin;
    X = np.arange(xmin,xmax,xstep);
    Y = np.arange(ymin,ymax,ystep);
    X,Y = np.meshgrid(X,Y);
    Z = np.zeros_like(X);
    nbperbin = np.zeros_like(X);
    for i in np.arange(len(x)):
        I = int((x[i]-xmin)/xstep);
        J = int((y[i]-ymin)/ystep);
        if I == nbin:
            I = I-1;
        if J == nbin:
            J = J-1;
        Z[I,J] = Z[I,J]+z[i];
        nbperbin[I,J] = nbperbin[I,J] + 1;
    for i in np.arange(np.shape(X)[0]):
        for j in np.arange(np.shape(X)[1]):
            if nbperbin[i,j] != 0 :
                Z[i,j] = Z[i,j]/nbperbin[i,j];
#                Z[i,j] = nbperbin[i,j];
    return X,Y,Z
        

if __name__=="__main__":
    data = parse("../../fort.19");
    nbin = 200;

    X,Y,Z = binnedAverage2d(data[0],data[1],data[2],nbin=nbin);

    fig = plt.figure(0);
    ax = Axes3D(fig,elev=45,azim=45)
    surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=plt.get_cmap('jet'),linewidth=0, antialiased=False)
    ax.set_xlabel("x");ax.set_ylabel("y");ax.set_zlabel("z");
    
    fig = plt.figure(1)
    plotRoutines.multicoloredDots(data[0],data[1],data[2])
    plt.xlabel('x');plt.ylabel('y');
    clbar = plt.colorbar();
    clbar.set_label('z');
    plt.show();
