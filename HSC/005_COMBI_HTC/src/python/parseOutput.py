import numpy as np
import matplotlib.pyplot as plt
import os,csv,gzip
from pylab import rfft,polyfit,polyval
import plotRoutines as pltRoutines
#import fit as fit
#from Sussix import sussix_inp,sussixBS

# beam parameters ['X','DeltaX','Y','DeltaY','PX','DeltaPX','PY','DeltaPY','EmitX','EmitY','intensity','S','DeltaS','dp/p','Delta dp/p','emitS'];
# beam ['X','PX','Y','PY','S','dp/p','DeltaI'];

def parseBeamParameter(fileName,filter=1,lower=0,upper=-1,nparam=18):
    data = [];
    for i in np.arange(nparam):
        data.append([]);
    coreName, extension = os.path.splitext(fileName)
    if '.gz' in extension:
        myFile = gzip.open(fileName,'r');
        gzipped = True;
    else:
        myFile = open(fileName,'r');
        gzipped = False;
    count = 0;
    for line in myFile.readlines():
        if gzipped:
            row = line.decode('latin-1').rstrip().split(' ');
        else:
            row = line.rstrip().split(' ');
        isLower = 1;
        if upper != -1:
            if count>=upper:
                isLower = 0;
        if count%filter ==0 and count>=lower and isLower:
            if len(row)==nparam:
                for i in np.arange(nparam):
                    data[i].append(float(row[i]));
            else:
                print("ERROR at line",count,':',row,'Skipping line');
        count=count+1;
    myFile.close();
    return data;
   
def parseBeamDistribution(fileName):
    data = [];
    for i in np.arange(2):
        data.append([]);
    reader = csv.reader(open(fileName, 'rb'), delimiter=',');
    for row in reader:
        if len(row)>=2:
            for i in np.arange(2):
                data[i].append(float(row[i]));
    return data;

def parseTransverseBeamDistribution(fileName):
    nLine = 0;
    reader = csv.reader(open(fileName, 'rb'), delimiter=',');
    for row in reader:
        if len(row)!=3:
            print('ERROR while parsing, 3 columns required');
            return;
        nLine += 1;
    nStep = np.sqrt(nLine);
    X = np.zeros((nStep,nStep));
    Y = np.zeros((nStep,nStep));
    Z = np.zeros((nStep,nStep));
    reader = csv.reader(open(fileName, 'rb'), delimiter=',');
    i = 0;
    j = 0;
    for row in reader:
        X[i][j] = float(row[0]);
        Y[i][j] = float(row[1]);
        Z[i][j] = float(row[2]);
        if j < nStep-1:
            j += 1;
        else:
            i += 1;
            j = 0;
        
    return X,Y,Z;

def t2f(t):
    n=len(t)/2+1
    fs=1/(t[1]-t[0])
    return np.arange(0.,n)*fs/len(t)
#    return 1.0-np.arange(0.,n)*fs/len(t)

def c2db(c):
    """complex signal to decibel"""
    return 20.*np.log10(abs(c))

def sort(first,second):
    sortDone = False;
    while not sortDone:
        sortDone = True;
        for i in np.arange(len(first)-1):
            if first[i] < first[i+1]:
                tmp = first[i];
                first[i] = first[i+1];
                first[i+1] = tmp;
                tmp = second[i];
                second[i] = second[i+1];
                second[i+1] = tmp;
                sortDone = False;

def getFitError(a,b,x,y):
    retVal = 0;
    xi = 0;
    xavg = np.average(x);
    for i in np.arange(len(x)):
        retVal+=(a*x[i]+b-y[i])*(a*x[i]+b-y[i]);
        xi+=(x[i]-xavg)*(x[i]-xavg);
    retVal = np.sqrt(retVal/((len(x)-2)*xi));
    return retVal;
    
def toPercentPerHour(value):
    return value*100*11245*3600;
    
def toSeconds(turns):
    return [turns[k]/11245.0 for k in np.arange(len(turns))];
    
def amplitude(data,span=10,step = 1):
    retVal = [];
    retVal.append([]);
    retVal.append([]);
    for i in np.arange(span,len(data),step):
        retVal[0].append(i-span/2);
        retVal[1].append((np.max(data[i-span:i+1])-np.min(data[i-span:i+1]))/2);
    return retVal;

def makeStandardPlots(bpData,fitPlot=False,startFFT=0,endFFT=-1,startFit=0,endFit=-1,spectrogram=True,sussix=False) :
    if(endFit==-1 or endFit > len(bpData[0])):
        endFit = len(bpData[0]);
    if(endFFT == -1 or endFFT > len(bpData[0])):
        endFFT = len(bpData[0]);
    fig = plt.figure(0);
    fig.add_subplot(311);
    if len(bpData[0]) > 50:
        ampl = amplitude(bpData[0],10);
        (a,b) = polyfit(ampl[0],ampl[1],1);
        plt.plot(ampl[0],ampl[1],'-r');
        plt.plot(np.arange(len(bpData[0])),polyval([a,b],np.arange(len(bpData[0]))),'-k');
    plt.plot(np.arange(len(bpData[0])),bpData[0],'-b');
#    pltRoutines.multicoloredDots(bpData[0],bpData[4],np.arange(len(bpData[0])));
    plt.xlabel('x');#plt.ylabel('px');
    fig.add_subplot(312);
    if len(bpData[2]) > 50:
        ampl = amplitude(bpData[2],10);
        (a,b) = polyfit(ampl[0],ampl[1],1);
        plt.plot(ampl[0],ampl[1],'-r');
        plt.plot(np.arange(len(bpData[2])),polyval([a,b],np.arange(len(bpData[2]))),'-k');
    plt.plot(np.arange(len(bpData[2])),bpData[2],'-b');
#    pltRoutines.multicoloredDots(bpData[2],bpData[6],np.arange(len(bpData[2])));
    plt.xlabel('y');#plt.ylabel('py');
    fig.add_subplot(313);
    if len(bpData[11]) > 5000 :
        ampl = amplitude(bpData[11],1000);
        (a,b) = polyfit(ampl[0],ampl[1],1);
        plt.plot(ampl[0],ampl[1],'-r');
        plt.plot(np.arange(len(bpData[11])),polyval([a,b],np.arange(len(bpData[2]))),'-k');
    plt.plot(np.arange(len(bpData[11])),bpData[11],'-b');
#    pltRoutines.multicoloredDots(bpData[2],bpData[6],np.arange(len(bpData[2])));
    plt.xlabel('t');#plt.ylabel('py');
#    plt.subplots_adjust(bottom=0.1, right=0.8, top=0.9)
#    cax = plt.axes([0.85, 0.1, 0.075, 0.8])
#    plt.colorbar(cax=cax);
    fig = plt.figure(1);
    fig.add_subplot(311);
    plt.plot(np.arange(len(bpData[1])),bpData[1],label='$\Delta$x');
    plt.plot(np.arange(len(bpData[3])),bpData[3],label='$\Delta$y');
    plt.xlabel('Turn');plt.ylabel('sigma');
    plt.legend(loc=0);
    fig.add_subplot(312);
    plt.plot(np.arange(len(bpData[5])),bpData[5],label='$\Delta$px');
    plt.plot(np.arange(len(bpData[7])),bpData[7],label='$\Delta$py');
    plt.xlabel('Turn');plt.ylabel('sigma');
    plt.legend(loc=0);
    fig.add_subplot(313);
    plt.plot(np.arange(len(bpData[12])),bpData[12],label='$\Delta$t');
    plt.twinx();
    plt.plot(np.arange(len(bpData[14])),bpData[14],label='$\Delta$dpp');
    plt.xlabel('Turn');plt.ylabel('sigma');
    plt.legend(loc=0);
    fig = plt.figure(2);
#    fig.add_subplot(211);
    plt.plot(t2f(np.arange(endFFT-startFFT)),c2db(rfft([bpData[0][i] for i in np.arange(startFFT,endFFT)])),label='x');
#    plt.plot(t2f(np.arange(len(bpData[4])-startFFT)),c2db(rfft([bpData[4][i] for i in np.arange(startFFT,len(bpData[4]))])),label='px');
    plt.plot(t2f(np.arange(endFFT-startFFT)),c2db(rfft([bpData[2][i] for i in np.arange(startFFT,endFFT)])),label='y');
#    plt.plot(t2f(np.arange(len(bpData[6])-startFFT)),c2db(rfft([bpData[6][i] for i in np.arange(startFFT,len(bpData[6]))])),label='py');
    plt.plot(t2f(np.arange(endFFT-startFFT)),c2db(rfft([bpData[11][i] for i in np.arange(startFFT,endFFT)])),label='t');
    plt.xlabel('Q [frev]');plt.ylabel('Ampl [dB]');
    plt.legend(loc=0);
    if sussix:
        plt.figure(6);
        sussix_inp(ir=1,turns=len(bpData[0]),tunex=0.31,tuney=0.32,istun=1,narm=400);
        out = sussixBS(bpData[0],bpData[2],bpData[4],bpData[6]);
        sort(out.ox,out.ax);
        sort(out.oy,out.ay);
        plt.plot(out.ox,out.ax,color = 'b',label='x');
        plt.plot(out.oy,out.ay,color = 'r',label='y');
        plt.legend(loc=0);
        #plt.plot(1.0-out.ox,out.ax,color = 'b',label='Sussix x');
        #plt.plot(1.0-out.oy,out.ay,color = 'r',label='Sussix y');
    plt.figure(3);
    plt.plot(np.arange(len(bpData[8])),bpData[8],label='x');
    if fitPlot:
        (a,b) = polyfit(np.arange(startFit,endFit),[bpData[8][i] for i in np.arange(startFit,endFit)],1);
        aerr = getFitError(a,b,np.arange(startFit,endFit),[bpData[8][i] for i in np.arange(startFit,endFit)]);
        plt.plot(np.arange(startFit,endFit),polyval([a,b],np.arange(startFit,endFit)),label='{0:03.3E} x+{1:03.1f}'.format(a,b));
    plt.plot(np.arange(len(bpData[9])),bpData[9],label='y');
    if fitPlot:
        (a1,b1) = polyfit(np.arange(startFit,endFit),[bpData[9][i] for i in np.arange(startFit,endFit)],1);
        a1err = getFitError(a1,b1,np.arange(startFit,endFit),[bpData[9][i] for i in np.arange(startFit,endFit)]);
        plt.plot(np.arange(startFit,endFit),polyval([a1,b1],np.arange(startFit,endFit)),label='{0:03.3E} y+{1:03.1f}'.format(a1,b1));
    plt.plot(np.arange(len(bpData[15])),bpData[15],label='t');
    if fitPlot:
        (a,b) = polyfit(np.arange(startFit,endFit),[bpData[15][i] for i in np.arange(startFit,endFit)],1);
        a2err = getFitError(a,b,np.arange(startFit,endFit),[bpData[15][i] for i in np.arange(startFit,endFit)]);
        plt.plot(np.arange(startFit,endFit),polyval([a,b],np.arange(startFit,endFit)),label='{0:03.3E} x+{1:03.1f}'.format(a,b));

    average = np.zeros(len(bpData[8]));
    for i in np.arange(len(average)):
        average[i] = (bpData[8][i]+bpData[9][i])/2.0;
#    plt.plot(np.arange(len(average)),average,label='avg');
    if fitPlot:
        (aavg,bavg) = polyfit(np.arange(startFit,endFit),[average[i] for i in np.arange(startFit,endFit)],1);
        aavgerr = getFitError(aavg,bavg,np.arange(startFit,endFit),[average[i] for i in np.arange(startFit,endFit)]);
        plt.plot(np.arange(startFit,endFit),polyval([aavg,bavg],np.arange(startFit,endFit)),label='{0:03.3E} y+{1:03.1f}'.format(aavg,bavg));
    plt.xlabel('Turn');plt.ylabel('Emitance');
    plt.legend(loc=0);
    plt.twinx();
    plt.plot(np.arange(len(bpData[10])),bpData[10],'--k',label='Intensity');
    plt.ylabel('Intensity');

    plt.figure(4);
    plt.plot(t2f(np.arange(endFFT-startFFT)),c2db(rfft([bpData[1][i] for i in np.arange(startFFT,endFFT)])),label='$\Delta$ x');
    plt.plot(t2f(np.arange(endFFT-startFFT)),c2db(rfft([bpData[3][i] for i in np.arange(startFFT,endFFT)])),label='$\Delta$ y');
    plt.xlabel('Q [frev]');plt.ylabel('Ampl [dB]');plt.legend();
    

    #print toPercentPerHour(a),toPercentPerHour(aerr),toPercentPerHour(a1),toPercentPerHour(a1err),toPercentPerHour(aavg),toPercentPerHour(aavgerr);
    if spectrogram:
        if len(bpData[0]) > 2000:
            plt.figure(5);
            span = 2000;
            step = 10;
            limInf = 0.29;
            limSup = 0.33;
            #limInf = 0.6;
            #limSup = 0.7;
            spectrums = [];
            freq = t2f(np.arange(span));
            indices = [];
            for i in range(len(freq)):
                if freq[i]>limInf and freq[i]<limSup:
                    indices.append(i);
            for i in np.arange(0,len(bpData[0])-span,step):
                spectra = c2db(rfft([bpData[0][i] for i in np.arange(i,i+span)]));
                spectrums.append([spectra[k] for k in indices]);
            plt.imshow(np.flipud(spectrums), aspect='auto', extent=[limInf,limSup,0,len(bpData[0])-span/2]);
            #plt.imshow(np.fliplr(np.flipud(spectrums)), aspect='auto', extent=[limInf,limSup,0,len(bpData[0])-span/2]);

def makeLongitudinalParameterPlots(bpData,startFFT=0) :
    plt.figure(0);
    plt.plot(t2f(np.arange(len(bpData[11])-startFFT)),c2db(rfft([bpData[11][i] for i in np.arange(startFFT,len(bpData[11]))])),label='s');
    plt.plot(t2f(np.arange(len(bpData[13])-startFFT)),c2db(rfft([bpData[13][i] for i in np.arange(startFFT,len(bpData[13]))])),label='dp/p');
    plt.figure(1);
    plt.plot(np.arange(len(bpData[12])),bpData[12]);
    plt.figure(2);
    plt.plot(np.arange(len(bpData[15])),bpData[15]);
    plt.figure(3);
    plt.plot(np.arange(len(bpData[11])),bpData[11],'b',label='s');
    plt.twinx();
    plt.plot(np.arange(len(bpData[13])),bpData[13],'r',label='dp/p');
    plt.legend(loc=0);

def makeHistogramPlot(data,bins=100,color='r',label='raw',fitPlot=True,linestyle='-'):
    events,edges = np.histogram(data,bins=bins);
    bin = np.arange(len(events),dtype=float);
    for i in np.arange(len(events)):
        bin[i] = (edges[i]+edges[i+1])/2.0;
    [x,fx],mu,sigma,myMax = fit.gaussianFit(events,bin);
    plt.plot(bin,events,label=label,color=color,linestyle=linestyle);
    if fitPlot:
        plt.plot(x,fx,label=str(mu)+','+str(sigma),color='b',linestyle=linestyle);
    plt.ylabel('count');
    plt.legend();
    return mu,sigma,myMax,edges

def makeStandardBeamPlots(beamData,color='r',legend='raw',bins=1000,fitPlot=True,densityPlot=True,linestyle='-'):
    fig = plt.figure(0);
    nSigma = 6;
    fig.add_subplot(221);
    mux,sigmax,maxx,bins = makeHistogramPlot(beamData[0],bins=bins,color=color,label=legend,fitPlot=fitPlot,linestyle=linestyle);
    plt.xlabel('x');
#    print 'mux = ',mux,', sigmax =',sigmax,', maxx = ',maxx;
    plt.axis([mux-nSigma*sigmax,mux+nSigma*sigmax,0,1.1*maxx]);
    fig.add_subplot(222);
    mupx,sigmapx,maxpx,bins = makeHistogramPlot(beamData[1],bins=bins,color=color,label=legend,fitPlot=fitPlot,linestyle=linestyle);
    plt.xlabel('px');
#    print 'mupx = ',mupx,', sigmapx =',sigmapx,', maxpx = ',maxpx;
    plt.axis([mupx-nSigma*sigmapx,mupx+nSigma*sigmapx,0,1.1*maxpx]);
    fig.add_subplot(223);
    muy,sigmay,maxy,bins = makeHistogramPlot(beamData[2],bins=bins,color=color,label=legend,fitPlot=fitPlot,linestyle=linestyle);
    plt.xlabel('y');
#    print 'muy = ',muy,', sigmay =',sigmay,', maxy = ',maxy;
    plt.axis([muy-nSigma*sigmay,muy+nSigma*sigmay,0,1.1*maxy]);
    fig.add_subplot(224);
    mupy,sigmapy,maxpy,bins = makeHistogramPlot(beamData[3],bins=bins,color=color,label=legend,fitPlot=fitPlot,linestyle=linestyle);
    plt.xlabel('py');
#    print 'mupy = ',mupy,', sigmapy =',sigmapy,', maxpy = ',maxpy;
    plt.axis([mupy-nSigma*sigmapy,mupy+nSigma*sigmapy,0,1.1*maxpy]);

    if densityPlot:
        fig = plt.figure(1);
        nBin = 100;
        plt.hexbin(beamData[0],beamData[1],gridsize=nBin,extent=[mux-nSigma*sigmax,mux+nSigma*sigmax,mupy-nSigma*sigmapy,mupy+nSigma*sigmapy]);
        plt.xlabel('x');plt.ylabel('y');

    return bins;
