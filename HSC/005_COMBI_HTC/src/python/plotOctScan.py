import os,time
import numpy as np
import matplotlib.pyplot as plt
from parseOutput import *
import matplotlib as mpl

if __name__=="__main__":
    mpl.rcParams['font.size'] = 30.0
    #IOcts = [350.0,500.0]
    IOcts = [0.0,50.0,100.0,150.0,200.0,250.0,300.0]#,350.0,400.0,450.0,500.0]
    for IOct in IOcts:
        print(IOct)
        inputdir ='/hpcscratch/user/xbuffat/COMBI/output/octScan/noBB/octScan_noBB_chomra15.0_dGain0.04_Ioct'+str(IOct)+'_noise0.0002/'
        fileName = inputdir+"B1b1.bparam"
        if os.path.exists(fileName):
            time0 = time.time()
            bpData = parseBeamParameter(fileName)
            time1 = time.time()
            plt.figure(0)
            ampl = amplitude(bpData[0],span=10,step=100)
            #(a,b) = polyfit(ampl[0],ampl[1],1)
            lines = plt.plot(ampl[0],ampl[1],'-',label='H, '+str(IOct))
            color = lines[-1].get_color()

            ampl = amplitude(bpData[2],span=10,step=100)
            #(a,b) = polyfit(ampl[0],ampl[1],1)
            plt.plot(ampl[0],ampl[1],'--',label='V, '+str(IOct),color=color)

            plt.figure(1)
            plt.plot(np.arange(len(bpData[8])),bpData[8],label='H, '+str(IOct),color=color)

            plt.plot(np.arange(len(bpData[9])),bpData[9],'--',label='V, '+str(IOct),color=color)
            time2 = time.time()
            print('parsing',time1-time0,'plotting',time2-time1)
        else:
            print(fileName,'does not exist')

    plt.figure(0)
    plt.xlabel('Turn')
    plt.ylabel('Amplitude')
    plt.grid()
    plt.legend(loc=0)

    plt.figure(1)
    plt.xlabel('Turn')
    plt.ylabel('Emittance')
    plt.grid()

    plt.show()
