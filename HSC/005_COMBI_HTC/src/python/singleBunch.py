import numpy as np
import matplotlib.pyplot as plt
from parseOutput import *
import matplotlib as mpl

if __name__=="__main__":
    mpl.rcParams['font.size'] = 30.0;
    analysis = 1;
    inputdir ='/hpcscratch/user/xbuffat/COMBI/output/octScan/noBB/octScan_noBB_chomra15.0_dGain0.02_Ioct200.0_noise0.001_npart4000000.0_nslice80_iSeed5/';
    if analysis == 1 :
        data = parseBeamParameter(inputdir+"B1b1.bparam");
        #for i in np.arange(len(data)):
        #    data[i] = data[i][0:len(data[i])-2];
        makeStandardPlots(data,fitPlot=False,sussix=False,startFit=000000,startFFT=0000,endFFT=100000,spectrogram=False);
        #plt.plot(np.arange(len(data[0])),data[0],label='x');
        #plt.plot(np.arange(len(data[9])),data[9],label='y');
        #plt.legend();

    if analysis == 2 :
        cmap = plt.get_cmap('jet');
        bins = 10000;
        turns = [100000]
        for i in np.arange(len(turns)):
            beamData = parseBeam(inputdir+"B1b1_t"+str(turns[i])+".beam");
            beamData = parseBeam(inputdir+"B1b1_t"+str(turns[i])+".beam");
            bins = makeStandardBeamPlots(beamData,cmap(i/len(turns)),str(turns[i]),bins=bins,fitPlot=True,startFit=1000,startFFT=1000,densityPlot = False);

    if analysis == 3 :
        data1 = parseBeamParameter(inputdir+"B1b1.bparam");
        data2 = parseBeamParameter(inputdir+"B2b1.bparam");
        fig = plt.figure(5);
        fig.add_subplot(211)
        plt.plot(np.arange(len(data1[0])),[data1[0][i]/data1[1][i] for i in np.arange(len(data1[0]))],'-b',label="B1");
        plt.plot(np.arange(len(data2[0])),[data2[0][i]/data2[1][i] for i in np.arange(len(data2[0]))],'-r',label="B2");
        plt.xlabel("turn");plt.ylabel("x [sigma]");plt.legend();
        fig.add_subplot(212);
        plt.plot(np.arange(len(data1[0])),[data1[0][i]/data1[1][i] for i in np.arange(len(data1[0]))],'-b',label="B1");
        plt.plot(np.arange(len(data2[0])),[data2[0][i]/data2[1][i] for i in np.arange(len(data2[0]))],'-r',label="B2");
        plt.xlabel("turn");plt.ylabel("y [sigma]");plt.legend();

    if analysis == 4:
        beamData = parseBeam(inputdir+"B1b1.beam");
#        beamData1 = parseBeam(inputdir+"B0b0_t1000000.beam");
        nBin = 100;
        densityPlot = True;
        fig = plt.figure(1);
        mux,sigmax,maxx,bins = makeHistogramPlot(beamData[0],bins=nBin,fitPlot=True);
        print(sigmax,np.max(beamData[0])/sigmax);
#        mux,sigmax,maxx,bins = makeHistogramPlot(beamData1[0],bins=nBin,fitPlot=True,linestyle='--');
        plt.xlabel('x');plt.ylabel('count');
        fig = plt.figure(2);
        mupx,sigmapx,maxpx,bins = makeHistogramPlot(beamData[1],bins=bins,fitPlot=True);
        print(sigmapx,np.max(beamData[1])/sigmapx);
#        mupx,sigmapx,maxpx,bins = makeHistogramPlot(beamData1[1],bins=bins,fitPlot=True,linestyle='--');
        plt.xlabel('px');plt.ylabel('count');
        fig = plt.figure(3);
        muy,sigmay,maxy,bins = makeHistogramPlot(beamData[2],bins=bins,fitPlot=True);
        print(sigmay,np.max(beamData[2])/sigmay);
#        muy,sigmay,maxy,bins = makeHistogramPlot(beamData1[2],bins=bins,fitPlot=True,linestyle='--');
        plt.xlabel('y');plt.ylabel('count');
        fig = plt.figure(4);
        mupy,sigmapy,maxpy,bins = makeHistogramPlot(beamData[3],bins=bins,fitPlot=True);
        print(sigmapy,np.max(beamData[3])/sigmapy);
#        mupy,sigmapy,maxpy,bins = makeHistogramPlot(beamData1[3],bins=bins,fitPlot=True,linestyle='--');
        plt.xlabel('py');plt.ylabel('count');
       
        
    plt.show();
