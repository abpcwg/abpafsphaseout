from scipy import optimize
from numpy import *

class Parameter:
    def __init__(self, value):
        self.value = value

    def set(self, value):
         self.value = value

    def __call__(self):
        return self.value

def fit(function, parameters, y, x = None):
    def f(params):
        i = 0
        for p in parameters:
            p.set(params[i])
            i += 1
        return y - function(x)

    if x is None: x = arange(len(y))
    p = [param() for param in parameters]
    optimize.leastsq(f, p)

def estimateGaussianParameter(y,x=None):
    if x is None: x = arange(len(y));
    mu = Parameter(sum(x*y)/sum(y));
    sigma = Parameter(sqrt(abs(sum((x-mu())**2*y)/sum(y))));
    height = Parameter(max(y));
    return mu,sigma,height

def gaussianFit(y,x = None):
    if x is None: x = arange(len(y));
    mu,sigma,height = estimateGaussianParameter(y,x);

    def f(x): return height() * exp(-((x-mu())/sigma())**2)

    fit(f, [mu, sigma, height], y,x);
    return [x,f(x)],mu(),sigma(),height();

#assumes x[0] ~ 0
def estimateExpParameter(y,x = None):
    if x is None: x = arange(len(y));
    avgspan = 100;
    if len(y)<avgspan :
        A = Parameter(average(y));
        b = Parameter((y[len(y)-1]-y[0])/((x[len(y)-1]-x[0])*A()));
    else:
        A = Parameter(average(y[0:avgspan]));
        b = Parameter((y[avgspan-1]-y[0])/((x[avgspan-1]-x[0])*A()));
        #print A(),b()
    return A,b

def expFitLin(y,x = None):
    if x is None: x = arange(len(y));
    [a,b] = polyfit(x,log(y),1);
    return [exp(b),a];
    
def expFit(y,x = None):
    if x is None: x = arange(len(y));
    A,b = estimateExpParameter(y,x);

    def f(x): return A() * exp(b()*x);

    fit(f, [A,b], y,x);
    return [x,f(x)],A(),b();
