#include "extern.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(int argc, char *argv[])
{
    unsigned int currentTime = time(NULL);
    rdmin_(&currentTime);
    unsigned int seed;
    rdmout_(&seed);
    
    int ibeam = 2;
    int ibunch = 1;
    double energy = 4000;
    double mass = 0.938272;
    double charge = 1;
    double bunchIntensity = 1E11;
    double emit = 1.5E-6;
    double betastar = 0.6;
    int npart = 1E6;
    double xfact = 0.0;
    double xkick = 0.0;
    
    initconst_(&ibeam,&ibunch,&energy,&mass,&charge);
    double particleCharge = charge >= 0.0 ? bunchIntensity/npart : -1.0*bunchIntensity/npart;
    
    double rms[NCOORD];
    rms[0] = sqrt(mass*emit*betastar);
    rms[1] = sqrt(mass*emit/betastar);
    rms[2] = sqrt(mass*emit*betastar);
    rms[3] = sqrt(mass*emit/betastar);

    double* beam = (double*)malloc(npart*NCOORD*sizeof(double));
    for(int i = 0;i<npart*NCOORD;++i) {
        beam[i] = 0;
    }
    double bparam[NPARAM];
    bfillpar_(beam,&npart,bparam,rms,&bunchIntensity,&xfact, &xkick, &seed);
    beampar_(beam,&npart,bparam,rms, &particleCharge);

    int ntest=1E5;
    double nMin=-10;
    double nMax=10;
    double* testBeam = (double*)malloc(ntest*NCOORD*sizeof(double));
    
    for(int i=0;i<ntest*NCOORD;i+=NCOORD) {
            double n = ((double)i)/(NCOORD*ntest);
            testBeam[i] = (nMin+(nMax-nMin)*n)*rms[0];
            //printf("%i,%f,%.20E\n",i,n,testBeam[i]);
            for(int j=1;j<NCOORD;++j){
                testBeam[i+j] = 0.0;
            }
    }

    int plan = 2;
    double sep = -8;
    int usekick=1;
    double kick = 0;
    lrdpkick_(bparam,&plan,&sep,&rms[0],&kick);
    pkicklr_(testBeam,&ntest,bparam, bparam,&plan,&sep,&rms[0],&usekick,&kick);

    FILE* file = fopen("testLR_H.csv","a+");
    for(int i=0;i<ntest*NCOORD;i+=NCOORD) {
            fprintf(file,"%.20E,%.20E\n",testBeam[i]/rms[0],testBeam[i+1]);
    }
    fprintf(file,"\n");
    fclose(file);
    
    for(int i=0;i<ntest*NCOORD;i+=NCOORD) {
        double n = ((double)i)/(NCOORD*ntest);
        testBeam[i] = 0.0;
        testBeam[i+1] = 0.0;
        testBeam[i+2] = (nMin+(nMax-nMin)*n)*rms[2];
        //printf("%i,%f,%.20E\n",i,n,testBeam[i+2]);
        for(int j=3;j<NCOORD;++j){
            testBeam[i+j] = 0.0;
        }
    }
    
    pkicklr_(testBeam,&ntest,bparam, bparam,&plan,&sep,&rms[0],&usekick,&kick);

    file = fopen("testLR_V.csv","a+");
    for(int i=0;i<ntest*NCOORD;i+=NCOORD) {
            fprintf(file,"%.20E,%.20E\n",testBeam[i+2]/rms[0],testBeam[i+3]);
    }
    fprintf(file,"\n");
    fclose(file);
}
