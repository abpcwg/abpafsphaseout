#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void swap(double* a, double* b)
{
  double t=*a; 
  *a=*b;
  *b=t;
}
void sort(double arr[], int beg, int end)
{
  printf("QUICKSORT : %d\n",end-beg);
  if (beg < end)
  {
    double piv = arr[beg];
    int l = beg + 1, r = end;
    while (l < r)
    {
      if (arr[l] >= piv)
        l++;
      else
        --r;
        swap(&arr[l], &arr[r]);
    }
    swap(&arr[--l], &arr[beg]);
    sort(arr, beg, l);
    sort(arr, r, end);
  }
}
void printlist(double list[],int n)
{
   int i;
   for(i=0;i<n;i++)
      printf("%E\t",list[i]);
   printf("\n");
}

void main()
{
   const int MAX_ELEMENTS = 1000;
   double list[MAX_ELEMENTS];

   int i = 0;
   
   // generate random numbers and fill them to the list
   srand ( time(NULL) );
   for(i = 0; i < MAX_ELEMENTS; i++ ){
	   list[i] = rand();
   }
   printf("The list before sorting is:\n");
   printlist(list,MAX_ELEMENTS);
   
   // sort the list using quicksort
   sort(list,0,MAX_ELEMENTS);

   // print the result
   printf("The list after sorting using quicksort algorithm:\n");
   printlist(list,MAX_ELEMENTS);
}

