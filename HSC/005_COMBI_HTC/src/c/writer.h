#ifndef __WRITERHEADER__
#define __WRITERHEADER__

void printBeamParameter(double[]);
void writeBeamParameter(char*, double[]);
void writeBeamParameter(char*,char*, double[]);

void writeSlicePositions(char* outputDir,char* fileName,double* sliceMoments,int nSlice,int coordinate);
void writeSlicePositions(char* fileName,double* sliceMoments,int nSlice,int coordinate);
void writePizzaSlicePositions(char* outputDir,char* fileName,double*** sliceMoments,int nSlice,int nRing,int coordinate);
void writePizzaSlicePositions(char* fileName,double*** sliceMoments,int nSlice,int nRing,int coordinate);

//coordinate value correspond to COORD or 101:JX, 102:JY, 103:JL
bool writeBeamDistribution(char* outputDir,char* fileName,double* beam,int npart,double* rms,double limInf,double limSup,int nStep,int coordinate,double* workspace);
bool writeBeamDistribution(char* fileName,double* beam,int npart, double* rms,double limInf,double limSup,int nStep,int coordinate,double* workspace);
void beamDistribution(double* beam,int npart,double* rms,double limInf,double limSup,int nStep,int coordinate,double* workspace);
bool writeTransverseBeamDistribution(char* outputDir,char* fileName,double* beam,int npart,double* rms,double limInf,double limSup,int nStep,double* workspace);
bool writeTransverseBeamDistribution(char* fileName,double* beam,int npart, double* rms,double limInf,double limSup,int nStep,double* workspace);
void transverseBeamDistribution(double* beam,int npart,double* rms,double limInf,double limSup,int nStep,double* workspace);

bool writeBeam(char* outputDir,char* fileName,double* beam,int npart);
bool writeBeam(char* fileName,double* beam,int npart);

bool writeTestParticle(char* outputDir,char* fileName,int ibeam,int ibunch,double* beam,double* bparam,int npartReal,int npartTest);
bool writeTestParticle(char* fileName,int ibeam,int ibunch,double* beam,double* bparam,int npartReal,int npartTest);

bool writeString(char* outputDir,char* fileName, char* string);
bool writeString(char* fileName,char* string);

void sortAlphabetically(char* string);

#endif

