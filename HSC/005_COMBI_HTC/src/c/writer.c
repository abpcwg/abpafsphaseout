#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "const.h"
#include <fcntl.h>
#include <unistd.h>
#include "writer.h"
#include <math.h>
#include <sys/types.h> 

void sortAlphabetically(char* string) {
//strcmp
//	FILE* file = fopen(fileName,"r");
	
//	FILE* file = fopen(fileName,"w");
}

//TODO check that file does exist
void writeBeamParameter(char* fileName,double bparam[]) {
	FILE* file = fopen(fileName,"a+");
	for(int i=0;i<NPARAM;++i)
	{
		fprintf(file,"%.20E ",bparam[i]);
	}
	fprintf(file,"\n");
	fclose(file);
}

void writeBeamParameter(char* outputDir,char* fileName, double bparam[]) {
	if(outputDir != NULL)
	{
		char completeFileName[strlen(outputDir)+1+strlen(fileName)];
		strcpy(completeFileName,outputDir);
		strcat(completeFileName,"/");
		strcat(completeFileName,fileName);
		writeBeamParameter(completeFileName,bparam);
	}
	else
	{
		printf("...writing to file %s\n",fileName);
		fflush(stdout);
		writeBeamParameter(fileName,bparam);
	}
}

void printBeamParameter(double bparam[]) {
	printf("xavg = %E\n",bparam[0]);
	printf("xsig = %E\n",bparam[1]);
	printf("yavg = %E\n",bparam[2]);
	printf("ysig = %E\n",bparam[3]);
	printf("zavg = %E\n",bparam[12]);
	printf("zsig = %E\n",bparam[13]);
	printf("pxavg = %E\n",bparam[4]);
	printf("pxsig = %E\n",bparam[5]);
	printf("pyavg = %E\n",bparam[6]);
	printf("pysig = %E\n",bparam[7]);
    printf("dpavg = %E\n",bparam[14]);
	printf("dpsig = %E\n",bparam[15]);
	printf("xemit = %E\n",bparam[8]);
	printf("yemit = %E\n",bparam[9]);
	printf("Intensity = %E\n",bparam[10]);
	fflush(stdout);

}


void writeSlicePositions(char* outputDir,char* fileName,double* sliceMoments,int nSlice,int coordinate) {
	if(outputDir != NULL)
	{
		char completeFileName[strlen(outputDir)+1+strlen(fileName)];
		strcpy(completeFileName,outputDir);
		strcat(completeFileName,"/");
		strcat(completeFileName,fileName);
		return writeSlicePositions(completeFileName,sliceMoments,nSlice,coordinate);
	}
	else
	{
		return writeSlicePositions(fileName,sliceMoments,nSlice,coordinate);
	}
}

void writeSlicePositions(char* fileName,double* sliceMoments,int nSlice,int coordinate){
	FILE* file = fopen(fileName,"a+");
	for(int i=0;i<nSlice;++i)
	{
		fprintf(file,"%.20E ",sliceMoments[coordinate*nSlice+i]);
	}
	fprintf(file,"\n");
	fclose(file);
}

void writePizzaSlicePositions(char* outputDir,char* fileName,double*** sliceMoments,int nSlice,int nRing,int coordinate) {
	if(outputDir != NULL)
	{
		char completeFileName[strlen(outputDir)+1+strlen(fileName)];
		strcpy(completeFileName,outputDir);
		strcat(completeFileName,"/");
		strcat(completeFileName,fileName);
		return writePizzaSlicePositions(completeFileName,sliceMoments,nSlice,nRing,coordinate);
	}
	else
	{
		return writePizzaSlicePositions(fileName,sliceMoments,nSlice,nRing,coordinate);
	}
}

void writePizzaSlicePositions(char* fileName,double*** sliceMoments,int nSlice,int nRing,int coordinate){
	FILE* file = fopen(fileName,"a+");
	for(int i=0;i<nSlice;++i){
        for(int j=0;j<nRing;++j){
		    fprintf(file,"%.20E ",sliceMoments[i][j][coordinate]);
        }
	}
	fprintf(file,"\n");
	fclose(file);
}


bool writeBeamDistribution(char* outputDir,char* fileName,double* beam,int npart,double* rms,double limInf,double limSup,int nStep,int coordinate,double* workspace) {
	if(outputDir != NULL)
	{
		char completeFileName[strlen(outputDir)+1+strlen(fileName)];
		strcpy(completeFileName,outputDir);
		strcat(completeFileName,"/");
		strcat(completeFileName,fileName);
		return writeBeamDistribution(completeFileName,beam,npart,rms,limInf,limSup,nStep,coordinate,workspace);
	}
	else
	{
		return writeBeamDistribution(fileName,beam,npart,rms,limInf,limSup,nStep,coordinate,workspace);
	}
}
//coordinate value correspond to COORD or 101:JX, 102:JY, 103:JL
bool writeBeamDistribution(char* fileName,double* beam,int npart, double* rms,double limInf,double limSup,int nStep,int coordinate,double* workspace) {
    beamDistribution(beam,npart,rms,limInf,limSup,nStep,coordinate,workspace);
	double sLength = (limSup-limInf)/nStep;
	bool retVal = false;
	FILE* file = fopen(fileName,"w");
	if(file != NULL) {
		for(int i = 0;i<nStep;++i) {
			fprintf(file,"%.20E,%.20E\n",(i+0.5)*sLength,workspace[i]);
			fflush(file);
		}
		fclose(file);
		return true;
	}
	return retVal;
}


void beamDistribution(double* beam,int npart,double* rms,double limInf,double limSup,int nStep,int coordinate,double* workspace) {
    for(int i = 0;i<nStep;++i) {
        workspace[i] = 0;
    }
    double sLength = (limSup-limInf)/nStep;
    double quanta = M_PI*npart;
    if(coordinate < NCOORD) {
        for(int i=0;i<npart;++i) {
            int l = floor((beam[i*NCOORD+coordinate]-limInf)/sLength);
            if(l >= 0 && l < nStep) {
                workspace[l] += quanta;
            }
        }
    }else if(coordinate == 101) {
        for(int i=0;i<npart;++i) {
            int l = floor((beam[i*NCOORD]*beam[i*NCOORD]/(rms[0]*rms[0])+beam[i*NCOORD+1]*beam[i*NCOORD+1]/(rms[1]*rms[1])-limInf)/sLength);
            if(l >= 0 && l < nStep) {
                workspace[l] += quanta;
            }
        }
        double r = 0;
        for(int i = 0;i<nStep;++i) {
            r = (i+0.5)*sLength+limInf;
            workspace[i] = workspace[i]*r*r;
        }
    }else if(coordinate == 102) {
        for(int i=0;i<npart;++i) {
            int l = floor((beam[i*NCOORD+2]*beam[i*NCOORD+2]/(rms[2]*rms[2])+beam[i*NCOORD+3]*beam[i*NCOORD+3]/(rms[3]*rms[3])-limInf)/sLength);
            if(l >= 0 && l < nStep) {
                workspace[l] += quanta;
            }
        }
        double r = 0;
        for(int i = 0;i<nStep;++i) {
            r = (i+0.5)*sLength+limInf;
            workspace[i] = workspace[i]*r*r;
        }
    }else if(coordinate == 103) {
        for(int i=0;i<npart;++i) {
            int l = floor((beam[i*NCOORD+4]*beam[i*NCOORD+4]/(rms[4]*rms[4])+beam[i*NCOORD+5]*beam[i*NCOORD+5]/(rms[5]*rms[5])-limInf)/sLength);
            if(l >= 0 && l < nStep) {
                workspace[l] += quanta;
            }
        }
        double r = 0;
        for(int i = 0;i<nStep;++i) {
            r = (i+0.5)*sLength+limInf;
            workspace[i] = workspace[i]*r*r;
        }
    }
}


/*
//write csv file
bool writeBeam(char* fileName,double* beam,int npart) {
	bool retVal = false;
	FILE* file = fopen(fileName,"a+");
	if(file != NULL) {
		for(int i = 0;i<npart;++i) {
			for(int j = 0;j<NCOORD;++j) {
				fprintf(file,"%.20E ",beam[i*NCOORD+j]);
			}
			fprintf(file,"\n");
			fflush(file);
		}
		fclose(file);
		return true;
	}
	return retVal;
}
*/

// write binary file
bool writeBeam(char* fileName,double* beam,int npart) {
	bool retVal = false;
	FILE* file = fopen(fileName,"w");
	if(file != NULL) {
	    fwrite(beam,sizeof(double),npart*NCOORD,file);
		fclose(file);
		return true;
	}
	return retVal;
}

bool writeBeam(char* outputDir,char* fileName,double* beam,int npart) {
	if(outputDir != NULL)
	{
		char completeFileName[strlen(outputDir)+1+strlen(fileName)];
		strcpy(completeFileName,outputDir);
		strcat(completeFileName,"/");
		strcat(completeFileName,fileName);
		return writeBeam(completeFileName,beam,npart);
	}
	else
	{
		return writeBeam(fileName,beam,npart);
	}
}

bool writeTransverseBeamDistribution(char* outputDir,char* fileName,double* beam,int npart,double* rms,double limInf,double limSup,int nStep,double* workspace) {
	if(outputDir != NULL)
	{
		char completeFileName[strlen(outputDir)+1+strlen(fileName)];
		strcpy(completeFileName,outputDir);
		strcat(completeFileName,"/");
		strcat(completeFileName,fileName);
		return writeTransverseBeamDistribution(completeFileName,beam,npart,rms,limInf,limSup,nStep,workspace);
	}
	else
	{
		return writeTransverseBeamDistribution(fileName,beam,npart,rms,limInf,limSup,nStep,workspace);
	}
}

bool writeTransverseBeamDistribution(char* fileName,double* beam,int npart, double* rms,double limInf,double limSup,int nStep,double* workspace) {
    transverseBeamDistribution(beam,npart,rms,limInf,limSup,nStep,workspace);
	double sLength = (limSup-limInf)/nStep;
	bool retVal = false;
	FILE* file = fopen(fileName,"w");
	if(file != NULL) {
		for(int i = 0;i<nStep;++i) {
		    for(int j= 0;j<nStep;++j) {
			    fprintf(file,"%.20E,%.20E,%.20E\n",i*sLength,j*sLength,workspace[i*nStep+j]);
			    fflush(file);
			}
		}
		fclose(file);
		return true;
	}
	return retVal;
}

void transverseBeamDistribution(double* beam,int npart,double* rms,double limInf,double limSup,int nStep,double* workspace) {
    for(int i = 0;i<nStep*nStep;++i) {
        workspace[i] = 0;
    }
    double sLength = (limSup-limInf)/nStep;
    double norm = 0.0;
    for(int i = 0;i<npart;++i) {
        norm += beam[i*NCOORD+6];
    }
    norm *= sLength*sLength;
    for(int i=0;i<npart;++i) {
        int lx = floor((0.5*(beam[i*NCOORD]*beam[i*NCOORD]/(rms[0]*rms[0])+beam[i*NCOORD+1]*beam[i*NCOORD+1]/(rms[1]*rms[1]))-limInf)/sLength);
        int ly = floor((0.5*(beam[i*NCOORD+2]*beam[i*NCOORD+2]/(rms[2]*rms[2])+beam[i*NCOORD+3]*beam[i*NCOORD+3]/(rms[3]*rms[3]))-limInf)/sLength);
        if(lx >= 0 && lx < nStep && ly >= 0 && ly < nStep ) {
            workspace[lx*nStep + ly] += beam[i*NCOORD+6]/norm;
        }
    }
}

bool writeTestParticle(char* fileNameFormat,int ibeam,int ibunch,double* beam,double* bparam,int npartReal,int npartTest) {
	for(int i = npartReal;i<npartReal+npartTest;++i) {
        char fileName[200];
        sprintf(fileName,fileNameFormat,ibeam,ibunch,i-npartReal);
	    FILE* file = fopen(fileName,"a+");
	    if(file != NULL) {
			for(int j = 0;j<4;++j)	{
				fprintf(file,"%.20E ",beam[i*NCOORD+j]);
			}
		    fprintf(file,"\n");
		    fflush(file);
		    fclose(file);
	    } else{
            return false;
        }
	}
	return true;
}

bool writeTestParticle(char* outputDir,char* fileName,int ibeam,int ibunch,double* beam,double* bparam,int npartReal,int npartTest) {
	if(outputDir != NULL)
	{
		char completeFileName[strlen(outputDir)+1+strlen(fileName)];
		strcpy(completeFileName,outputDir);
		strcat(completeFileName,"/");
		strcat(completeFileName,fileName);
		return writeTestParticle(completeFileName,ibeam,ibunch,beam,bparam,npartReal,npartTest);
	}
	else
	{
		return writeTestParticle(fileName,ibeam,ibunch,beam,bparam,npartReal,npartTest);
	}
}

bool writeString(char* fileName,char* string) {
	FILE* file = fopen(fileName,"r+");
	if(file==NULL) {
		file = fopen(fileName,"w+");
		if(file==NULL) {
			return false;
		}
	}
	char thisLine[200];
	char lastLine[200];
	bool appendAtEnd = true;
	int nLine = 0;
	fpos_t* pos = (fpos_t*) malloc(sizeof(fpos_t));
	fgetpos(file,pos);
	char* debugbuffer = (char*) malloc(200*sizeof(char));
	if(fgets(lastLine,sizeof(lastLine),file)!= NULL) {
		++nLine;
		if(strcmp(lastLine,string)>0) {
			appendAtEnd = false;
		} else {
			fgetpos(file,pos);
			++nLine;
			while(fgets(thisLine,sizeof(thisLine),file) != NULL) {
				debugbuffer = (char*) realloc(debugbuffer,(nLine)*200*sizeof(char));
				sprintf(debugbuffer,"%s%s",debugbuffer,lastLine);
				if(strcmp(lastLine,string)<0 && strcmp(thisLine,string)>0) {
					appendAtEnd = false;
					break;
				}
				strcpy(lastLine,thisLine);
				fgetpos(file,pos);
				++nLine;
			}
		}
	}
	if(nLine==0) {
//		printf("adding first line %s : %s\n",fileName,string);
		fprintf(file,"%s",string);
	}else if(appendAtEnd) {
//		printf("append at end %s : %s\n",fileName,string);
		file = freopen(fileName,"a+",file);
		fprintf(file,"\n%s",string);
	}else if(nLine > 0) {
		char* buffer = (char*) malloc(200*sizeof(char));
		sprintf(buffer,"%s\n",string);
		int nlines = 1;
		fsetpos(file,pos);
		while(fgets(thisLine,sizeof(thisLine),file) != NULL) {
			++nlines;
			buffer = (char*) realloc(buffer,nlines*200*sizeof(char));
			sprintf(buffer,"%s%s",buffer,thisLine);
		}
//		printf("\n\nprocess %i addind %s : %s at line %i\ndebugbuffer\n%sbuffer \n%s end buffer\n",getpid(),fileName,string,nLine,debugbuffer,buffer);
//		fflush(stdout);
		fsetpos(file,pos);
		fprintf(file,"%s",buffer);
		free(buffer);
	}
	free(pos);
	fflush(file);
	funlockfile(file);
	fclose(file);
	return true;
}

bool writeString(char* outputDir,char* fileName, char* string) {
	if(outputDir != NULL)
	{
		char completeFileName[strlen(outputDir)+1+strlen(fileName)];
		strcpy(completeFileName,outputDir);
		strcat(completeFileName,"/");
		strcat(completeFileName,fileName);
		return writeString(completeFileName,string);
	}
	else
	{
		return writeString(fileName,string);
	}
}

