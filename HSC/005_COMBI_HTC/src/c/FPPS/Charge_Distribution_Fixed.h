#ifndef CHARGEDISTRIBUTIONFIXED
#define CHARGEDISTRIBUTIONFIXED

#include "Charge_Distribution.h"

class Charge_Distribution_Fixed : public Charge_Distribution{
public:

  Charge_Distribution_Fixed(const Mesh& mesh, const double& d=1.0);
	Charge_Distribution_Fixed(const int& n_theta, const int& m_r, const double& r_max, const double& d=1.0);
	Charge_Distribution_Fixed(const Charge_Distribution_Fixed& d);


	void addParticle(const double& x,const double& y, const double& charge);


	
	virtual Charge_Distribution_Fixed* copy() const;

	void normaliseRho();



	void setDeltaz(const double& z);


	//For Benchmark purpose
	
	virtual void FillRhoConst(const double& ConstChargeDensity, const double& rmax);
	virtual void FillRhoGaussian(const double& totalCharge, const double& sigma);
	virtual void FillRhoBiGaussian(const double& totalCharge, const double& sigmax, const double& sigmay);
	void FillRhoFunction1(double rmax);
	void FillRhoFunction2(double rmax);

	virtual double getCharge(const int& theta, const int& r) const;

	double checkSum() const;

 protected:

	double depth;

};


#endif
