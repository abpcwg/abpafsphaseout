#include "Charge_Distribution_Fixed_Scaled.h"
#include <math.h>
#include <iostream>
#include "functionsFPS.h"


Charge_Distribution_Fixed_Scaled::Charge_Distribution_Fixed_Scaled(const Mesh& mesh, const double& a, const double& d):
  Charge_Distribution_Fixed(mesh,d),
  f(NULL)
{

}

Charge_Distribution_Fixed_Scaled::Charge_Distribution_Fixed_Scaled(const Charge_Distribution_Fixed_Scaled& d):
  Charge_Distribution_Fixed(*d.mesh,d.depth),
  f(NULL)
{
}

Charge_Distribution_Fixed_Scaled::Charge_Distribution_Fixed_Scaled(const int& n_theta, const int& m_r,const double& d):
  Charge_Distribution_Fixed(n_theta,m_r,1.0,d),
  f(NULL)
{

}

Charge_Distribution_Fixed_Scaled::~Charge_Distribution_Fixed_Scaled()
{
  delete f;
}

void Charge_Distribution_Fixed_Scaled::addParticle(const double& x, const double& y, const double& charge){

  double rold=computeR(x,y);
  double r=f->inv_f(rold);
  
  double theta=computeTheta(x,y);
  unsigned int r_ind=mesh->which_R(r);
  unsigned int theta_ind=mesh->which_Theta(theta);

  double dr=mesh->getDeltaR();
  double dtheta=mesh->getDeltaTheta();
  
  double r_i=mesh->getR(r_ind+1);
  double r_iplus1=r_i+dr;
  double theta_i=mesh->getTheta(theta_ind);

  double norm=dtheta*(sq2(r_iplus1)-sq2(r_i))/2.;

  if(r_ind<mesh->getM_r() && theta_ind<mesh->getN_theta()){ 
        double a=theta-theta_i;
        Func[r_ind][theta_ind]+=(dtheta-a)*(sq2(r_iplus1)-sq2(r))*charge/norm/2.;
        if(theta_ind+1<mesh->getN_theta()) Func[r_ind][theta_ind+1]+=(a)*(sq2(r_iplus1)-sq2(r))/2./norm*charge;
        else Func[r_ind][(theta_ind+1)%mesh->getN_theta()]+=(a)*(sq2(r_iplus1)-sq2(r))/2./norm*charge;
        if(r_ind+1<mesh->getM_r()) Func[r_ind+1][theta_ind]+=(sq2(r)-sq2(r_i))*(dtheta-a)/2./norm*charge;
        if(r_ind+1<mesh->getM_r() && theta_ind+1<mesh->getN_theta()) Func[r_ind+1][theta_ind+1]+=(sq2(r)-sq2(r_i))*(a)/2./norm*charge;
        else if(r_ind+1<mesh->getM_r() && theta_ind+1>=mesh->getN_theta()) Func[r_ind+1][(theta_ind+1)%mesh->getN_theta()]+=(sq2(r)-sq2(r_i))*(a)/2./norm*charge;
        //Func[r_ind][theta_ind]+=charge;
    }else Func[mesh->getM_r()-1][mesh->getN_theta()-1]+=charge;

}

Charge_Distribution_Fixed_Scaled* Charge_Distribution_Fixed_Scaled::copy() const
{
  return new Charge_Distribution_Fixed_Scaled(*this);
}

void Charge_Distribution_Fixed_Scaled::FillRhoConst(const double& ConstChargeDensity, const double& rmax)
{

  double r=0.0;
  double theta=0.0;
  double newr=0.0;
	//	std::cout<<"theta="<<computeTheta(particles[2*i+nx],particles[2*i+ny])<<std::endl;
	//	std::cout<<"r="<<computeR(particles[2*i+nx],particles[2*i+ny])<<std::endl;
	/*for(size_t i=0;i<mesh->getM_r();++i){
		for(size_t j=0; j<mesh->getN_theta(); ++j)
		{
		  newr=mesh->getR(i+1);
		  r=f.f(newr);
		  if(fabs(r)<=rmax) Func[i][j]=ConstChargeDensity/(M_PI*rmax*rmax);// *fabs(r)*f.getA()*(1./(1.-fabs(newr))+fabs(newr)/sq2(1.-fabs(newr)));// const
			else Func[i][j]=0.0;
		}
		}*/
	//normaliseRho();

  	for(size_t i=0;i<mesh->getM_r();++i){
		for(size_t j=0; j<mesh->getN_theta(); ++j)
		{
		  newr=mesh->getR(i+1);
		  r=f->f(newr);
		  theta=mesh->getTheta(j);
		  // std::cout<<theta<<std::endl;
		  //Func[i][j]=exp(-fabs(r))*(1./fabs(r)-1.); // phi=exp(-|r|)
		  //Func[i][j]=-((-2.*(-1. + fabs(r))*cos(r) - sin(r)*(2.*r/fabs(r)))/exp(fabs(r))+1./r*((r*cos(r) + sin(r) - fabs(r)*sin(r))/exp(fabs(r)))); //phi=r sin(r) exp(-|r|)
		  //Func[i][j]=-((2*(-1 + exp(-pow(r,2))))/pow(r,3) + 2/(exp(pow(r,2))*r) +(4*r)/exp(pow(r,2))+1./r*(-2/exp(pow(r,2)) - (-1 + exp(-pow(r,2)))/pow(r,2))); //phi=(exp(-r^2)-1)/r
		  Func[i][j]=-sin(theta)*exp(-fabs(r))*(1.-1./fabs(r)-1./r/r);
		  
		  
		}
	}
}

void Charge_Distribution_Fixed_Scaled::FillRhoGaussian(const double& totalCharge, const double& sigma)
{
	double r=0.0;
	double newr=0.0;
	for(size_t i=0;i<mesh->getM_r();++i){
		for(size_t j=0; j<mesh->getN_theta(); ++j)
		{
			newr=mesh->getR(i+1);
			r=f->f(newr);
			
			Func[i][j]=totalCharge/(2.0*M_PI*sigma*sigma)*exp(-(r*r)/(2.0*sigma*sigma));
		}
	}
	//normaliseRho();
}

void Charge_Distribution_Fixed_Scaled::FillRhoBiGaussian(const double& totalCharge, const double& sigmax, const double& sigmay)
{
	double r=0.0;
	double newr=0.0;
	double theta=0.0;
	double x=0.0;
	double y=0.0;
	for(size_t i=0;i<mesh->getM_r();++i){
		for(size_t j=0; j<mesh->getN_theta(); ++j)
		{
			newr=mesh->getR(i+1);
			r=f->f(newr);
			theta=mesh->getTheta(j);
			x=r*cos(theta);
			y=r*sin(theta);
			
			Func[i][j]=totalCharge/(2.0*M_PI*sigmax*sigmay)*exp(-(x*x)/(2.0*sigmax*sigmax))*exp(-(y*y)/(2.0*sigmay*sigmay));
		}
	}
	//normaliseRho();
}


void Charge_Distribution_Fixed_Scaled::setChangeCoord(const ChangeCoord& g)
{
  f=g.copy();
}

double Charge_Distribution_Fixed_Scaled::computeEr(const unsigned int& r, const unsigned int& theta) const
{
  double coef=f->der_inv_f(mesh->getR(r+1));
  //if(r<mesh->getM_r()) dr=mesh->getR(r+1)-mesh->getR(r);
	if(r>0 && r<mesh->getM_r()-1) return coef*(Func[r+1][theta]-Func[r-1][theta])/(2.0*mesh->getDeltaR());
	else if(r==0) return coef*(Func[1][theta]-Func[0][theta])/(mesh->
							      getDeltaR());
	else if(r==mesh->getM_r()-1) return coef*(Func[r][theta]-Func[r-1][theta])/(mesh->getDeltaR());
	else{ 
		std::cerr<< "From computeEr: Problem with the input's dimension."<<std::endl;
		return 0.0;
	}


}


double Charge_Distribution_Fixed_Scaled::computeEthetaFD(const unsigned int& r, const unsigned int& theta) const
{
  double r_=f->f(mesh->getR(r+1));
	if(theta>0 && theta<mesh->getN_theta()-1) return 1.0/r_ * (Func[r][theta+1]-Func[r][theta-1])/(2.0*mesh->getDeltaTheta());
	else if (theta==0) return 1.0/
			    r_ * (Func[r][1]-Func[r][mesh->getN_theta()-1])/(2.0*mesh->getDeltaTheta());
	else if (theta==mesh->getN_theta()-1) return 1.0/r_ * (Func[r][0]-Func[r][mesh->getN_theta()-2])/(2.0*mesh->getDeltaTheta());
	else{
		std::cerr<< "From computeEthetaFD: Problem with the input's dimension."<<std::endl;
		return 0.0;
	}
}

void Charge_Distribution_Fixed_Scaled::normaliseRho(){

  double dr=mesh->getDeltaR();
  double r=0.0;
  double der_f=0.0;

#pragma omp parallel for firstprivate(dr,r,der_f) schedule(guided,10)
	for(size_t i=0;i<mesh->getM_r();++i){
		for(size_t j=0; j<mesh->getN_theta(); ++j)
		{
		  r=f->f(mesh->getR(i+1));
		  der_f=f->der_f(mesh->getR(i+1));

		  Func[i][j]/=((sq2(r+dr/2.)-sq2(r-dr/2.))*mesh->getDeltaTheta()/2.*der_f);//((i+1)*mesh.getDeltaR()*mesh.getDeltaR()*mesh.getDeltaTheta()); //divide by the volume of the cell.
		  //else Func[i][j]/=((sq2(mesh->getR(i+1)+dr/2.)-sq2(mesh->getR(i+1)-dr/2.))*mesh->getDeltaTheta()/2.);
			//std::cout<<Func[i][j]<<std::endl;
		}
	}
}
