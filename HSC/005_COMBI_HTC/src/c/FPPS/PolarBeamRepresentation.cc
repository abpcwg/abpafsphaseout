#include "../const.h"
#include "PolarBeamRepresentation.h"
#include <cmath>

PolarBeamRepresentation::PolarBeamRepresentation(double* b,int n):
beam(b),npart(n) {
    radius = new double[npart];
    angle = new double[npart];
}
PolarBeamRepresentation::~PolarBeamRepresentation() {
    delete radius;
    delete angle;
}

double PolarBeamRepresentation::getCharge(int i){
    return beam[NCOORD*i+6];
}
double PolarBeamRepresentation::getRadius(int i){
    return radius[i];
}
double PolarBeamRepresentation::getAngle(int i){
    return angle[i];
}
int PolarBeamRepresentation::getSize(){
    return npart;
}

void PolarBeamRepresentation::kick(int i,double radialKick,double polarKick) {
    double costheta=beam[NCOORD*i]/radius[i];
    double sintheta=beam[NCOORD*i+2]/radius[i];
    beam[NCOORD*i+1] += costheta*radialKick-sintheta*polarKick;
    beam[NCOORD*i+3] += sintheta*radialKick+costheta*polarKick;
}

void PolarBeamRepresentation::update() {
    #pragma omp parallel for schedule(guided,1000)
    for(int i=0; i<npart; ++i){
        radius[i] = sqrt(beam[NCOORD*i]*beam[NCOORD*i]+beam[NCOORD*i+2]*beam[NCOORD*i+2]);
        angle[i] = atan2(beam[NCOORD*i+2],beam[NCOORD*i]);
        if (angle[i] < 0) angle[i] += 2.0*M_PI;
    }
}
