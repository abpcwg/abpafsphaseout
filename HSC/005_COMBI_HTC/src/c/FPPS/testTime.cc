#include "FastPolarPoissonSolver_FD2.h"
#include <random>
#include <stdlib.h> 
#include <fstream>
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>
#include "Charge_Distribution_Fixed.h"


using namespace std;

int main(int argc, char* argv[]){

  timeval Start;
  timeval End;
  int M(0);
  if(argc>1)  M=pow(2,atoi(argv[1]));
  else M=256;
 
  double rmax=2.;
  Mesh* me;
  me = new Mesh(M,M,rmax);

  unsigned int npart=1000000;

  std::random_device rd;
  unsigned int seed(1);
  std::default_random_engine generator(seed);
  
  double min_r(0.0);
  double sigma(0.2);
  double min_t(0.0);
  double max_t(2*M_PI);
  std::normal_distribution<double> dist_x(0.0, sigma);
  std::normal_distribution<double> dist_y(0.0, sigma);
  std::uniform_real_distribution<double> dist_theta(0.0,M_PI); 

  std::ofstream* out;
  out=new std::ofstream;
  std::string filename1="randomdist.txt";
  out->open(filename1.c_str());

  double* part = (double*) malloc(sizeof(double)*2*npart);
  double x_,y_,theta;
  int compteur=0;
  
  for(int i=0; i<npart; ++i)
    {
      x_=dist_x(generator);
      y_=dist_y(generator);
      if(x_*x_+y_*y_<=rmax*rmax) compteur+=1;
      theta=dist_theta(generator);
      part[2*i]=x_;//x_*cos(theta);
      part[2*i+1]=y_;//x_*sin(theta);      
    }
//Test of the finite element solver
  npart=compteur;
    FastPolarPoissonSolver_FD2* p_; 

    Scalar_Potential pot(*me);
    
    Function_on_a_mesh* Er ;
    Function_on_a_mesh* Etheta ;
    Charge_Distribution_Fixed* chd;

    
    Electric_Field_Solver* El;

    p_=new FastPolarPoissonSolver_FD2(M,M,rmax,npart);

    chd=new Charge_Distribution_Fixed(M,M,rmax);

    chd->FillRhoGaussian(1.0,0.2);
    
     gettimeofday(&Start,NULL);
     chd->FillRho(part,npart,0,1,1.0/npart,2);
    gettimeofday(&End,NULL);

    p_->solvePoisson(*chd,pot);
    
    El = new Electric_Field_Solver(pot.getE(true));
    
    Er=new Function_on_a_mesh(El->getEr());
    Etheta =new Function_on_a_mesh(El->getEtheta());
    
    
    double Duration = End.tv_sec-Start.tv_sec+(End.tv_usec-Start.tv_usec)/1E6;

    cout<<M<<" "<<Duration<<endl;


    pot.printOutFunc("/Users/adrienflorio/Documents/EPFL/F2M2/OpenMPcode/testAlgorithm/pot.txt");
    me->printOutR("/Users/adrienflorio/Documents/EPFL/F2M2/OpenMPcode/testAlgorithm/FPS/r_"+std::to_string(M)+".txt");
    Er->printOutFunc("/Users/adrienflorio/Documents/EPFL/F2M2/OpenMPcode/testAlgorithm/FPS/Er_"+std::to_string(M)+".txt");
    
   delete me;
   delete El;
   delete Er;
   delete Etheta;
   delete p_;
   delete chd;

   free(part);
   
   return 0;
}
