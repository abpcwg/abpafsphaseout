#ifndef POLARBEAMREPRESENTATION
#define POLARBEAMREPRESENTATION

class PolarBeamRepresentation {
public:
	PolarBeamRepresentation(double* beam,int npart);
    virtual ~PolarBeamRepresentation();
    
    double getRadius(int i);
    double getAngle(int i);
    double getCharge(int i);
    int getSize();

    void kick(int i,double radialKick, double polarKick);

	void update();
protected:
    double* beam;
    int npart;
    double* radius;
    double* angle;
};

#endif
