#include "FastPolarPoissonSolver_FD2_Scaled.h"
//#include <random>
#include <stdlib.h> 
#include <fstream>
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>
#include "Charge_Distribution_Fixed_Scaled.h"
#include "MeshScaled.h"
#include "ChangeCoord_Frac.h"
#include "ChangeCoord_Tanh.h"
#include <cmath>
#include "Electric_Field_Solver_Scaled.h"

int NCOORD=7;

using namespace std;

int main(int argc, char* argv[]){

  timeval Start;
  timeval End;
  int M(0);
  int N(0);
  if(argc>1)  M=pow(2,atoi(argv[1]));
  else M=256;
  N=M;

M=500;
N=100;

  int compteurint=1;
  compteurint=atoi(argv[2]);
  std::string compteurstr="1";
  if(argc>2) compteurstr=argv[2];
 
  double rmax=1.;
  Mesh* me;
  me = new Mesh(N,M,rmax);

  unsigned int npart=5000000;
  if(argc>3) npart=atoi(argv[3]);

  //std::random_device rd;
  //unsigned int seed(rd());
  //std::default_random_engine generator(seed);
  
	srand(time(NULL));



  double* part = (double*) malloc(sizeof(double)*7*npart);
  double x_,y_;

double U1, U2, W, mult;
  
  for(int i=0; i<npart; ++i)
    {
  	do
    	{
      		U1 = -1 + ((double) rand () / RAND_MAX) * 2;
      		U2 = -1 + ((double) rand () / RAND_MAX) * 2;
      		W = pow (U1, 2) + pow (U2, 2);
    	}
  	while (W >= 1 || W == 0);

	mult = sqrt ((-2 * log (W)) / W);
  	part[7*i] = U1 * mult;
    part[7*i+1]=0;
  	part[7*i+2] = U2 * mult;
    part[7*i+3]=0;
    part[7*i+4]=0;
    part[7*i+5]=0;
    part[7*i+6]=0;
    }




/*std::ifstream infile("randomdist.txt");
while (infile >> go >> b)
{
  	part[2*comp]=go;//x_*cos(theta);
      	part[2*comp+1]=b;//x_*sin(theta); 
	comp+=1;
	//std::cout<<go<<std::endl;
}*/


//Test of the finite element solver

    FastPolarPoissonSolver_FD2_Scaled* p_;

    double a=0.5;


    
    Function_on_a_mesh* Er ;
    Function_on_a_mesh* Etheta ;
    Charge_Distribution_Fixed_Scaled* chd;

    
    Electric_Field_Solver_Scaled* El;

    ChangeCoord_Frac g(a);


    p_=new FastPolarPoissonSolver_FD2_Scaled(N,M,rmax);
    p_->setChangeCoord(g);

    Mesh msc(N,M,1.0);
    p_->setMesh(msc);

    chd=new Charge_Distribution_Fixed_Scaled(N,M,rmax);
    chd->setMesh(msc);
    chd->setChangeCoord(g);
	chd->init();

    El = new Electric_Field_Solver_Scaled(); 
    El->setEr(Function_on_a_mesh(N,M,1.0));
    El->setEtheta(Function_on_a_mesh(N,M,1.0));
    El->setChangeCoord(g);
	El->setMesh(msc);
    //chd->FillRhoGaussian(1.0,0.2);

int ntimes=10;
double Duration=0;

for(int i=0;i<ntimes;++i){
      gettimeofday(&Start,NULL);
    chd->FillRho(part,npart,0,2,1./(double)npart,7);
      //gettimeofday(&Start,NULL);
   p_->solvePoisson(*chd);
  //gettimeofday(&End,NULL);
  // El->fillE(*chd);
	// El->applykicks(part,npart,123.0);
    //Er=new Function_on_a_mesh(El->getEr());
    //Etheta =new Function_on_a_mesh(El->getEtheta());
    gettimeofday(&End,NULL);
    Duration += End.tv_sec-Start.tv_sec+(End.tv_usec-Start.tv_usec)/1E6;
}

    cout<<M<<" "<<" "<<N<<" "<<npart<<" "<<compteurint<<" "<<Duration/ntimes<<endl;


    //pot.printOutFunc("/Users/adrienflorio/Documents/EPFL/F2M2/OpenMPcode/testAlgorithm/pot_scaled_nm.txt");
    //msc.printOutR("./testOMP/r_.txt");

    //Er->printOutFunc("./testOMP/_npart__"+compteurstr+".txt");
    //Etheta->printOutFunc("/Users/adrienflorio/Documents/EPFL/F2M2/OpenMPcode/testAlgorithm/FPS/Et_"+std::to_string(M)+".txt");*/
    
   delete me;
   delete El;
   delete Er;
   delete Etheta;
   delete p_;
   delete chd;

   //free(part);
   
   return 0;
}
