#include "action.h"
#include "const.h"
#include <stdlib.h>
#include <stdio.h>

void getActionStrRep(char* retVal,action act) {
    if(act.nargs > 0) {
        char tmp[1000];
        sprintf(tmp,"%E",act.args[0]);
        for(int j=1;j<act.nargs;++j) {
            sprintf(tmp,"%s,%E",tmp,act.args[j]);
        }
        sprintf(retVal,"%i(%s)",act.actcode,tmp);
    } else {
        sprintf(retVal,"%i()",act.actcode);
    }
};

