#include "passageRecord.h"
#include<stdio.h>

PassageRecord getElement(PassageRecords* records, int index) {
    if(records->position + index < records->length) {
        return records->array[records->position+index];
    } else {
        return records->array[index+records->position-records->length];
    }
}

void add(PassageRecords* records,PassageRecords* toBeAdded) {
    for(int i = 0;i<toBeAdded->length;++i) {
        add(records,&toBeAdded->array[i]);
    }
}

void add(PassageRecords* records, PassageRecord* element) {
    if(records->maxLength > 0) {
        if(records->length == records->maxLength) {
            set(&records->array[records->position],element);
            records->position += 1;
            if(records->position == records->length) {
                records->position = 0;
            }
        } else {
            records->length += 1;
            records->position = records->length-1;
            set(&records->array[records->position],element);
        }
    }
}

void set(PassageRecord* record, PassageRecord* element) {
    record->turn = element->turn;
    record->T = element->T;
    record->X = element->X;
    record->Y = element->Y;
    record->charge = element->charge;
}

void add(PassageRecords* records, int turn, double T, double X, double Y, double charge) {
    if(records->maxLength > 0 ) {
        if(records->length == records->maxLength) {
            set(&records->array[records->position],turn,T,X,Y,charge);
            records->position += 1;
            if(records->position == records->length) {
                records->position = 0;
            }
        } else {
            records->position = records->length;
            records->length = records->length + 1;
            set(&records->array[records->position],turn,T,X,Y,charge);
        }
    }
}

void set(PassageRecord* record, int turn, double T, double X, double Y, double charge) {
    record->turn = turn;
    record->T = T;
    record->X = X;
    record-> Y = Y;
    record->charge = charge;
}

void updateZ(PassageRecords* records, double revolTime, int nturn, double longitudinalPosition) {
    for(int i = 0;i<records->length;++i) {
        //printf("Longitudinal pos %E T %E\n",longitudinalPosition,records->array[i].T);
        records->array[i].Z = revolTime*(nturn-records->array[i].turn)+(longitudinalPosition-records->array[i].T);
    }
}



