#ifndef __EXTERNHEADER__
#define __EXTERNHEADER__

#include "const.h"

extern "C" {

   void rdfill_(char*,int*,char*,int*,int[],int[],int*,int*,int*);
   void advance_(int*, int*, int*);
   void initconst_(int*,int*,double*,double*,double*);
   void bfillpar_(double*,int*,double[],double*,double*,double*,double*);
   void bfilltestpar_(double*,int*,int*,double*,double*,double*);
   void beampar_(double*,int*,double[],double*,double*);
   void pkick_(double*,int*, double*, float*, float*,double*,double*);
   void pkickgauss_(double*,int*,double[]);
   void pkicklr_(double*,int*,double[], double[],int*,double*,double*,int*,double*);
   void inckicklr_(double*,int*,double*,int*,double*,double*);
   void lrdpkick_(double[],int*,double*,double*,double*);
   void lrdpkick2d_(double[],double*,double*,double*,double*,double*);
   void dipole_(double*,int*,int*,double*);
   void transfer_(double*,int*, double*, double*,double*,double*,double*,double*,double*,double*);
   void transfercoupl_(double*,int*, double*, double*,double*,double*,double*);
   void transpi2_(double*,int*,double*,double*);
   void sinnoise_(double*,int*,double*,double*,double*,double*);
   void whitenoise_(double*,int*,double*,double*);
   void quadwhitenoise_(double*,int*,double*,double*);
   void transincnoise_(double*,int*,double*,double*);
   void ccamplnoise_(double*,int*,double*,double*);
   void rf_(double*,int*,double*,double*);
   void octupole_(double*,int*,double*,double*,double*,double*,double*,double*);
   void transferoct_(double*,int*,double*,double*,double*,double*,double*,double*,double*);
   void offset_(double*,int*,double*,double*);
   void synchrad_(double*,int*,double*,double*,double*,double*,double*,double*,double*,double*,double*,double*,double*,int*);
   void synchrad_gauss_(double*,int*,double*,double*,double*,double*,double*,double*,double*,double*,double*,double*);
   void synchradgaussintegral_(double*,int*,double*,double*,double*,double*,double*,double*,double*);
   void synchradgauss_(double*,int*,double*,double*,double*,double*);
   void initrand_(int*);
   void sbc_(double*,int*,double[],double*,int*,double*,double[],double[]);
   void quad_(double*,int*,double*,double*);
   //   void populate_(double*, double*, double*, double*, double*, double*,
   //             int);

   //   unsigned int secure_rand_base(void);

//     void bbhfmm_(float*, float*, float*, float*, int*, int*,
//                  float*, float*, int*,
//                  int*, float*, float*, int*, float*, float*,
//                  float*, float*, int*, int*, float*);

   void hfmmprep_(float*,int*,int*,float*,float*,int*,
                  double*,int*,int*,int*,
                  int*,float*,float*,float*,int*,
                  float*,double*);
   void hfmmcalc_(float*,float*,float*,float*,int*,int*,
                  float*,float*,int*,
                  double*,int*,int*,
                  float*,float*,float*,float*,
                  int*,int*,
                  float*,float*,int*,int*,float*,
                  int*,
                  int*,float*,float*,float*,
                  int*,int*,int*,int*);
   void fmmsetup_(int*, int*, float*, float*);
//   void rannox_(float *dum);

}

#endif
