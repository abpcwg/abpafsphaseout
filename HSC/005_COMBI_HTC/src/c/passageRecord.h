
#ifndef __PASSRECHEADER__
#define __PASSRECHEADER__


//implement an array containing passage records, allowing in-place rotation
//


struct PassageRecord {
    double Z;    //Longitudinal position relative to another bunch (ns)  (changes over time, all the other should be constant)
    int turn; //Turn of passage
    double T;    //Longitudinal position relative to slot 0 (ns)
    double X;    //H position
    double Y;    //V position
    double charge;
};

struct PassageRecords {
    PassageRecord* array;
    int position;
    int length;
    int maxLength;
};

PassageRecord getElement(PassageRecords* array, int index);
void add(PassageRecords* records,PassageRecords* toBeAdded);
void add(PassageRecords* array, PassageRecord* element);
void set(PassageRecord* record, PassageRecord* element);
void add(PassageRecords* records, int turn, double T, double X, double Y, double charge);
void set(PassageRecord* record, int turn, double T, double X, double Y, double charge);
void updateZ(PassageRecords* records, double revolTime, int nturn, double longitudinalPosition);

#endif
