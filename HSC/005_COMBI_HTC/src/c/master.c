#include "extern.h"
#include "const.h"
#include "defs.h"
#include "mpi.h"
#include "parser.h"
#include "passageRecord.h"
#include "action.h"
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

extern int TAG_INIT;
extern int TAG_ACTION;
extern int TAG_ARGS;
extern int TAG_PASSAGERECORD;
extern int TAG_PASSAGEARRAY;
extern int TAG_COMPLETION;

bool init(ParserOutput parserOutput,action*& col ,int& npos,int& numt,int& nBunchB1,int& nBunchB2);
double findBestKick(double* kicks,int nkicks);
int B1Bunch2proc(int bunchNB,int nBunchB1,int nBunchB2);
int B2Bunch2proc(int bunchNB,int nBunchB1,int nBunchB2);

//TODO global vars are not needed, reimplementation of rdfill is needed.
int xxx1[NPOSMAX], xxx2[NPOSMAX];                         // bunch number for each possible interaction point

void doMasterJob(int myid, ParserOutput parserOutput) {
#ifdef TIMING
    timeval initStart;
    gettimeofday(&initStart,NULL);
#endif
    unsigned int currentTime = time(NULL);
    unsigned int wallTime = -1;
    if(parserOutput.wallTime>0) {
        wallTime = currentTime+(int)(parserOutput.wallTime*3600);
    }
    MPI_Status status;
    int activeProcs;
    MPI_Comm_size(MPI_COMM_WORLD, &activeProcs);

    printf("\nCOMBI-hybrid version %s\n", VERSION);
    fflush(stdout);

    // Read input files
    printf("MASTER: initializing\n");
    fflush(stdout);

    actiondata exitLoop;
    exitLoop.actcode = ABORTACTION;
    exitLoop.partner = -1;
    exitLoop.nargs = 0;
    exitLoop.turn = parserOutput.firstTurn;


    action* col = NULL;                     // action code for each possible interaction point
    int npos = 0, numt = 0, nBunchB1 = 0, nBunchB2 = 0;
    
    bool init1Ok = true;
    init1Ok = init(parserOutput,col,npos,numt,nBunchB1,nBunchB2);
    //abort all slaves if there was an error during init    
    if(!init1Ok) {
        initdata init;
        init.abort = true;
        while(activeProcs>1) {
            int dest = activeProcs-1;
            MPI_Send(&init, sizeof(init), MPI_BYTE, dest,TAG_INIT, MPI_COMM_WORLD);
            --activeProcs;
        }
    }else{
        printf("MASTER: npos=%d numt=%i\n", npos, numt);
        char actionSequence[NPOSMAX*1000];
        char actionStrRep[1000];
        getActionStrRep(actionStrRep,col[0]);
        sprintf(actionSequence,"    %s",actionStrRep);
        for(int i = 1;i<npos;++i) {
            getActionStrRep(actionStrRep,col[i]);
            sprintf(actionSequence,"%s\n    %s",actionSequence,actionStrRep);
        }
        printf("MASTER: action sequence : \n%s\n",actionSequence);
        
        if(parserOutput.xfact>0.0) {
            printf("MASTER: xfact=%f\n",parserOutput.xfact);
        }
        if(parserOutput.kick>0.0) {
            printf("MASTER: kick=%f\n",parserOutput.kick);
        }
        fflush(stdout);

        int ib1, ib2;

        // TEMPORARY EXIT
        //      MPI_Finalize();
        //      return 0;

        // INITIALIZATION OF BEAMS

        // Find bunches and populate them
        // Note: for compatibility with the scalar code, all bunches in Beam 1
        //       are populated first, then those in Beam 2.

        bool initBunchOK = true;
        initdata init;
        int requiredProcs = nBunchB1 + nBunchB2 + 1;
        if (requiredProcs > activeProcs) {
            printf("MASTER : ERROR : %i process required, only %i are active : abort\n",requiredProcs,activeProcs);
            initBunchOK = false;
        } else {
            // fire unused slave (in case too many processes are specified)
            init.abort = true;
            while(activeProcs>requiredProcs) {
                int dest = activeProcs-1;
                MPI_Send(&init, sizeof(init), MPI_BYTE, dest,TAG_INIT, MPI_COMM_WORLD);
                --activeProcs;
            }
            //init slaves
            init.abort = false;
            for (int ipos = 0; ipos < npos; ipos++) {
                ib1 = xxx1[ipos];
                fflush(stdout);
                if (ib1 > 0) {
                    init.ibeam = 1;
                    init.ibunch = ib1;
                    init.numberOfBunch = nBunchB1;
                    int lpos = ipos == 0 ? 0 : npos-ipos;
                    init.longitudinalPosition = parserOutput.bunchSpacing*lpos/2;
                    init.abort = false;
                    int rank = B1Bunch2proc(ib1,nBunchB1,nBunchB2);

#ifdef TDEBUG
                    printf("MASTER : ib1=%d,dest=%d\n",ib1, rank);
                    fflush(stdout);
#endif
                    MPI_Send(&init, sizeof(init), MPI_BYTE, rank,TAG_INIT, MPI_COMM_WORLD);
#ifdef TDEBUG
                    printf("MASTER: waiting for init ok from proc %d\n",rank);
                    fflush(stdout);
#endif
                    MPI_Recv(&init, sizeof(init), MPI_BYTE, rank,TAG_INIT, MPI_COMM_WORLD, &status);
                    if(init.abort) {
                        initBunchOK = false;
                    }
                }
            }
            for (int ipos = 0; ipos < npos; ipos++) {
                ib2 = xxx2[ipos];
                fflush(stdout);
                if (ib2 > 0) {
                    init.ibeam = 2;
                    init.ibunch = ib2;
                    init.numberOfBunch = nBunchB2;
                    init.abort = false;
                    init.longitudinalPosition = parserOutput.bunchSpacing*ipos/2;
                    int rank = B2Bunch2proc(ib2,nBunchB1,nBunchB2);
#ifdef TDEBUG
                    printf("MASTER: INITing B2 bunch %d proc %d\n",ib2, rank);
                    fflush(stdout);
#endif
                    MPI_Send(&init, sizeof(init), MPI_BYTE, rank,TAG_INIT, MPI_COMM_WORLD);
#ifdef TDEBUG
                    printf("MASTER: waiting for init ok from proc %d\n",rank);
                    fflush(stdout);
#endif
                    MPI_Recv(&init, sizeof(init), MPI_BYTE, rank,TAG_INIT, MPI_COMM_WORLD, &status);
                    if(init.abort) {
                        initBunchOK = false;
                    }
                }
            }
        }
        if(!initBunchOK) {
            init.abort = true;
            for(int proc = 1;proc < activeProcs; ++proc) {
                MPI_Send(&init, sizeof(init), MPI_BYTE, proc,TAG_INIT, MPI_COMM_WORLD);
            }
            return;
        }
        //Confirm initialisation ok to all procs
        init.abort = false;
        for(int proc = 1;proc < activeProcs; ++proc) {
            MPI_Send(&init, sizeof(init), MPI_BYTE, proc,TAG_INIT, MPI_COMM_WORLD);
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////    
        //do one turn where the LR dipole kick are computed (lrdpkick) and the kick is then set according to the group
        //LR in the same group have the get the same dipole kick
        //LR without specified group are corrected individually
        // THIS IS MEANT TO STUDY ORBIT EFFECT AND IS NOT USED IN THIS VERSION : the loop is kept to compute collshed and to forward maxNbSlice for impedance kicks
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

        printf("MASTER: computing long range dipole kicks\n");
        fflush(stdout);
        //allocate memory for the groups
        int* grIndex = (int*)malloc(0);
        int nGr = 0;
        int nHOGr = 0;
        for(int i = 0;i<npos;++i) {
            if(col[i].actcode == 10) {
                bool alreadyIn = false;
                for(int j = 0;j<nGr;++j) {
                    if(grIndex[j] == col[i].args[2]) {
                        alreadyIn = true;
                        break;
                    }
                }
                if(!alreadyIn) {
                    ++nGr;
                    grIndex = (int*)realloc(grIndex,nGr*sizeof(int));
                    grIndex[nGr-1] = (int)col[i].args[2];
                }
            }
            if(col[i].actcode == 2) {
                col[i].args[3] = nHOGr;
                ++nHOGr;
            }
        }
        double* kicksB1[nGr];
        double* kicksB2[nGr];
        int nkicksB1[nGr];
        int nkicksB2[nGr];
        for(int i = 0;i<nGr;++i) {
            kicksB1[i] = (double*) malloc(0);
            kicksB2[i] = (double*) malloc(0);
            nkicksB1[i] = 0;
            nkicksB2[i] = 0;
        }
        double HOkickHB1[nHOGr];
        double HOkickVB1[nHOGr];
        double HOkickHB2[nHOGr];
        double HOkickVB2[nHOGr]; 
        int nHOkick[nHOGr];
        for(int i=0;i<nHOGr;++i) {
            HOkickHB1[i] = 0.0;
            HOkickVB1[i] = 0.0;
            HOkickHB2[i] = 0.0;
            HOkickVB2[i] = 0.0;
            nHOkick[i] = 0;
        }
        // Step the bunches around 2*Nslot locations
        for(int istep = 0; istep < npos; istep++) {
#ifdef TDEBUG
            printf("MASTER: STEP %d OF %d (Turn 0)\n", istep, npos);
            fflush(stdout);
#endif

            // Check for and do action at each location
            int nsent = 0;      // Number of slave directives sent
            for(int ipos = 0; ipos < npos; ipos++) {
                actiondata adata;
                adata.turn = -1;
                adata.writeToFile = false;
#ifdef TDEBUG
                printf("MASTER: POS %d COLX %d B1 %d B2 %d (Turn 0)\n",ipos, col[ipos].actcode, xxx1[ipos], xxx2[ipos]);
#endif
                //Consider only long range, head-on and impedance
                if (col[ipos].actcode != 10 && col[ipos].actcode !=2 && col[ipos].actcode != 12 && col[ipos].actcode !=4 && col[ipos].actcode !=5) continue;
                ib1 = xxx1[ipos];
                ib2 = xxx2[ipos];
                adata.actcode = col[ipos].actcode;
                adata.nargs = col[ipos].nargs;
                if (ib1 > 0) {
                    if((col[ipos].needsPartner && ib2>0) || !col[ipos].needsPartner) {
                        adata.partner = ib2;
                        adata.partnerCPU = B2Bunch2proc(ib2,nBunchB1,nBunchB2);
                        int rank = B1Bunch2proc(ib1,nBunchB1,nBunchB2);
                        MPI_Send(&adata, sizeof(adata), MPI_BYTE, rank,TAG_ACTION, MPI_COMM_WORLD);
                        if(adata.nargs>0) {
                            MPI_Send(col[ipos].args,col[ipos].nargs*sizeof(double),MPI_BYTE,rank,TAG_ARGS, MPI_COMM_WORLD);
                        }
                        nsent++;
                    }
                }
                if (ib2 > 0) {
                    if((col[ipos].needsPartner && ib1>0) || !col[ipos].needsPartner) {
                        adata.partner = ib1;
                        adata.partnerCPU = B1Bunch2proc(ib1,nBunchB1,nBunchB2);
                        int rank = B2Bunch2proc(ib2,nBunchB1,nBunchB2);
                        MPI_Send(&adata, sizeof(adata), MPI_BYTE, rank,TAG_ACTION, MPI_COMM_WORLD);
                        if(adata.nargs>0) {
                            MPI_Send(col[ipos].args,col[ipos].nargs*sizeof(double),MPI_BYTE,rank,TAG_ARGS, MPI_COMM_WORLD);
                        }
                        nsent++;
                    }
                }
            } // Loop on ipos


            int nsync = 0;      // Number of slaves heard from
            // RECEIVE completion messages from slaves
            while (nsync < nsent) {
                actiondata adata;
#ifdef TDEBUG
                printf("MASTER: STEP %d: %d of %d slaves synced... waiting (Turn 0)\n",istep, nsync, nsent);
                fflush(stdout);
#endif
                MPI_Recv(&adata, sizeof(adata),MPI_BYTE, MPI_ANY_SOURCE,TAG_COMPLETION, MPI_COMM_WORLD, &status);
                if(adata.nargs > 0) {
                    double* args = (double*) malloc(adata.nargs*sizeof(double));
                    int source = status.MPI_SOURCE;
                    MPI_Recv(args, adata.nargs*sizeof(double), MPI_BYTE, source,TAG_ARGS, MPI_COMM_WORLD, &status);
                    if(adata.actcode == 10) {
                        //store kick per group
                        int index = -1;
                        for(int j = 0;j<nGr;++j) {
                            if(grIndex[j] == args[2]) {
                                index = j;
                                break;
                            }
                        }
                        if(index ==-1) {
                            printf("\n\n\nERROR during computation of long range dipole kick (Turn 0)\n\n\n");
                        }
                        if(source<=nBunchB1) {
                            ++nkicksB1[index];
                            kicksB1[index] = (double*) realloc(kicksB1[index],nkicksB1[index]*sizeof(double));
                            kicksB1[index][nkicksB1[index]-1] = args[3];
#ifdef TDEBUG
                            printf("...MASTER:  Rec'd from proc %d, kick = %e (Turn 0)\n",source,kicksB1[index][nkicksB1[index]-1]);
                            fflush(stdout);
#endif
                        }else {
                            ++nkicksB2[index];
                            kicksB2[index] = (double*) realloc(kicksB2[index],nkicksB2[index]*sizeof(double));
                            kicksB2[index][nkicksB2[index]-1] = args[3];
#ifdef TDEBUG
                            printf("...MASTER:  Rec'd from proc %d, kick = %e (Turn 0)\n",source,kicksB2[index][nkicksB2[index]-1]);
                            fflush(stdout);
#endif
                        }
                    }else if(adata.actcode == 2) {
                        if(source<=nBunchB1) {
                            ++nHOkick[(int)args[3]];
                            HOkickHB1[(int)args[3]] += args[4];
                            HOkickVB1[(int)args[3]] += args[5];
                        } else {
                            HOkickHB2[(int)args[3]] += args[6];
                            HOkickVB2[(int)args[3]] += args[7];
                        }
                    }
                }
                nsync++;
            }
#ifdef TDEBUG
            printf("MASTER: STEP %d: all %d slaves accounted for! (Turn 0)\n",istep, nsent);
            fflush(stdout);
#endif
            advance_(xxx1, xxx2, &npos);
        }  // End of STEP
        //Tell slaves to move to next step
        for (int iproc = 1; iproc < activeProcs; iproc++) {
            MPI_Send(&exitLoop, sizeof(exitLoop), MPI_BYTE, iproc,TAG_ACTION, MPI_COMM_WORLD);
        }
        // compute group kicks
        for(int gr = 0;gr<nGr;++gr)
        {
            double kickB1 = findBestKick(kicksB1[gr],nkicksB1[gr]);
            double kickB2 = findBestKick(kicksB2[gr],nkicksB2[gr]);
            for(int i = 0;i<npos;++i) {
                if(col[i].actcode==10 && col[i].args[2]==grIndex[gr]) {
                    col[i].args[3] = kickB1;
                    col[i].args[4] = kickB2;
                }
            }
        }
        for(int i = 0;i<npos;++i) {
            if(col[i].actcode == 2) {
                int index = (int)col[i].args[3];
                col[i].args[4] = HOkickHB1[index]/nHOkick[index];
                col[i].args[5] = HOkickVB1[index]/nHOkick[index];
                col[i].args[6] = HOkickHB2[index]/nHOkick[index];
                col[i].args[7] = HOkickVB2[index]/nHOkick[index];
            }
        }
        //deallocate memory for kick search
        for(int i = 0;i<nGr;++i) {
            free(kicksB1[i]);
            free(kicksB2[i]);
        }
        free(grIndex);
        //end closed orbit search
        
        //allocate memory to store bunch by bunch position for a turn
        //This has to be changed if multiple kicks per turn
        PassageRecords passageRecordsB1;
        passageRecordsB1.array = (PassageRecord*) malloc(nBunchB1*sizeof(PassageRecord));
        passageRecordsB1.maxLength = nBunchB1;
        passageRecordsB1.length = 0;
        PassageRecords passageRecordsB2;
        passageRecordsB2.array = (PassageRecord*) malloc(nBunchB2*sizeof(PassageRecord));
        passageRecordsB2.maxLength = nBunchB2;
        passageRecordsB2.length = 0;
        PassageRecord* tmpPassageRecord = (PassageRecord*) malloc(sizeof(PassageRecord));


#ifdef TIMING
        timeval initEnd;
        gettimeofday(&initEnd,NULL);
        double duration = initEnd.tv_sec-initStart.tv_sec+(initEnd.tv_usec-initStart.tv_usec)/1E6;
        printf("MASTER : time for initialization : %f s\n",duration);
        timeval turnStart;
        timeval turnEnd;
#endif

        printf("MASTER : Inititalization done, start traking\n");
        fflush(stdout);

        // Execute numt turns..........
        unsigned int lastTime = time(NULL);
        unsigned int lastTurn = 0;
        unsigned int newTime;
        bool abortNextTurn = false;
        
        char timingFileName[strlen(parserOutput.outputDir)+100];
		sprintf(timingFileName,"%sTiming",parserOutput.outputDir);
        for(int n = parserOutput.firstTurn+1; n <= numt; n++) {
#ifdef TIMING
            /////////////////////////////////////////////////////////////////////
            if(n > parserOutput.firstTurn+1){
                gettimeofday(&turnEnd,NULL);
                FILE* file = fopen(timingFileName,"a+");
                double duration = turnEnd.tv_sec-turnStart.tv_sec+(turnEnd.tv_usec-turnStart.tv_usec)/1E6;
                printf("MASTER : time for turn %i : %f s\n",n,duration);
                fprintf(file,"%.20E\n",duration);
	            fclose(file);
            }
            gettimeofday(&turnStart,NULL);
            ////////////////////////////////////////////////////////
#endif
#ifdef TDEBUG
            printf("MASTER: TURN %d OF %d\n", n, numt);
            fflush(stdout);
#endif
            newTime = time(NULL);
            if(wallTime>0) {
                if(newTime>wallTime) {
                    printf("\rWalltime passed after %i turns(%.2g%%), abort\n",n-1,100.0*(n-1)/numt);
                    fflush(stdout);
                    break;
                }
            }
            if (PROGRESSMETER) {
                if ((newTime-lastTime) > 1) {
                    printf("\r%.2g%% - %.2g s/turn      ", 100.0*n/numt,((double)(newTime-lastTime))/(n-lastTurn));
                    lastTime = newTime;
                                lastTurn = n;
                    fflush(stdout);
                }
                if(n==numt) {
                    printf("\r100%%, last turn            \n");
                }
            }

            actiondata adata;
            adata.turn = n;

            // Step the bunches around 2*Nslot locations

            for(int istep = 0; istep < npos; istep++) {

#ifdef TDEBUG
                printf("MASTER: STEP %d OF %d\n", istep, npos);
                fflush(stdout);
#endif

                //some stuff usefull to check the collision schedule
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //printf("step %i,",istep);
                //for(int ipos = 0; ipos < npos; ipos++) {
                //   if (xxx1[ipos] > 0) {
                //        printf("(B1b%i,%i),",xxx1[ipos],ipos);
                //    }
                //}
                //for(int ipos = 0; ipos < npos; ipos++) {
                //    if (xxx2[ipos] > 0) {
                //        printf("(B2b%i,%i),",xxx2[ipos],ipos);
                //    }
                //}
                //printf("\n");
                //for(int ipos = 0; ipos < npos; ipos++) {
                //   if( xxx1[ipos] > 0 && xxx2[ipos] > 0) {
                //        printf("HO B1b%i-B2b%i at %i, actcode %i\n",xxx1[ipos],xxx2[ipos],ipos,col[ipos].actcode);
                //    }
                //}
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////

                // Check for and do action at each location

                int nsent = 0;      // Number of slave directives sent

                for(int ipos = 0; ipos < npos; ipos++) {
#ifdef TDEBUG
                    printf("MASTER: POS %d COLX %d B1 %d B2 %d\n",ipos, col[ipos].actcode, xxx1[ipos], xxx2[ipos]);
#endif
                    if (col[ipos].actcode == 0) continue;

                    adata.writeToFile = ipos==0;

                    ib1 = xxx1[ipos];
                    ib2 = xxx2[ipos];

                    adata.actcode = col[ipos].actcode;
                    adata.nargs = col[ipos].nargs;

                    if (ib1 > 0) {
                        adata.beam = 1;
                        adata.partner = ib2;
                        adata.partnerCPU = B2Bunch2proc(ib2,nBunchB1,nBunchB2);
                        int rank = B1Bunch2proc(ib1,nBunchB1,nBunchB2);
#ifdef TDEBUG
                        printf("MASTER: sendig action %d to slave %d\n",adata.actcode, rank);
#endif
                        MPI_Send(&adata, sizeof(adata), MPI_BYTE, rank,TAG_ACTION, MPI_COMM_WORLD);
                        if(adata.nargs>0) {
                            MPI_Send(col[ipos].args,col[ipos].nargs*sizeof(double),MPI_BYTE,rank,TAG_ARGS, MPI_COMM_WORLD);
                        }
                        if(adata.actcode == 12) {
                            //printf("MASTER sending passager record with length %i  to B1b%i (istep %i)\n",passageRecordsB1.length,dest,istep);
                            //fflush(stdout);
                            MPI_Send(&passageRecordsB1,sizeof(PassageRecords),MPI_BYTE,rank,TAG_PASSAGERECORD, MPI_COMM_WORLD);
                            MPI_Send(passageRecordsB1.array,passageRecordsB1.maxLength*sizeof(PassageRecord),MPI_BYTE,rank,TAG_PASSAGEARRAY, MPI_COMM_WORLD);
                        }
                        nsent++;
                    }
                    if (ib2 > 0) {
                        adata.beam = 2;
                        adata.partner = ib1;
                        adata.partnerCPU = B1Bunch2proc(ib1,nBunchB1,nBunchB2);
                        int rank = B2Bunch2proc(ib2,nBunchB1,nBunchB2);
#ifdef TDEBUG
                        printf("MASTER: sendig action %d to slave %d\n",adata.actcode, rank);
#endif
                        MPI_Send(&adata, sizeof(adata), MPI_BYTE, rank,TAG_ACTION, MPI_COMM_WORLD);
                        if(adata.nargs>0) {
                            MPI_Send(col[ipos].args,col[ipos].nargs*sizeof(double),MPI_BYTE,rank,TAG_ARGS, MPI_COMM_WORLD);
                        }
                        if(adata.actcode == 12) {
                            MPI_Send(&passageRecordsB2,sizeof(PassageRecords),MPI_BYTE,rank,TAG_PASSAGERECORD, MPI_COMM_WORLD);
                            MPI_Send(passageRecordsB2.array,passageRecordsB2.maxLength*sizeof(PassageRecord),MPI_BYTE,rank,TAG_PASSAGEARRAY, MPI_COMM_WORLD);
                        }
                        nsent++;
                    }
                } // Loop on ipos

                // RECEIVE completion messages from slaves

                int nsync = 0;      // Number of slaves heard from
                
                while (nsync < nsent) {
                
#ifdef TDEBUG
                    printf("MASTER: STEP %d: %d of %d slaves synced... waiting\n",istep, nsync, nsent);
                    fflush(stdout);
#endif
                    actiondata returnActionData;
                    MPI_Recv(&returnActionData, sizeof(actiondata),MPI_BYTE, MPI_ANY_SOURCE,TAG_COMPLETION, MPI_COMM_WORLD, &status);
                    
#ifdef TDEBUG
                    printf("MASTER:  Recieved completion of action %i from proc %d\n", returnActionData.actcode,status.MPI_SOURCE);
                    fflush(stdout);
#endif

                    if(returnActionData.actcode == 12) {
                        MPI_Recv(tmpPassageRecord, sizeof(PassageRecord), MPI_BYTE, status.MPI_SOURCE,TAG_PASSAGERECORD, MPI_COMM_WORLD, &status);
                        if(returnActionData.beam == 1) {
                            add(&passageRecordsB1,tmpPassageRecord);
#ifdef TDEBUG
                            printf("MASTER recieved passage record form B1b%i, turn %i -> new length %i, (istep %i)\n",status.MPI_SOURCE,tmpPassageRecord->turn,passageRecordsB1.length,istep);
                            fflush(stdout);
#endif
                        } else if(returnActionData.beam ==2) {
                            add(&passageRecordsB2,tmpPassageRecord);
#ifdef TDEBUG
                            printf("MASTER recieved passage record form B1b%i, turn %i -> new length %i, (istep %i)\n",status.MPI_SOURCE,tmpPassageRecord->turn,passageRecordsB2.length,istep);
#endif
                        }

                    } else if (returnActionData.actcode == -1) {
                        abortNextTurn = true;
                    }
                    nsync++;
                }
#ifdef TDEBUG
                printf("MASTER: STEP %d: all %d slaves accounted for!\n",istep, nsent);
                fflush(stdout);
#endif
                advance_(xxx1, xxx2, &npos);

            }  // End of STEP
            ++exitLoop.turn;
            if(abortNextTurn) {
                    break;
            }
        }  // End of TURN
        // All done; send EXIT messages to slaves                
    }

#ifdef TDEBUG
    printf("MASTER: end of run: shutting down SLAVES\n");
    fflush(stdout);
#endif
    for (int iproc = 1; iproc < activeProcs; iproc++) {
        MPI_Send(&exitLoop, sizeof(exitLoop), MPI_BYTE, iproc,TAG_ACTION, MPI_COMM_WORLD);
    }
}


bool init(ParserOutput parserOutput, action*& col,int& npos,int& numt,int& nBunchB1,int& nBunchB2) {
    bool retVal = true;
    numt = parserOutput.nturn;
    int fillingFileASize = strlen(parserOutput.fillingFileA);
    int fillingFileBSize = strlen(parserOutput.fillingFileB);
    rdfill_(parserOutput.fillingFileA,&fillingFileASize,parserOutput.fillingFileB,&fillingFileBSize,xxx1,xxx2,&npos,&nBunchB1,&nBunchB2);
    col = (action*) malloc(npos*sizeof(action));
    retVal = parseCollisionFile(parserOutput.collisionFile,col,npos);
    int nGr = 0;
    int nHOGr = 0;
    for(int i = 0;i<npos;++i) {
        if(col[i].actcode == 10) {
            if(col[i].nargs==2) {
                col[i].nargs = col[i].nargs+1;
                col[i].args = (double*)realloc(col[i].args,col[i].nargs*sizeof(double));
                col[i].args[col[i].nargs-1] = 10000+nGr;
                ++nGr;
            }
            col[i].nargs = col[i].nargs+2;
            col[i].args = (double*)realloc(col[i].args,col[i].nargs*sizeof(double));
            col[i].args[col[i].nargs-2] = 0;
            col[i].args[col[i].nargs-1] = 0;
        }
        if(col[i].actcode == 2) {
            #ifdef GAUSS
                printf("Action 2 : Using soft gaussian method\n");
            #else
                printf("Action 2 : Using HFMM\n");
            #endif
            col[i].nargs = col[i].nargs+5;
            col[i].args = (double*) realloc(col[i].args,col[i].nargs*sizeof(double));
            col[i].args[7] = 0;
            col[i].args[6] = 0;
            col[i].args[5] = 0;
            col[i].args[4] = 0;
            col[i].args[3] = nHOGr;
            ++nHOGr;
        }
    }
    return retVal;
}

double findBestKick(double* kicks,int nkicks) {
    double kick = 0.0;
    for(int i = 0;i<nkicks;++i) {
        kick+=kicks[i];
    }
    kick = kick / nkicks;
    return kick;
}

int B1Bunch2proc(int bunchNB,int nBunchB1,int nBunchB2) {
    return bunchNB;
}

int B2Bunch2proc(int bunchNB,int nBunchB1,int nBunchB2) {
    if (bunchNB == 0) {
        return 0;
    }
    return nBunchB1 + bunchNB;
}

