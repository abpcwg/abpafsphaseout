#ifndef __SLICEHEADER__
#define __SLICEHEADER__

int getSliceNumber(const double& z,const int& nSlice,const double& limInf,const double& limSup);
void computeSlicesFirstOrderMoment(double* beam,int npart,double particleCharge,int nSlice,double* sliceMoments,double limInf,double limSup);
void computePizzaSlicesFirstOrderMoment(double* beam,int npart,double particleCharge,double* rms,int nSlice,int nRing,double*** sliceMoments,double lim);
void getPizzaSliceNumber(double z,double delta,double* rms,int nSlice,int nRing,double lim,int* indices);
void computeSlicesSecondOrderMoment(double* beam,int npart,double particleCharge,int nSlice,double* sliceMoments2nd,double limInf,double limSup);

#endif
