#!/usr/bin/env python3

import os, sys
import numpy as np

pathTest = sys.argv[1] 

#os.chdir(pathTest)
fileName = os.path.join(pathTest,'output/B1b1.bparam')
data = np.loadtxt(fileName,delimiter=' ',usecols=range(18))

if len(data) == 0 or len(np.shape(data)) == 1 or np.any(np.isnan(data)) or np.any(np.isinf(data)):
    print('Failed')
else:
    print('ok')
