#!/usr/bin/bash

PATHTEST="/afs/cern.ch/work/g/giadarol/afs_phaseout_temptest_combi"

source /cvmfs/sft.cern.ch/lcg/views/LCG_93python3/x86_64-centos7-gcc7-opt/setup.sh
set -e
module load mpi/mvapich2/2.3
mkdir $PATHTEST
cp -r ./* $PATHTEST/
cd $PATHTEST
mkdir output
mkdir bin
make
condor_submit EOSTest.job
