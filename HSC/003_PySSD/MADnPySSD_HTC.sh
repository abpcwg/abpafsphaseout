#!/usr/bin/sh

#args : directory containing a file footprint.mad

cd $1;
madx < footprint.mad > output;
rm -f $1/lyapunov.data;
rm -f $1/._*;
rm -f $1/fort.90;
