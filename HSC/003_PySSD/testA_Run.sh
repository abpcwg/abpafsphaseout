#!/usr/bin/bash

PATHTEST=/afs/cern.ch/work/g/giadarol/afs_phaseout_temptests_pyssd
MYPATH=$(pwd)
set -e
mkdir $PATHTEST
cp -r ./* $PATHTEST/
cd $PATHTEST
mkdir output

source /cvmfs/sft.cern.ch/lcg/views/LCG_93python3/x86_64-centos7-gcc7-opt/setup.sh

cd ./400beta_1.5sep_500.0Ioct_-180crab
madx < footprint.mad &> output
rm -f $1/lyapunov.data
rm -f $1/._*
rm -f $1/fort.90
cd ..

export PYTHONPATH=$PYTHONPATH:$PATHTEST/PySSD
python3 PySSD_HTC.py $PATHTEST/400beta_1.5sep_500.0Ioct_-180crab/ H

cd $MYPATH
