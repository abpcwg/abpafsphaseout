#!/bin/bash

###
# Bash script for DELPHI installation, and run on HTCondor
# The repository is cloned from GitLab
# Then install GMP, MPFR and ALGLIB and compile the code
# Then we launch the code on HTContor.
###

###
# For more infos on AFS phaseout, see G.Iadarola presentation: https://indico.cern.ch/event/812709/
# and script example: https://github.com/giadarol/afs_phaseout_ecloud_test/blob/master/test_pyecloud.sh
###

###
# 2019-05-07: script created by N. Mounet (nicolas.mounet@cern.ch)
###

# The script will stop on the first error 
set -e


##############################
# Path definition
##############################

# Folder in which the tests will be performed 
PATHTEST=/afs/cern.ch/work/g/giadarol/afs_phaseout_temptests_delphi_htc

######################
# Activate miniconda #
######################

printf "\n\n**********************\nActivating miniconda environment\n"

source $PATHTEST/miniconda2/bin/activate
PYTHONEXE=`which python`
echo "Python executable: " $PYTHONEXE

# Set matplotlib backend to Agg 
# (to avoid errors if display not avaialable)
export MPLBACKEND=Agg


#####################################
# Check what was launched on HTCondor
#####################################

printf "\n\n**********************\nChecking calculations launched on HTCondor\n"

DELPHI_RESULTS_PATH=$PATHTEST/DELPHI_results

cat > $PATHTEST/check_HTCondor.py << EOF
#!$PYTHONEXE

from particle_param import proton_param
from Impedance import imp_model_resonator,freq_param
from DELPHI import Qs_from_RF_param,longdistribution_decomp,DELPHI_wrapper
from string_lib import float_to_str
from io_lib import write_ncol_file
import os
import numpy as np

lxplusbatchDEL = 'retrieve'
electron, m0, clight, E0 = proton_param()
machine = 'LHC'

# scan definitions
planes = ['x','y']
dphase = 0.
Nbscan = np.arange(0.1,8.1,0.1)*1.e11 # bunch intensity
Mscan = np.array([1])
Qpscan = np.arange(-40,1)
dampscan = np.array([0.,0.02])

# some flags and miscellaneous
flagdamperimp = 0 # 1 to use frequency dependent damper gain (provided in Zd,fd)
d = None
freqd = None
strnorm = ['']
flagnorm = 0 # 1 if damper matrix normalized at current chromaticity (instead of at zero chroma)
nevery = 1 # downsampling of the impedance (take less points than in the full model)
kmax = 1 # number of converged eigenvalues (kmax most unstable ones are converged)
kmaxplot = 200; # number of kept and plotted eigenvalues (in TMCI plot)
flagm0 = True
flageigenvect = False
queue = '2nd' # LXBATCH queue (converted in DELPHI to a ht_condor one)

# DELPHI convergence criteria
crit = 5e-2
abseps = 1e-4
result_folder = '$DELPHI_RESULTS_PATH'

# machine and beam parameters (those not given by optics file)
circ = 26658.8832
R = circ/(2.*np.pi) # machine radius
E = 6500e9
Estr = float_to_str(round(E/1e9))+'GeV'
gamma = E*electron/E0
beta=np.sqrt(1.-1./(gamma**2))
f0=clight*beta/circ # rev. frequency
omega0=2.*np.pi*f0

# optics
Qx = 62.31
Qy = 60.32
avbetax = R/Qx
avbetay = R/Qy
alphap = 0.000347974726757016
print("gamma={}, R={}, av. beta function={} m, tune={}".format(gamma,R,avbetax,Qx))

# Broad-band impedance
Rt = 25e6 # shunt impedance in MOhm/m
fr = 2e9 # cutoff frequency in Hz
Q = 1 # quality factor
imp_mod, _ = imp_model_resonator(Rlist=Rt,frlist=fr,Qlist=Q,beta=avbetax,
                                 fpar=freq_param(fmin=10,fmax=1e13,ftypescan=2,
                                                 nflog=10,fminrefine=1e9,fmaxrefine=1e10,nrefine=200,fadded=[]),
                                 listcomp=['Zxdip','Zydip'])

# longitudinal parameters
h = 35640 # harmonic number
V = 12e6
taub = 1.e-9 # bunch length (sec)
sigmaz = taub*beta*clight/4. # RMS bunch length (m)
typelong = 'Gaussian' # Longitudinal distribution
eta = alphap-1./(gamma*gamma) # slip factor
Qs = 0.0019
omegas = Qs*omega0
print("eta={}, Qs={}, sigmaz={}".format(eta,Qs,sigmaz))
# Longitudinal distribution decomposition
g, a, b = longdistribution_decomp(taub, typelong=typelong)

# DELPHI results retrieval

tuneshiftQp, tuneshiftm0Qp = DELPHI_wrapper(imp_mod, Mscan, Qpscan, dampscan, Nbscan,
                                           [omegas], [dphase], omega0, Qx, Qy, gamma, eta, a, b, taub, g,
                                           planes, nevery=nevery, particle='proton', flagnorm=flagnorm,
                                           flagdamperimp=flagdamperimp, d=d, freqd=freqd, kmax=kmax, kmaxplot=kmaxplot,
                                           crit=crit, abseps=abseps, flagm0=flagm0, flageigenvect=flageigenvect,
                                           lxplusbatch=lxplusbatchDEL, comment="{}".format(machine),
                                           queue=queue, dire=result_folder+'/', flagQpscan_outside=True)


np.testing.assert_array_equal(tuneshiftQp.shape,(2, 1, 41, 2, 80, 1, 1, 200),err_msg="Incorrect output")
np.testing.assert_array_equal(tuneshiftm0Qp.shape,(2, 1, 41, 2, 80, 1, 1),err_msg="Incorrect output")

assert not np.any(np.isnan(tuneshiftQp)), "{} nan found".format(len(np.any(np.isnan(tuneshiftQp))))
assert not np.any(np.isnan(tuneshiftm0Qp)), "{} nan found (m=0)".format(len(np.any(np.isnan(tuneshiftm0Qp))))
assert not np.all(tuneshiftQp==0.), "only zeros found"
assert not np.all(tuneshiftm0Qp==0.), "only zeros found (m=0)"

with open(os.path.join(result_folder,'Qs.txt'),'w') as f:
    f.write("{}\n".format(Qs))

np.save(os.path.join(result_folder,'TuneshiftQp.npy'), tuneshiftQp)
np.save(os.path.join(result_folder,'Tuneshiftm0Qp.npy'), tuneshiftm0Qp)

strpart=['Re','Im']

for iplane, plane in enumerate(planes):
    for iM, M in enumerate(Mscan):
        for idamp,damp in enumerate(dampscan):
            for Qp in Qpscan:
                for ir, r in enumerate(['real','imag']):

                    # Output files name for data vs Qp
                    fileoutdataNb = os.path.join(result_folder,
                        'data_vs_Nb_'+machine+'_'+Estr+'_'+str(M)+'b_d'+float_to_str(damp)+'_Qp'+float_to_str(Qp)+'_converged'+strnorm[flagnorm]+'_'+plane)
                    fileoutdataNbm0 = os.path.join(result_folder,
                        'data_vs_Nb_m0_'+machine+'_'+Estr+'_'+str(M)+'b_d'+float_to_str(damp)+'_Qp'+float_to_str(Qp)+'_converged'+strnorm[flagnorm]+'_'+plane)
                    fileoutdata_all = os.path.join(result_folder,
                        'data_vs_Nb_all_'+machine+'_'+Estr+'_'+str(M)+'b_d'+float_to_str(damp)+'_Qp'+float_to_str(Qp)+'_converged'+strnorm[flagnorm]+'_'+plane)

                    ts = getattr(tuneshiftQp[iplane,iM,np.where(Qpscan==Qp),idamp,:,0,0,0], r)
                    data = np.hstack((Nbscan.reshape((-1,1)), ts.reshape((-1,1))))
                    write_ncol_file(fileoutdataNb+'_'+r+'.dat', data, header="Nb\t"+strpart[ir]+"_tuneshift")

                    tsm0 = getattr(tuneshiftm0Qp[iplane,iM,np.where(Qpscan==Qp),idamp,:,0,0], r)
                    data = np.hstack((Nbscan.reshape((-1,1)), ts.reshape((-1,1))))
                    write_ncol_file(fileoutdataNbm0+'_'+r+'.dat', data, header="Nb\t"+strpart[ir]+"_tuneshiftm0")

                    all_unstable_modes = getattr(tuneshiftQp[iplane,iM,np.where(Qpscan==Qp),idamp,:,0,0,:], r)
                    data = np.hstack((Nbscan.reshape((-1,1)), all_unstable_modes.reshape((-1,kmaxplot))))
                    write_ncol_file(fileoutdata_all+'_'+r+'.dat', data, header="Nb\t"+strpart[ir]+"_tuneshift")

with open(os.path.join(result_folder,'Qs.txt')) as f:
    Qs_ = float(f.read())

assert Qs==Qs_, "Qs.txt file is incorrect"
EOF

chmod +x $PATHTEST/check_HTCondor.py

$PATHTEST/check_HTCondor.py > $PATHTEST/check_HTCondor.out

if [ `cat $DELPHI_RESULTS_PATH/data_vs_Nb_*.dat|wc -l` -ne '79704' ]; then
    echo "data_vs_Nb files are incomplete"
fi


printf "Done\n**********************\n"
