#!/bin/bash

###
# Bash script for DELPHI installation, and run on HTCondor
# The repository is cloned from GitLab
# Then install GMP, MPFR and ALGLIB and compile the code
# Then we launch the code on HTContor.
###

###
# For more infos on AFS phaseout, see G.Iadarola presentation: https://indico.cern.ch/event/812709/
# and script example: https://github.com/giadarol/afs_phaseout_ecloud_test/blob/master/test_pyecloud.sh
###

###
# 2019-05-07: script created by N. Mounet (nicolas.mounet@cern.ch)
###

# The script will stop on the first error 
set -e


##############################
# Path definition
##############################

# Folder in which the tests will be performed 
PATHTEST=/afs/cern.ch/work/g/giadarol/afs_phaseout_temptests_delphi_htc


COMPILATION_OUTPUT="compilation.out"
COMPILATION_ERRORS="compilation.err"
CONDA_OUTPUT="conda.out"
CONDA_ERRORS="conda.err"

# Remove any pre-existing folder
if [ -d "$PATHTEST" ] || [ -f "$PATHTEST" ]; then
    echo "Removing $PATHTEST..."
    rm -rf $PATHTEST
fi

# Create the folder
mkdir $PATHTEST
cd $PATHTEST

#####################################################
# Clone the repository using kerberos authentication
# WARNING: the repository should be made public for the purpose of the test
#####################################################
git clone https://:@gitlab.cern.ch:8443/IRIS/DELPHI.git
# we also need the Impedance python code
git clone https://:@gitlab.cern.ch:8443/IRIS/IW2D.git


##############################
# Compilation of the C++ code
##############################

printf "\n\n**********************\nCompiling DELPHI C++ code\n"

cd $PATHTEST/DELPHI/DELPHI
make > $PATHTEST/$COMPILATION_OUTPUT 2> $PATHTEST/$COMPILATION_ERRORS

printf "Done\n**********************\n"


#########################################
# Download and install miniconda (python)
#########################################

printf "\n\n**********************\nInstalling Miniconda\n"

cd $PATHTEST
mkdir downloads
cd downloads

wget https://repo.anaconda.com/miniconda/Miniconda2-latest-Linux-x86_64.sh
bash Miniconda2-latest-Linux-x86_64.sh -b -p $PATHTEST/miniconda2 > $PATHTEST/$CONDA_OUTPUT 2> $PATHTEST/$CONDA_ERRORS

printf "Done\n**********************\n"


######################
# Activate miniconda #
######################

printf "\n\n**********************\nActivating and installing dependencies\n"

source $PATHTEST/miniconda2/bin/activate
PYTHONEXE=`which python`
echo "Python executable: " $PYTHONEXE

# Set matplotlib backend to Agg 
# (to avoid errors if display not avaialable)
export MPLBACKEND=Agg

##############################
# Install required libraries #
##############################

pip install numpy > $PATHTEST/$CONDA_OUTPUT 2> $PATHTEST/$CONDA_ERRORS
pip install scipy > $PATHTEST/$CONDA_OUTPUT 2> $PATHTEST/$CONDA_ERRORS
pip install matplotlib > $PATHTEST/$CONDA_OUTPUT 2> $PATHTEST/$CONDA_ERRORS

printf "Done\n**********************\n"

##################################
# Installation of DELPHI python code
##################################

printf "\n\n**********************\nAdding the PYTHON paths in conda activate script\n"

# pythonpath for general python tools
cd $PATHTEST/IW2D/PYTHON_codes_and_scripts/General_Python_tools
echo $'\n# Python path for General_Python_tools' >> $PATHTEST/miniconda2/bin/activate
echo "PYTHONPATH="`pwd`":\$PYTHONPATH" >> $PATHTEST/miniconda2/bin/activate
echo "export PYTHONPATH" >> $PATHTEST/miniconda2/bin/activate

# pythonpath for impedance library
cd $PATHTEST/IW2D/PYTHON_codes_and_scripts/Impedance_lib_Python
echo $'\n# paths for Impedance_lib_Python' >> $PATHTEST/miniconda2/bin/activate
echo "PYTHONPATH="`pwd`":\$PYTHONPATH" >> $PATHTEST/miniconda2/bin/activate
echo "export PYTHONPATH" >> $PATHTEST/miniconda2/bin/activate

# ld_library_path for DELPHI
cd $PATHTEST/DELPHI/DELPHI
echo $'\n# library path for DELPHI' >> $PATHTEST/miniconda2/bin/activate
echo "LD_LIBRARY_PATH="`pwd`":\$LD_LIBRARY_PATH" >> $PATHTEST/miniconda2/bin/activate
echo "export LD_LIBRARY_PATH" >> $PATHTEST/miniconda2/bin/activate

# pythonpath for DELPHI library
cd $PATHTEST/DELPHI/DELPHI_Python
echo $'\n# paths for DELPHI_Python' >> $PATHTEST/miniconda2/bin/activate
echo "PYTHONPATH="`pwd`":\$PYTHONPATH" >> $PATHTEST/miniconda2/bin/activate
echo "export PYTHONPATH" >> $PATHTEST/miniconda2/bin/activate
echo "PATH="`pwd`":\$PATH" >> $PATHTEST/miniconda2/bin/activate
echo "export PATH" >> $PATHTEST/miniconda2/bin/activate

source $PATHTEST/miniconda2/bin/activate

printf "Done\n**********************\n"

##############################
# Run on HTCondor
##############################

printf "\n\n**********************\nLaunching calculations on HTCondor\n"

DELPHI_RESULTS_PATH=$PATHTEST/DELPHI_results
mkdir $DELPHI_RESULTS_PATH

cat > $PATHTEST/run_HTCondor.py << EOF
#!$PYTHONEXE

from particle_param import proton_param
from Impedance import imp_model_resonator,freq_param
from DELPHI import Qs_from_RF_param,longdistribution_decomp,DELPHI_wrapper
from string_lib import float_to_str
import os,json
import numpy as np

lxplusbatchDEL = 'launch'
electron, m0, clight, E0 = proton_param()
machine = 'LHC'

# scan definitions
planes = ['x','y']
dphase = 0.
Nbscan = np.arange(0.1,8.1,0.1)*1.e11 # bunch intensity
Mscan = np.array([1])
Qpscan = np.arange(-40,1)
dampscan = np.array([0.,0.02])

# some flags and miscellaneous
flagdamperimp = 0 # 1 to use frequency dependent damper gain (provided in Zd,fd)
d = None
freqd = None
strnorm = ['']
flagnorm = 0 # 1 if damper matrix normalized at current chromaticity (instead of at zero chroma)
nevery = 1 # downsampling of the impedance (take less points than in the full model)
kmax = 1 # number of converged eigenvalues (kmax most unstable ones are converged)
kmaxplot = 200; # number of kept and plotted eigenvalues (in TMCI plot)
flagm0 = True
flageigenvect = False
queue = '2nd' # LXBATCH queue (converted in DELPHI to a ht_condor one)

# DELPHI convergence criteria
crit = 5e-2
abseps = 1e-4
result_folder = '$DELPHI_RESULTS_PATH'

# machine and beam parameters (those not given by optics file)
circ = 26658.8832
R = circ/(2.*np.pi) # machine radius
E = 6500e9
Estr = float_to_str(round(E/1e9))+'GeV'
gamma = E*electron/E0
beta=np.sqrt(1.-1./(gamma**2))
f0=clight*beta/circ # rev. frequency
omega0=2.*np.pi*f0

# optics
Qx = 62.31
Qy = 60.32
avbetax = R/Qx
avbetay = R/Qy
alphap = 0.000347974726757016
print("gamma={}, R={}, av. beta function={} m, tune={}".format(gamma,R,avbetax,Qx))

# Broad-band impedance
Rt = 25e6 # shunt impedance in MOhm/m
fr = 2e9 # cutoff frequency in Hz
Q = 1 # quality factor
imp_mod, _ = imp_model_resonator(Rlist=Rt,frlist=fr,Qlist=Q,beta=avbetax,
                                 fpar=freq_param(fmin=10,fmax=1e13,ftypescan=2,
                                                 nflog=10,fminrefine=1e9,fmaxrefine=1e10,nrefine=200,fadded=[]),
                                 listcomp=['Zxdip','Zydip'])

# longitudinal parameters
h = 35640 # harmonic number
V = 12e6
taub = 1.e-9 # bunch length (sec)
sigmaz = taub*beta*clight/4. # RMS bunch length (m)
typelong = 'Gaussian' # Longitudinal distribution
eta = alphap-1./(gamma*gamma) # slip factor
Qs = 0.0019
omegas = Qs*omega0
print("eta={}, Qs={}, sigmaz={}".format(eta,Qs,sigmaz))
# Longitudinal distribution decomposition
g, a, b = longdistribution_decomp(taub, typelong=typelong)

# DELPHI run
DELPHI_params = {'Mscan': Mscan.tolist(),
                 'Qpscan': Qpscan.tolist(),
                 'dampscan': dampscan.tolist(),
                 'Nbscan': Nbscan.tolist(),
                 'omegasscan': [omegas],
                 'dphasescan': [dphase],
                 'omega0': omega0,
                 'Qx': Qx, 'Qy': Qy,
                 'gamma': gamma, 'eta': eta,
                 'a': a,'b': b,
                 'taub': taub, 'g': g.tolist(),
                 'planes': planes,
                 'nevery': nevery,
                 'particle': 'proton',
                 'flagnorm': flagnorm,'flagdamperimp': flagdamperimp,
                 'd': d, 'freqd': freqd,
                 'kmax': kmax, 'kmaxplot': kmaxplot,
                 'crit': crit, 'abseps': abseps, 
                 'flagm0': flagm0, 'flageigenvect': flageigenvect,
                 'lxplusbatch': lxplusbatchDEL,
                 'comment': "{}".format(machine),
                 'queue': queue, 'dire': result_folder+'/',
                 'flagQpscan_outside': True,
                 }

with open(os.path.join(result_folder,'DELPHI_parameters.json'), 'w') as f:
    f.write(json.dumps(DELPHI_params,indent=4))

tuneshiftQp, tuneshiftm0Qp = DELPHI_wrapper(imp_mod, Mscan, Qpscan, dampscan, Nbscan,
                                           [omegas], [dphase], omega0, Qx, Qy, gamma, eta, a, b, taub, g,
                                           planes, nevery=nevery, particle='proton', flagnorm=flagnorm,
                                           flagdamperimp=flagdamperimp, d=d, freqd=freqd, kmax=kmax, kmaxplot=kmaxplot,
                                           crit=crit, abseps=abseps, flagm0=flagm0, flageigenvect=flageigenvect,
                                           lxplusbatch=lxplusbatchDEL, comment="{}".format(machine),
                                           queue=queue, dire=result_folder+'/', flagQpscan_outside=True)

EOF

chmod +x $PATHTEST/run_HTCondor.py

$PATHTEST/run_HTCondor.py > $PATHTEST/run_HTCondor.out

printf "Done\n**********************\n"
