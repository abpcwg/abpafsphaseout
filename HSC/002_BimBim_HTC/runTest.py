import os,sys
import numpy as np
from HTCondor import HTCondorJobLauncher

pathTest = sys.argv[1]

nslice = 11
nring = 4
chroma = 15.0
gain = 1E-2
sep = 10.0
studyName = 'BimBimTest'
job = HTCondorJobLauncher(execFile=os.path.join(pathTest,'BimBimTest.sh'),arguments=pathTest+' '+studyName+' '+str(nslice)+' '+str(nring)+' '+str(sep)+' '+str(gain)+' '+str(chroma),outputDir=pathTest,studyName=studyName)
job.launch()
