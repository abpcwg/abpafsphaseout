# PySSD

PySSD is a Python Solver for Stability Diagram. It solves numerically the dipersion integral based on arbitrary distibution function and amplitude detuning.
Numpy and Scipy are required.

MADnPySSD_LSF.sh takes as argument a directory, where it will run madx < footprint.mad and PySSD on the output (on LXBATCH)


Author : X. Buffat (xavier.buffat@cern.ch)