import os,sys,shutil,time
import numpy as np
from HTCondor import HTCondorJobLauncher

PROTONMASS = 0.938272013

def getSep(sep,beta,emit,energy):
    return sep*np.sqrt(beta*emit/np.sqrt((energy/PROTONMASS)**2-1))*1000.0/2.0

def getXing(sep,beta,emit,energy):
    gamma = energy/0.938
    return sep/np.sqrt(beta*gamma/emit)/2

if __name__=='__main__':

    pathTest = sys.argv[1]

    maxAmpl = 3.0

    templateDir = os.path.join(pathTest,'template')
    scanOutputDir = os.path.join(pathTest,'output')

    opticskey = '400_400_400_400'
    emit = 1.7E-6
    energy = 7000.0
    intensity = 2.3E11

    xing = 250.0
    xing2 = -170.0
    xing8 = -250.0

    sep2 = 1.44
    sep8 = 1.4
    sep5 = 0.55

    normSep = 1.5
    IOct = 500.0
    crab = -180

    print(opticskey,normSep,crab)
    beta = opticskey.split('_')[1]

    newDir = beta+'beta_'+str(normSep)+'sep_'+str(IOct)+'Ioct_'+str(crab)+'crab'
    outputDir = os.path.join(scanOutputDir,newDir)
    beta = float(beta)/1000
    if not os.path.exists(outputDir):
        shutil.copytree(templateDir,outputDir)

        fileName = os.path.join(outputDir,'footprint.mad')
        os.system('sed -i s/%OPTKEY/'+opticskey+'/g '+fileName)

        fileName = os.path.join(outputDir,'beamDefinition')
        os.system('sed -i s/%EMIT/'+str(emit)+'/g '+fileName)
        os.system('sed -i s/%INTENSITY/'+str(intensity)+'/g '+fileName)

        fileName = os.path.join(outputDir,'machineConfig')
        sep = getSep(normSep,beta,emit,energy)
        os.system('sed -i s/%IOCT/'+str(IOct)+'/g '+fileName)
        os.system('sed -i s/%XING1/'+str(xing)+'/g '+fileName)
        os.system('sed -i s/%XING5/'+str(xing)+'/g '+fileName)
        os.system('sed -i s/%XING2/'+str(xing2)+'/g '+fileName)
        os.system('sed -i s/%XING8/'+str(xing8)+'/g '+fileName)
        os.system('sed -i s/%CRAB1/'+str(crab)+'/g '+fileName)
        os.system('sed -i s/%CRAB5/'+str(crab)+'/g '+fileName)
        os.system('sed -i s/%SEP1/'+str(sep)+'/g '+fileName)
        os.system('sed -i s/%SEP2/'+str(sep2)+'/g '+fileName)
        os.system('sed -i s/%SEP5/'+str(sep)+'/g '+fileName)
        os.system('sed -i s/%SEP8/'+str(sep8)+'/g '+fileName)

        jobMAD = HTCondorJobLauncher(execFile=os.path.join(pathTest,'MADnPySSD_HTC.sh'),arguments=outputDir,outputDir=outputDir,studyName='MAD')
        jobPySSDH = HTCondorJobLauncher(execFile=os.path.join(pathTest,'PySSD_HTC.sh'),arguments=pathTest+' '+outputDir+' H '+str(maxAmpl),outputDir=outputDir,studyName='PySSD_H_'+str(maxAmpl)+'sigma')
        jobPySSDV = HTCondorJobLauncher(execFile=os.path.join(pathTest,'PySSD_HTC.sh'),arguments=pathTest+' '+outputDir+' V '+str(maxAmpl),outputDir=outputDir,studyName='PySSD_V_'+str(maxAmpl)+'sigma')
        
        dabFileName = os.path.join(outputDir,'MADnPySSD.dag')
        myFile = open(dabFileName,'w')
        myFile.write(
                    'JOB  A  '+jobMAD.getFileName()+'\n' 
                   +'JOB  B  '+jobPySSDH.getFileName()+'\n'
                   +'JOB  C  '+jobPySSDV.getFileName()+'\n'
                   +'PARENT A CHILD B C')
        myFile.close()
        os.system('condor_submit_dag -f '+dabFileName)
