# AFS Phaseout tests for the BE-ABP group

This repository contains tests for the AFS phaseout

It can be cloned using git:
```
git clone https://gitlab.cern.ch/abpcwg/abpafsphaseout.git
```

There is one folder for each ABP section and each folder contains a readme file with instructions on how to run the tests. 

**Contact persons for each section:**

LAT: Andrea Latina

HSC: Giovanni Iadarola

HSI: Hannes Bartosik

HSS: Riccardo De Maria


**AFS phaseout coordinators for ABP:**

Giovanni Rumolo

Laurent Deniau
