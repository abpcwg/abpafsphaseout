#!/usr/bin/bash
start=$SECONDS

testdir=`date +%Y%m%dT%H%M%S`
mkdir $testdir

echo "START: $SECONDS seconds" >$testdir/test_summary.txt

(cd $testdir;
git clone https://github.com/MethodicalAcceleratorDesign/MAD-X.git \
  madx-nightly &>stdouterr_gitclone.txt)

status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`
echo "gitclone: $status, $SECONDS seconds, " >>$testdir/test_summary.txt

(cd $testdir;
 madx-nightly/scripts/build-test-lxplus7.sh update &>stdouterr_buildandtest.txt)

status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`
echo "buildandtest: $status, $SECONDS seconds" >>$testdir/test_summary.txt
