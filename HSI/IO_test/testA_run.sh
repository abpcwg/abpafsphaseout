#! /bin/bash

echo "Modify this file with the path you want to test."

PATHTEST='/afs/cern.ch/work/p/pyorbit/private/test_afs/'

echo -ne "Setting the environment..."
source /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3/x86_64-centos7-gcc8-opt/setup.sh
echo "done."

printf "\n**** TEST ON: $PATHTEST ****\n"
python test.py $PATHTEST

## for comparison with afs
#PATHAFS='/afs/cern.ch/work/p/pyorbit/private/test_afs'
#printf "\n**** TEST ON: $PATHAFS ****\n"
#python test.py $PATHAFS

printf "\n**** TEST ON: /tmp/ ****\n"
python test.py '/tmp/'
