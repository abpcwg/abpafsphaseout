# AFS Phaseout tests for the ABP-HSI section

This folder contains tests for the ABP-HSI section. 

In each folder there is a script called ```testA_Run.sh``` that launches the tests.
In some cases there is a second script caller ```testB_Check.sh``` to check the outcome of the test.

In all scripts there is a variable called PATHTEST which sets the path in which the test will be executed (the folder will be created).
The path specified in ```testA_Run.sh``` and ```testB_Check.sh``` should be the same.

The folders with names ending with HTC contain tests that launch jobs on HTCondor.
In those cases it is necessary to verify that all HTCondor jobs are completed before launching ```testB_Check.sh```.
