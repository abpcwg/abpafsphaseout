# This is a placet script

# Default input arguments
array set args {
	input1 0
	input2 1
}

array set args $argv
set input1 $args(input1)
set input2 $args(input2)

puts "input1 = $input1"
puts "input2 = $input2"

Octave {
	disp("Load data file")
        pathtest = getenv("PATHTEST");
	data = load([ pathtest "/data_file.dat" ]);
	
	# save temp file
	save temp_file_${input1}_${input2}.dat data
	
	# save output file to desired location
	save([ pathtest "/Results/output_${input1}_${input2}.dat" ], "data");
}
